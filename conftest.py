import os
import time
from subprocess import check_output
import hypothesis as hyp
from hypothesis import settings
from hypothesis import Verbosity
import pytest

settings.register_profile("full", settings(max_examples=30, deadline=10000))
settings.register_profile("dev", settings(max_examples=5, deadline=10000))
settings.register_profile("debug", settings(max_examples=2, deadline=10000, verbosity=Verbosity.debug))
settings.load_profile(os.getenv(u'HYPOTHESIS_PROFILE', 'dev'))

@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()

    # we only look at actual failing test calls, not setup/teardown
    if rep.when == "call" and rep.failed:
        if "tmpdir" in item.fixturenames:
            if os.access(str(item.funcargs["tmpdir"])+'/inbeam.dat', os.R_OK):
                archive = 'case_'+time.strftime('%Y%m%d_%H%M%S')+'.tar'
                check_output(["tar", "cvf", archive, "-C", str(item.funcargs["tmpdir"]), "."])
                check_output(["gtar", "xf", archive, "-C", "lastfailed"])

def pytest_addoption(parser):
    parser.addoption(
        "--libdir",
        action="append",
        default=[],
        help="torbeam libdir to be passed to test functions",
    )

def pytest_generate_tests(metafunc):
    if "libdir" in metafunc.fixturenames:
        metafunc.parametrize("libdir", metafunc.config.getoption("libdir"))
    else: 
        metafunc.parametrize("libdir", '^^^libtorbeam^lib')
