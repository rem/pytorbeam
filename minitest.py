#!/usr/bin/env python3

import os
import sys
sys.path.append("../pytorbeam")

import ctbm

def test_relativistic():
    path = 'tests/test_X3'
    m, n, eqdata = ctbm.readTopfile3(path+'/topfile')
    k, l, prdata = ctbm.readPrdata(path+'/ne.dat', path+'/Te.dat')
    intinbeam, dblinbeam = ctbm.readInbeam(path+'/inbeam.dat')
    intinbeam[6] = intinbeam[6] % 10
    intinbeam[8] = 1    # no extra output
    result = ctbm.call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata,
                                   mode=ctbm.TBM_MODE_NORMAL)
    rhoresult = result[0]
    print (rhoresult)


print ('environment:')
print ('NAG_KUSARI_FILE:', os.environ['NAG_KUSARI_FILE'])
sys.stdout.flush()

test_relativistic()