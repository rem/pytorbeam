#!/usr/bin/env python

import re
import sys
import ctbm
import numpy as np
from ctypes import *
from subprocess import Popen, PIPE
import eqtools
import namelist as nl
from MDSplus import Connection

def cmdline(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

def beam_param_tcv(shot, time, launcher):
    script = "function [] = auto_eclp(shot, times, l)\n \
    % prints to stdout all relevant launch_params of requested shot/time\n \
        function showL(lpar, pow, l)\n \
            fprintf('! Beam params of Launcher %d : -------------------------------------------\\n',l);\n \
            fprintf(' xf = %g,       ! wave freq: om = 2*pi*xf\\n', lpar{l}{1}.freq);\n \
            fprintf(' xtordeg = %g,     ! beam toroidal start angle\\n', lpar{l}{1}.phi_toray-180);\n \
            fprintf(' xpoldeg = %g,     ! beam poloidal start angle\\n', lpar{l}{1}.theta_toray-90);\n \
            a = find( lpar{l}{1}.r1 < 115 );\n \
            R = lpar{l}{1}.r1(a(1));\n \
            %R = sqrt(lpar{l}{1}.x0^2+lpar{l}{1}.y0^2);\n \
            fprintf(' xxb = %g,     ! radial beam starting coordinate\\n', R);\n \
            fprintf(' xyb = 0.0,     ! irrelvant beam starting coordinate\\n');\n \
            fprintf(' xzb = %g,     ! vertical beam starting coordinate\\n', lpar{l}{1}.z1(a(1)) );\n \
            fprintf(' xryyb = 87.93,     ! initial principal curvature\\n');\n \
            fprintf(' xrzzb = 87.93,     ! initial principal curvature\\n');\n \
            fprintf(' xwyyb = %g,     ! initial principal width\\n', 1.88);\n \
            fprintf(' xwzzb = %g,     ! initial principal width\\n', 1.88);\n \
            fprintf(' xpw0 = %g,     ! initial beam power [MW]\\n', pow(l)/1000. );\n \
            fprintf('! Plasma params: ----------------------------------\\n');\n \
            fprintf(' xrmaj = 89.0,     ! major radius\\n');\n \
            fprintf(' xrmin = 25.0,     ! minor radius\\n');\n \
            fprintf(' xzaxis = %g,      ! z-axis of original equilibrium\\n', lpar{l}{1}.z_axis);\n \
        end\n \
    \n \
    fprintf('BEGIN OUTPUT\\n');\n \
    try\n \
      [r_phi0,z_phi0,r1,z1,rc,zc,thetphi_L,thetphi_tor,raysbylauncher,powers,fname,z_axis,launch_params] =toray_raygeom_me(shot,times);\n \
    catch\n \
      fprintf('! error from toray_raygeom_me, no result\\n');\n \
      fprintf('END OUTPUT\\n');\n \
      l = 0;\n \
      exit \n \
    end\n \
    \n \
    if l > 0, \n \
      showL(launch_params, powers, l), \n \
    else,\n \
      for i=1:6,\n \
        if isempty(launch_params{i})\n \
          continue\n \
        end\n \
       showL(launch_params, powers, i);\n \
      end\n \
    end\n \
    fprintf('END OUTPUT\\n');\n \
    \n \
    end\n"

    with open('auto_eclp.m', 'w') as f:
        f.write(script)
    cmd = 'matlab -nodesktop -nodisplay -nosplash -r "auto_eclp('+ str(shot)+','+str(time)+','+str(launcher)+');quit;"'
    res = cmdline(cmd)
    resL = res[res.index('BEGIN OUTPUT')+len('BEGIN OUTPUT\n'):res.index('END OUTPUT')]
    strarr = np.array(resL.split('\n'))
    for s in strarr:
        u = s.replace('!', '#').lstrip()
        exec(u)

    cmdline('rm -f auto_eclp.m')
    return resL

def beam_param_all(shot, times):
    script = "function [] = auto_eclp_all(shot, stimes)\n \
      times = str2num(strrep(stimes,',',' ')); \n \
      fprintf('BEGIN OUTPUT\\n'); \n \
       try\n \
         [r_phi0,z_phi0,r1,z1,rc,zc,thetphi_L,thetphi_tor,raysbylauncher,powers,fname,z_axis,launch_params] =toray_raygeom_me(shot,times);\n \
       catch\n \
         fprintf('! error from toray_raygeom_me, no result\\n');\n \
         fprintf('END OUTPUT\\n');\n \
         times = [];\n \
       end\n \
       if length(times) > 0 \n \
         for i=1:length(launch_params) \n \
           for j=1:length(launch_params{i}) \n \
             lpar = launch_params{i}{j};\n \
             if isempty(lpar)\n \
               continue\n \
             end\n \
             a = find( lpar.r1 < 115 );\n \
             fprintf(' %g %g %g %g %g %g 0.0 %g %g 87.93 87.93 1.88 1.88 89.0 25.0\\n', ...\n \
               i, lpar.time, lpar.freq, lpar.phi_toray-180, lpar.theta_toray-90, lpar.r1(a(1)), lpar.z1(a(1)), powers(i+9*(j-1))/1000. ); \n \
           end\n \
         end\n \
         fprintf('END OUTPUT\\n');\n \
       end \n"

    with open('auto_eclp_all.m', 'w') as f:
        f.write(script)
    cmd = 'matlab -nodesktop -nodisplay -nosplash -r "auto_eclp_all(' + str(shot) +','
    stimes = str(times.tolist()).replace(' ', '').replace('[', '\'').replace(']', '\'')
    cmd = cmd + stimes + ');quit;"'
    #print '----------------'
    #print cmd
    #print '----------------'
    res = cmdline(cmd)
    resL = res[res.index('BEGIN OUTPUT')+len('BEGIN OUTPUT\n'):res.index('END OUTPUT')]
    resN = 'reslist = [ ['+resL.replace('  ', ' ').replace('  ', ' ').strip().replace('\n', '],[').replace(' ', ',') + '] ]'
    resN = resN.replace('[,', '[')
    exec(resN)
    resA = np.array(reslist, dtype='double')
    cmdline('rm -f auto_eclp_all.m')
    return resA

def write_inbeam_tcv(shot, time, launcher):
    with open('inbeam.dat', 'w') as f:
        f.write('&edata\n')
        f.write('! ODE solver params: ---------------------------------------------------\n')
        f.write(' xrtol = 3e-07,	     ! required rel. error\n')
        f.write(' xatol = 3e-07,	     ! required abs. error\n')
        f.write(' xstep = 1.000000,	     ! integration step\n')
        f.write(' npow = 1,	     ! power absorption on(1)/off(0)\n')
        f.write(' ncd = 0,	     ! current drive calc. on(1)/off(0)\n')
        f.write(' ianexp = 2,	     ! analytic (1) or experimental (2) equilibrium\n')
        f.write(' ndns = 2,	     ! analytic (1) or interpolated (2) density profiles\n')
        f.write(' nte = 2,	     ! analyt. (1) or interp. (2) electron temp. prof.\n')
        f.write(' ncdroutine = 0,    ! 0->Curba, 1->Lin-Liu, 2->Lin-Liu+momentum conservation\n')
        f.write(' nshot = '+str(shot)+',    ! shotnumber\n')
        f.write(' xtbeg = '+str(time)+',    ! tbegin\n')
        f.write(' xtend = '+str(time)+',    ! tend\n')
        f.write(' nabsroutine = 0,    ! Westerhof(0) or Farina (1)\n')
        f.write(' noout = 0,    ! suppress output (1) \n')
        f.write(' nrela = 0,    ! weakly (0) or fully (1) relativistic absorption\n')
        resL = beam_param_tcv(shot, time, launcher)
        if len(resL) > 10:
            f.write(resL)
        f.write(' xb0 = 2.500000,	     ! central toroidal magnetic field [T]\n')
        f.write(' xdns = 1.02e+14,	     ! electron density [cm**(-3)]\n')
        f.write(' edgdns = 1e+13,     ! electron density [cm**(-3)]\n')
        f.write(' xe1 = 2.000000,	     ! exponent in the density profile\n')
        f.write(' xe2 = 0.500000,	     ! exponent in the density profile\n')
        f.write(' xte0 = 25.000000,	     ! electron temperature [keV]\n')
        f.write(' xteedg = 1.000000,    ! electron temperature [keV]\n')
        f.write(' xe1t = 2.000000,	     ! exponent in the temperature profile\n')
        f.write(' xe2t = 2.000000,	     ! exponent in the temperature profile\n')
        f.write(' xdel0 = 0.000000,	     ! Shafranov shift on the axis\n')
        f.write(' xdeled = 0.000000,	     ! Shafranov shift on the edge\n')
        f.write(' xelo0 = 1.000000,	     ! elongation on the axis\n')
        f.write(' xeloed = 1.500000,	     ! elongation on the edge\n')
        f.write(' xq0 = 1.000000,	     ! safety factor on the axis\n')
        f.write(' xqedg = 4.000000	     ! safety factor on the edge\n')
        f.write('/\n')

def file2array(fname, tplidx=0):
    f = open(fname)
    strarr = np.array(f.readlines())
    f.close()
    strarr = strarr[tplidx:]

    nnum = len(re.split(r'\s+', strarr[0].strip()))
    mnum = strarr.size
    res = np.zeros([nnum, mnum])
    res[:, :] = np.NAN
    ic = 0
    for s in strarr:
        jc = 0
        for i in re.split(r'\s+', s.strip()):
            try:
                res[jc][ic] = float(i)
                jc += 1
            except Exception:
                jc += 1
        jc += 1

    return res

def btvac_wrap_tcv(shot, time):
    script = "function [btvac] = auto_btvac(shot, time)\n" + \
    "% prints to stdout value of Btor (@ 0.88 m) at time for shot\n" + \
    "\n" + \
    "fprintf('BEGIN OUTPUT\\n');\n" + \
    "try\n" + \
    "  a = gdat(shot, 'b0');\n" + \
    "  btvac = mean(a.data(find(a.t(find(a.t < time+0.05)) > time-0.05)));\n" + \
    "  fprintf('>>> %f\\n', btvac);\n" + \
    "catch\n" + \
    "  fprintf('! error from gdat, no result \\n');\n" + \
    "end\n" + \
    "  fprintf('END OUTPUT\\n');\n" + \
    "\n" + \
    "end\n"
    with open('auto_btvac.m', 'w') as f:
        f.write(script)
    cmd = 'matlab -nodesktop -nodisplay -nosplash -r "auto_btvac('+ str(shot)+','+str(time)+');quit;"'
    #print 'execute: '+cmd
    res2 = cmdline(cmd)
    resL = res2[res2.index('BEGIN OUTPUT')+len('BEGIN OUTPUT\n'):res2.index('END OUTPUT')]
    strarr = np.array(resL.split('\n'))
    s = strarr[0][strarr[0].index('>>> ')+4:]
    btvac = float(s)
    cmdline('rm -f auto_btvac.m')
    return btvac

class tbmd:
######################################################################################
#    ok def readEqdata(self):
#    ok def readPrdata(self):
#    ok def readBeamP(self):
#    ok def readAll(self):
#    def eqdata(self, time = 2.0):
#    def prdata(self, time = 2.0):
#    def beamdata(self, time = 2.0, gyr = 5, beamdbl = np.zeros(50, dtype='double')):
#    def default_inbeams(self):
#    def clc1time(self, time, gyr=[5]):
    def __init__(self, shotnumber, eqdiag='tcv_shot', \
                      nediag='\\results::toray.input:ne_prof',\
                      tediag='\\results::toray.input:te_prof'):
      self.shot = shotnumber
      self.eqd = eqdiag
      self.ned = nediag
      self.ted = tediag
      self.eqdok = False
      self.treeopen = False
      self.eq = None

    def readEqdata(self):
      try:
        self.eq = eqtools.TCVLIUQETree(self.shot) ##, tree=self.eqd)
        self.eqdok = True
      except Exception as e:
        print('Cannot continue without equilibrium data.')
        self.eqdok = False
        print(e)

      return 0

    def readPrdata(self):
      try:
        self.c = Connection('tcvdata.epfl.ch')
      except:
        print ("cannot make connection to MDSplus")
        sys.exit(1)
      try:
        self.c.openTree('tcv_shot', self.shot)
        try:
          self.nedta = self.c.get(self.ned).data()
          self.nearea = np.array(self.c.get('dim_of('+self.ned+',0)'))
          self.netime = np.array(self.c.get('dim_of('+self.ned+',1)'))
          self.nestatus = 1
        except:
          self.nestatus = 0
        try:
          self.tedta = self.c.get(self.ted).data()
          self.tearea = np.array(self.c.get('dim_of('+self.ted+',0)'))
          self.tetime = np.array(self.c.get('dim_of('+self.ted+',1)'))
          self.testatus = 1
        except:
          self.testatus = 0
        self.c.closeAllTrees()
      except:
        print ("Tree open failed")
        self.nestatus = 0
        self.testatus = 0

    def readBeamSingle(self, timept):
      res = beam_param_tcv(self.shot, timept, 0)
      res = res.replace('! Beam params of Launcher ', '&L')
      res = res.replace(': -', ' ! ')
      res = res.replace('! minor radius\n', '\n/\n')
      return nl.read_namelist_file(res, text=True)

    def readBeamP(self, timepts=None):
      if timepts is None:
        timepts = self.eq.getTimeBase()
      res = beam_param_all(self.shot, timepts)
      ll = np.size( res[:,0] )
      lt = np.size(timepts)
      self.ecpol = np.zeros([7,lt],dtype='double')
      self.ecpol[0,:] = np.array(timepts)
      self.ector = np.zeros([7,lt],dtype='double')
      self.ector[0,:] = np.array(timepts)
      self.ecfreq = np.zeros([7,lt],dtype='double')
      self.ecfreq[0,:] = np.array(timepts)
      self.ecpow = np.zeros([7,lt],dtype='double')
      self.ecpow[0,:] = np.array(timepts)
      self.ecxxb = np.zeros([7,lt],dtype='double')
      self.ecxxb[0,:] = np.array(timepts)
      self.ecxzb = np.zeros([7,lt],dtype='double')
      self.ecxzb[0,:] = np.array(timepts)
      for i in np.arange(0,ll):
        gyr = int(res[i,0])
        ii = np.argmin(np.abs(timepts-res[i,1]))
        self.ecpol[gyr,ii] = res[i,4]
        self.ector[gyr,ii] = res[i,3]
        self.ecfreq[gyr,ii] = res[i,2]
        self.ecpow[gyr,ii] = res[i,8]
        self.ecxxb[gyr,ii] = res[i,5]
        self.ecxzb[gyr,ii] = res[i,7]

    def readAll(self):
        if not hasattr(self, 'sfnedata'):
            self.readPrdata()
        if not self.eqdok:
            self.readEqdata()
        if not hasattr(self, 'ecpow'):
            self.readBeamP()

    def eqdata(self, time = 2.0):
      if self.eqdok:
        timeidx = np.argmin(abs(self.eq.getTimeBase() - time))
        R = self.eq.getRGrid()
        z = self.eq.getZGrid()
        m = R.size
        n = z.size
        eqmat = np.zeros(1+m+n+4*m*n, dtype='double')
        eqmat[0] = 0.0
        eqmat[1:m+1] = R
        eqmat[m+1:m+n+1] = z
        psi = self.eq.rz2psi(R, z, time, make_grid=True)
        br = self.eq.rz2BR(R, z, time, make_grid=True)
        bz = self.eq.rz2BZ(R, z, time, make_grid=True)
        # assume Bt as 1/R with Bt@axis as ref
        r0 = self.eq.getMagR()[0]
        b0 = self.eq.getBtVac()[timeidx]
        bt, dum = np.meshgrid( r0*b0/R, z )

        if m > 40 or n > 70:
            target_m = 39
            target_n = 69
        else:
            target_m = 0
            target_n = 0

        # change to different grid resolution, if needed
        if target_m > 0 and target_n > 0 and ( target_m != m or target_n != n):
            print ('interpolating equilibrium grid') # from (%(m)dx%(n)d) to (%(tm)dx%(tn)d)\n' %\
                       #{ 'm': m, 'n': n, 'tm': target_m, 'tn': target_n }
            Rn = np.linspace(start=R[0], stop=R[-1], num=target_m)
            zn = np.linspace(start=z[0], stop=z[-1], num=target_n)
            f = sp.interpolate.interp2d(R, z, br, kind='cubic', copy=True)
            br = f(Rn, zn)
            f = sp.interpolate.interp2d(R, z, bt, kind='cubic', copy=True)
            bt = f(Rn, zn)
            f = sp.interpolate.interp2d(R, z, bz, kind='cubic', copy=True)
            bz = f(Rn, zn)
            f = sp.interpolate.interp2d(R, z, psi, kind='cubic', copy=True)
            psi = f(Rn, zn)
            R = Rn
            z = zn
            m = target_m
            n = target_n
            eqmat[m+n+1:m+n+1+m*n] = br.flatten()
            eqmat[m+n+1+m*n:m+n+1+m*n*2] = bt.flatten()
            eqmat[m+n+1+m*n*2:m+n+1+m*n*3] = bz.flatten()
            eqmat[m+n+1+m*n*3:m+n+1+m*n*4] = -self.eq._psiRZ[timeidx,:,:].flatten()

      return m,n,eqmat

    def prdata(self, time = 2.0):
      stride = 1
      if self.nestatus:
        k = self.nedta.shape[1]
        stridek = (k-1) / 150 + 1
        l = self.tedta.shape[1]
        stridel = (l-1) / 150 + 1
        prmat = np.zeros(600, dtype='double')
        neidx = np.argmin(abs(self.netime - time))
        teidx = np.argmin(abs(self.tetime - time))
        prmat[0:k/stridek] = self.nearea[np.arange(0,k,stridek)]
        prmat[k/stridek:2*(k/stridek)] = self.nedta[neidx,np.arange(0,k,stridek)]*1e-19
        prmat[2*k/stridek:2*k/stridek+l/stridel] = self.tearea[np.arange(0,l,stridel)]
        prmat[2*k/stridek+l/stridel:2*k/stridek+2*l/stridel] = self.tedta[teidx,np.arange(0,l,stridel)]*1e-3

        return k/stridek,l/stridel,prmat

    def beamdata(self, time = 2.0, gyr = 5, beamdbl = np.zeros(50, dtype='double')):
      ''' Creates/overwrites entries relevant for beam data in dblinbeam array

      '''
      if gyr < 1 or gyr > 9:
        beamdbl[0] = -1.0
        return beamdbl
      #beamdbl = np.zeros(50)
      ecidx = np.argmin(abs(self.ecpow[0,:] - time))

      beamdbl[0] = self.ecfreq[gyr,ecidx] #ecdata['xf']
      beamdbl[1] = self.ector[gyr,ecidx] #ecdata['xtordeg']
      beamdbl[2] = self.ecpol[gyr,ecidx] #ecdata['xtordeg']
      beamdbl[23] = self.ecpow[gyr,ecidx] #ecdata['xpw0']
      beamdbl[3] = self.ecxxb[gyr,ecidx] #ecdata['xxb']
      beamdbl[4] = 0.0 #ecdata['xyb']
      beamdbl[5] = self.ecxzb[gyr,ecidx] #ecdata['xzb']
      beamdbl[19] = 87.93 #ecdata['xryyb']
      beamdbl[20] = 87.93 #ecdata['xrzzb']
      beamdbl[21] = 1.88 #ecdata['xwyyb']
      beamdbl[22] = 1.88 #ecdata['xwzzb']

      beamdbl[24] = 89.0 #ecdata['xrmaj']
      beamdbl[25] = 25.0 #ecdata['xrmin']

      return beamdbl

    def default_inbeams(self):
      """Returns sensible initialization for most values of intinbeam & dblinbeam

        Routine does not prescribe starting position and/or angles as this
        normally depends on user's choice.
        Values are set such that typical (TCV) runs should be possible with
        few additional parameters to be filled in. Current drive is switched on.
      """

      intinbeam = np.zeros(50, dtype=int)
      dblinbeam = np.zeros(50)

      intinbeam[49] = 0
      intinbeam[0] = 2
      intinbeam[1] = 2
      intinbeam[2] = -1
      intinbeam[3] = 1
      intinbeam[4] = 1
      intinbeam[5] = 2
      intinbeam[6] = 2
      intinbeam[7] = 30
      intinbeam[8] = 0
      intinbeam[9] = 0
      intinbeam[10] = 2
      intinbeam[11] = 1   # Farina: 1 Westerhof 0
      intinbeam[12] = 0
      intinbeam[13] = 1

      dblinbeam[0] = 8.27e10
      dblinbeam[6] = 10.2e13
      dblinbeam[7] = 1.0e13
      dblinbeam[8] = 2.0
      dblinbeam[9] = 0.5
      dblinbeam[10] = 25.0
      dblinbeam[11] = 1.0
      dblinbeam[12] = 2.0
      dblinbeam[13] = 2.0
      dblinbeam[14] = 3.e-7
      dblinbeam[15] = 3.e-7
      dblinbeam[16] = 1.0
      dblinbeam[17] = 1.5
      dblinbeam[18] = 1.5
      dblinbeam[23] = 0.
      dblinbeam[24] = 165.
      dblinbeam[25] = 60.
      dblinbeam[26] = -2.5
      dblinbeam[27] = 0.
      dblinbeam[28] = 0.
      dblinbeam[29] = 1.
      dblinbeam[30] = 1.5
      dblinbeam[31] = 1.
      dblinbeam[32] = 4.
      dblinbeam[33] = -1.
      dblinbeam[34] = 1.
      dblinbeam[35] = 0.96

      return intinbeam, dblinbeam

    def clc1time(self, time, gyr=[5]):
      self.readAll()
      m,n,eqdata = self.eqdata(time=float(time))
      k,l,prdata = self.prdata(time=float(time))
      intinbeam, dblinbeam = self.default_inbeams()
      tt1 = []
      tt2 = []
      ttt = []
      rrr = []
      for g in gyr:
        dblinbeam = self.beamdata(time=float(time),gyr=g,beamdbl=dblinbeam)
        #ctbm.writeInbeam( intinbeam, dblinbeam, fname='inbeam'+str(g).strip()+'.dat' )
        rr, t1d, t1td, t2d, t2nd, ic, ibg, nv, vp = ctbm.call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata)
        rrr.append(rr)
        tt1.append(t1d)
        tt2.append(t2nd)
        if len(ttt)==0:
            ttt = np.zeros_like(t2nd)
            ttt[0,:] = t2nd[0,:]
      ttt[1,:] = ttt[1,:] + t2nd[1,:]
      ttt[2,:] = ttt[2,:] + t2nd[2,:]

      self.rhores = rrr

      return tt1, tt2, ttt

    def plotInput(self, fig1, fig2, time=2.0):
      m,n,eqdata = self.eqdata(time=float(time))
      k,l,prdata = self.prdata(time=float(time))
      ctbm.plotEq(eqdata=eqdata, eqm=m, eqn=n, plteq=fig1)
      ctbm.plotPr(prdata=prdata, prk=k, prl=l, pltpr=fig2)

    def plotOutput(self, fig1, fig3, fig4, tt1, tt2, ttt, \
          gcol=['darkviolet', 'magenta', 'brown', 'coral', 'b', 'g', 'r', 'cyan']):
      i = 0
      for t1d in tt1:
        fig1.plot(t1d[0,:]/100., t1d[1,:]/100., gcol[i % len(gcol)])
        fig1.plot(t1d[2,:]/100., t1d[3,:]/100., gcol[i % len(gcol)])
        fig1.plot(t1d[4,:]/100., t1d[5,:]/100., gcol[i % len(gcol)])
        i = i + 1

      i = 0
      for t2nd in tt2:
        fig3.plot(t2nd[0,:], t2nd[1,:], gcol[i % len(gcol)])
        fig4.plot(t2nd[0,:], t2nd[2,:], gcol[i % len(gcol)])
        i = i + 1

      fig3.set_title('P_EC vs rho')
      fig4.set_title('I_ECCD vs rho')
      fig3.plot(tt2[0][0,:], ttt[1,:], 'k')
      fig4.plot(tt2[0][0,:], ttt[2,:], 'k')

      return 0

def __main__():
  import matplotlib.pylab as plt
  print ("demo program plots single/all ECRH2 gyrotrons power and current deposition")

  if len(sys.argv) < 2:
    print('Need to identify dataset with shot ( +time)')
    print(' e.g. '+sys.argv[0]+' 30594 5.43 ')
    print('  add launcher number, if not default (5), like:')
    print('      '+sys.argv[0]+' <shot> [time [launcher]] ')
    sys.exit(1)

  if len(sys.argv) > 3:
    gyrlist = [int(sys.argv[3])]
  else:
    gyrlist = [1,4,6]

  plt1 = plt.subplot(2,2,1)
  plt2 = plt.subplot(2,2,2)
  plt3 = plt.subplot(2,2,3)
  plt4 = plt.subplot(2,2,4)

  ttime = 1.0
  if len(sys.argv) > 1:
      dat = tbmd(int(sys.argv[1]))
  if len(sys.argv) > 2:
      ttime = float(sys.argv[2])

  dat.readAll()
  dat.plotInput(plt1, plt2, time=ttime)
  tt1, tt2, ttt = dat.clc1time(ttime, gyr=gyrlist)

  dat.plotOutput(plt1, plt3, plt4, tt1, tt2, ttt)
  plt.show()

  m,n,eqdata = dat.eqdata()
  k,l,prdata = dat.prdata()
  rhoresult = dat.rhores[0]
  t1data = tt1[0]
  t2n_data = tt2[0]
  intinbeam, dblinbeam = dat.default_inbeams()
  dblinbeam = dat.beamdata(time=ttime, gyr=gyrlist[-1],beamdbl=dblinbeam)

  ctbm.makeBigPlot(m, n, eqdata, k, l, prdata, rhoresult, t1data, t2n_data, intinbeam, dblinbeam)
  plt.show()

if __name__ == "__main__":
    __main__()

