#!/usr/bin/env python
__authors__ = 'Matthias Reich'
__email__ = "rem@ipp.mpg.de"
__version__ = '1.0'
__date__ = '15.11.2016'

import sys
sys.path.append('/afs/ipp/aug/ads-diags/common/python/lib')
#import os
try:
    import dd
    import map_equ
except Exception, e:
    print "Problems loading ddww/equilibrium module"
    print "OS thinks:"
    print e
    sys.exit(1)
from ctypes import cdll, c_int32, c_double, byref
import numpy as np
import ctbm
#from IPython import embed

try:
    libecrh = '/afs/ipp/home/e/ecrh/lib/libaug_ecrh_setmirrors.so'
    ecrh = cdll.LoadLibrary(libecrh)
except Exception:
    print 'Cannot find AUG ECRH library, will not run AUG live cases'

def getECRHanglesAUG(shot, jgy, time=0.0):
    if jgy < 5:
        ecrhsys = 'P_sy1_g'+str(jgy)
    else:
        ecrhsys = 'P_sy2_g'+str(jgy - 4)

    sf = dd.shotfile('ECS', shot)
    pol = sf.getParameter(ecrhsys, 'GPolPos').data
    tor = sf.getParameter(ecrhsys, 'GTorPos').data
    sf.close()

    if jgy > 4 and time > 0:
        sf = dd.shotfile('ECN', shot)
        sig = sf("G"+str(jgy-4)+"POL")
        if time < sig.time[0] or time > sig.time[sig.time.size-1]:
            raise Exception("Time not accessible in mirror position signal")
        pol = sig.data[sig.time > time][0]/100 # [V] (=cm) -> [m]
        sf.close()

    return pol, tor

def ecrh2tbm(pol_ecs, tor_ecs, jgy, nshot):
    """Converts ECRH machine parameters to TORBEAM compatible angles

      Input can be directly from values read from shotfile.
      System ECRH1 has parameters GPolPos and GTorPos in parameter set
      P_sy1_g which don't change during a shot.
      System ECRH2 has parameters beta and time-varying poloidal mirror
      which can be read from ECN.
    """
    if nshot > 27400:
        datum = 0
    else:
        datum = 20000101

    c_err = c_int32(0)
    c_sysunt = c_int32(100 + 100*(jgy / 5) + (jgy % 5) + (jgy / 5))

    if jgy > 4:
        c_theta = c_double(1.e3*pol_ecs) # [m] -> [mm]
    else:
        c_theta = c_double(pol_ecs)
        c_phi = c_double(tor_ecs)
        _err = byref(c_err)
        _sysunt = byref(c_sysunt)
        _theta = byref(c_theta)
        _phi = byref(c_phi)
        c_datum = c_double(datum)
        _datum = byref(c_datum)
        ecrh.setval2tp_(_err, _sysunt, _theta, _phi, _datum)

    if c_err.value < 0 or c_err.value > 100:
        raise Exception("Parameter problem: "+str(c_err.value))
    if c_err.value > 0:
        raise Exception("Calculation problem: "+str(c_err.value))

    return -c_theta.value, -c_phi.value

class tbmd(object):
    def __init__(self, shotnumber, eqdiag='EQH', nediag='IDA', tediag='IDA'):
        self.shot = shotnumber
        self.eqd = eqdiag
        self.ned = nediag
        self.ted = tediag
        self.kk = map_equ.equ_map()
        self.eqdok = False

    def readEqdata(self):
        if self.kk.Open(self.shot, diag=self.eqd):
            self.eqdok = True
            self.kk._read_pfm()
            self.kk._read_scalars()
            self.kk._read_profiles()
        else:
            print 'Cannot continue without equilibrium data.'
        return 0

    def readPrdata(self):
        self.sfn = dd.shotfile(self.ned, self.shot)
        if self.ned == 'IDA':
            self.nedta = self.sfn("ne")
        self.sft = dd.shotfile(self.ted, self.shot)
        if self.ted == 'IDA':
            self.tedta = self.sft("Te")
        return 0

    def readBeamP(self):
        self.ec = dd.shotfile('ECN', self.shot)
        leng = self.ec("G1POL").time.size
        self.ecpol = np.zeros([9, leng], dtype='double')
        self.ecpol[0, :] = self.ec("G1POL").time
        self.ecpol[5, :] = self.ec("G1POL").data/100.
        self.ecpol[6, :] = self.ec("G2POL").data/100.
        self.ecpol[7, :] = self.ec("G3POL").data/100.
        self.ecpol[8, :] = self.ec("G4POL").data/100.
        self.ec.close()
        self.ec = dd.shotfile('ECS', self.shot)
        self.ecpol[1, :] = self.ec.getParameter('P_sy1_g1', 'GPolPos').data
        self.ecpol[2, :] = self.ec.getParameter('P_sy1_g2', 'GPolPos').data
        self.ecpol[3, :] = self.ec.getParameter('P_sy1_g3', 'GPolPos').data
        self.ecpol[4, :] = self.ec.getParameter('P_sy1_g4', 'GPolPos').data
        self.ector = np.zeros(9, dtype='double')
        self.ector[1] = self.ec.getParameter('P_sy1_g1', 'GTorPos').data
        self.ector[2] = self.ec.getParameter('P_sy1_g2', 'GTorPos').data
        self.ector[3] = self.ec.getParameter('P_sy1_g3', 'GTorPos').data
        self.ector[4] = self.ec.getParameter('P_sy1_g4', 'GTorPos').data
        self.ector[5] = self.ec.getParameter('P_sy2_g1', 'GTorPos').data
        self.ector[6] = self.ec.getParameter('P_sy2_g2', 'GTorPos').data
        self.ector[7] = self.ec.getParameter('P_sy2_g3', 'GTorPos').data
        self.ector[8] = self.ec.getParameter('P_sy2_g4', 'GTorPos').data
        self.ecfreq = np.zeros(9, dtype='double')
        self.ecfreq[1] = self.ec.getParameter('P_sy1_g1', 'gyr_freq').data
        self.ecfreq[2] = self.ec.getParameter('P_sy1_g2', 'gyr_freq').data
        self.ecfreq[3] = self.ec.getParameter('P_sy1_g3', 'gyr_freq').data
        self.ecfreq[4] = self.ec.getParameter('P_sy1_g4', 'gyr_freq').data
        self.ecfreq[5] = self.ec.getParameter('P_sy2_g1', 'gyr_freq').data
        self.ecfreq[6] = self.ec.getParameter('P_sy2_g2', 'gyr_freq').data
        self.ecfreq[7] = self.ec.getParameter('P_sy2_g3', 'gyr_freq').data
        self.ecfreq[8] = self.ec.getParameter('P_sy2_g4', 'gyr_freq').data
        leng = self.ec("PG1N").time.size
        self.ecpow = np.zeros([9, leng], dtype='double')
        self.ecpow[0, :] = self.ec("PG1N").time
        self.ecpow[1, :] = self.ec("PG1").data/1e6
        self.ecpow[2, :] = self.ec("PG2").data/1e6
        self.ecpow[3, :] = self.ec("PG3").data/1e6
        self.ecpow[4, :] = self.ec("PG4").data/1e6
        self.ecpow[5, :] = self.ec("PG1N").data/1e6
        self.ecpow[6, :] = self.ec("PG2N").data/1e6
        self.ecpow[7, :] = self.ec("PG3N").data/1e6
        self.ecpow[8, :] = self.ec("PG4N").data/1e6
        self.ec.close()

        return 0

    def readAll(self):
        if not self.kk.eq_open:
            self.readEqdata()
        if not hasattr(self, 'sfn'):
            self.readPrdata()
        if not hasattr(self, 'ecpow'):
            self.readBeamP()

    def eqdata(self, time=2.0):
        if self.kk.eq_open:
            timeidx = np.argmin(abs(self.kk.t_eq - time))
            m = self.kk.pfm.shape[0]
            n = self.kk.pfm.shape[1]
            eqmat = np.zeros(1+m+n+4*m*n, dtype='double')
            eqmat[0] = self.kk.psix[timeidx]
            eqmat[1:m+1] = self.kk.Rmesh
            eqmat[m+1:m+n+1] = self.kk.Zmesh
            br, bz, bt = self.kk.rz2brzt(self.kk.Rmesh, self.kk.Zmesh, t_in=time)
            eqmat[m+n+1:m+n+1+m*n] = np.transpose(br[0]).flatten()
            eqmat[m+n+1+m*n:m+n+1+m*n*2] = np.transpose(bt[0]).flatten()
            eqmat[m+n+1+m*n*2:m+n+1+m*n*3] = np.transpose(bz[0]).flatten()
            eqmat[m+n+1+m*n*3:m+n+1+m*n*4] = np.transpose(self.kk.pfm[:, :, timeidx]).flatten()

        return m, n, eqmat

    def prdata(self, time=2.0):
        if self.sfn.status:
            k = self.nedta.data.shape[1]
        stridek = (k-1) / 150 + 1
        l = self.tedta.data.shape[1]
        stridel = (l-1) / 150 + 1
        prmat = np.zeros(600, dtype='double')
        neidx = np.argmin(abs(self.nedta.time - time))
        teidx = np.argmin(abs(self.tedta.time - time))
        prmat[0:k/stridek] = self.nedta.area[neidx, np.arange(0, k, stridek)]
        prmat[k/stridek:2*(k/stridek)] = self.nedta.data[neidx, np.arange(0, k, stridek)]*1e-19
        prmat[2*k/stridek:2*k/stridek+l/stridel] = self.tedta.area[teidx, np.arange(0, l, stridel)]
        prmat[2*k/stridek+l/stridel:2*k/stridek+2*l/stridel] = self.tedta.data[teidx, np.arange(0, l, stridel)]*1e-3

        return k/stridek, l/stridel, prmat

    def beamdata(self, time=2.0, gyr=5, beamdbl=np.zeros(50, dtype='double')):
        ''' Creates/overwrites entries relevant for beam data in dblinbeam array

        '''
        # pos, angle, width of gyrotrons 1-8 (ECRH1/ECRH2)
        if gyr < 1 or gyr > 8:
            beamdbl[0] = -1.0
            return beamdbl
        #beamdbl = np.zeros(50)
        ecidx = np.argmin(abs(self.ecpol[0, :] - time))
        ecpol = self.ecpol[gyr, ecidx]
        ector = self.ector[gyr]
        pol, tor = ecrh2tbm(ecpol, ector, gyr, self.shot)
        beamdbl[0] = self.ecfreq[gyr]
        beamdbl[1] = tor
        beamdbl[2] = pol
        ecidx = np.argmin(abs(self.ecpow[0, :] - time))
        beamdbl[23] = self.ecpow[gyr, ecidx]
        if gyr == 1 or gyr == 2:
            beamdbl[3] = 238.
            beamdbl[4] = 234.5
            beamdbl[5] = 0.0
            beamdbl[19] = 87.9
            beamdbl[20] = 87.9
            beamdbl[21] = 3.64
            beamdbl[22] = 3.64
        if gyr == 2:
            beamdbl[4] = 190.5
        if gyr == 3 or gyr == 4:
            beamdbl[3] = 231.1
            beamdbl[4] = 6.5 + 1.85977
            beamdbl[5] = 0.
            beamdbl[19] = 296.6
            beamdbl[20] = 296.6
            beamdbl[21] = 3.29
            beamdbl[22] = 3.29
        if gyr == 4:
            beamdbl[4] = 6.5 - 1.85977
        if gyr > 4:
            beamdbl[3] = 236.1
            beamdbl[4] = 168.5 + 2.79188
            beamdbl[5] = 32.0
            beamdbl[19] = 85.4
            beamdbl[20] = 85.4
            beamdbl[21] = 2.55
            beamdbl[22] = 2.55
        if gyr == 6:
            beamdbl[4] = 168.5 - 2.79188
        if gyr == 7:
            beamdbl[4] = 168.5 - 2.79188
            beamdbl[5] = -32.0
        if gyr == 8:
            beamdbl[5] = -32.0

        # need to clear y-coordinate due to TORBEAM using it (differently)
        beamdbl[4] = 0.0

        return beamdbl

    def default_inbeams(self):
        """Returns sensible initialization for most values of intinbeam & dblinbeam

          Routine does not prescribe starting position and/or angles as this
          normally depends on user's choice.
          Values are set such that typical (AUGD) runs should be possible with
          few additional parameters to be filled in. Current drive is switched on.
        """

        intinbeam = np.zeros(50, dtype=int)
        dblinbeam = np.zeros(50)

        intinbeam[49] = 0
        intinbeam[0] = 2
        intinbeam[1] = 2
        intinbeam[2] = -1
        intinbeam[3] = 1
        intinbeam[4] = 1
        intinbeam[5] = 2
        intinbeam[6] = 2
        intinbeam[7] = 30
        intinbeam[8] = 0
        intinbeam[9] = 0
        intinbeam[10] = 2
        intinbeam[11] = 1   # Farina: 1 Westerhof 0
        intinbeam[12] = 0
        intinbeam[13] = 1

        dblinbeam[0] = 1.4e11
        #...
        dblinbeam[6] = 10.2e13
        dblinbeam[7] = 1.0e13
        dblinbeam[8] = 2.0
        dblinbeam[9] = 0.5
        dblinbeam[10] = 25.0
        dblinbeam[11] = 1.0
        dblinbeam[12] = 2.0
        dblinbeam[13] = 2.0
        dblinbeam[14] = 3.e-7
        dblinbeam[15] = 3.e-7
        dblinbeam[16] = 1.0
        dblinbeam[17] = 1.5
        dblinbeam[18] = 1.5
        dblinbeam[23] = 0.
        dblinbeam[24] = 165.
        dblinbeam[25] = 60.
        dblinbeam[26] = -2.5
        dblinbeam[27] = 0.
        dblinbeam[28] = 0.
        dblinbeam[29] = 1.
        dblinbeam[30] = 1.5
        dblinbeam[31] = 1.
        dblinbeam[32] = 4.
        dblinbeam[33] = -1.
        dblinbeam[34] = 1.
        dblinbeam[35] = 0.96

        return intinbeam, dblinbeam

    def clc1time(self, time, gyr=None):
        if gyr is None:
            gyr = [5]
        self.readAll()
        m, n, eqdata = self.eqdata(time=float(time))
        k, l, prdata = self.prdata(time=float(time))
        intinbeam, dblinbeam = self.default_inbeams()
        tt1 = []
        tt2 = []
        ttt = []
        for g in gyr:
            dblinbeam = self.beamdata(time=float(time), gyr=g, beamdbl=dblinbeam)
            #rr, t1d, t1td, t2d, t2nd, ic, ibg, nv, vp = ctbm.call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata)
            result = ctbm.call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata)
            tt1.append(result[1])
            tt2.append(result[3])
            t2nd = result[4]
            if len(ttt) == 0:
                ttt = np.zeros_like(t2nd)
                ttt[0, :] = t2nd[0, :]
                ttt[1, :] = ttt[1, :] + t2nd[1, :]
                ttt[2, :] = ttt[2, :] + t2nd[2, :]
        return tt1, tt2, ttt

    def plotInput(self, fig1, fig2, time=2.0):
        m, n, eqdata = self.eqdata(time=float(time))
        k, l, prdata = self.prdata(time=float(time))
        ctbm.plotEq(eqdata=eqdata, eqm=m, eqn=n, plteq=fig1)
        ctbm.plotPr(prdata=prdata, prk=k, prl=l, pltpr=fig2)

    def plotOutput(self, fig1, fig3, fig4, tt1, tt2, ttt, gcol=None):
        if gcol is None:
            gcol = ['darkviolet', 'magenta', 'brown', 'coral', 'b', 'g', 'r', 'cyan']
        i = 0
        for t1d in tt1:
            fig1.plot(t1d[0, :]/100., t1d[1, :]/100., gcol[i % len(gcol)])
            fig1.plot(t1d[2, :]/100., t1d[3, :]/100., gcol[i % len(gcol)])
            fig1.plot(t1d[4, :]/100., t1d[5, :]/100., gcol[i % len(gcol)])
            i = i + 1
        i = 0
        for t2nd in tt2:
            fig3.plot(t2nd[0, :], t2nd[1, :], gcol[i % len(gcol)])
            fig4.plot(t2nd[0, :], t2nd[2, :], gcol[i % len(gcol)])
            i = i + 1

        fig3.set_title('P_EC vs rho')
        fig4.set_title('I_ECCD vs rho')
        fig3.plot(tt2[0][0, :], ttt[1, :], 'k')
        fig4.plot(tt2[0][0, :], ttt[2, :], 'k')

        return 0

def __main__():
    import matplotlib.pylab as plt

    print "demo program plots single/all ECRH2 gyrotrons power and current deposition"

    if len(sys.argv) < 3:
        print 'Need to identify dataset with shot+time'
        print ' e.g. ' + sys.argv[0] + ' 30594 5.43 '
        print '  add launcher number, if not default (5), like:'
        print '      ' + sys.argv[0] + ' <shot> <time> [launcher] '
        sys.exit(1)

    if len(sys.argv) > 3:
        gyrlist = [int(sys.argv[3])]
    else:
        gyrlist = [5, 7, 8, 6]

    plt1 = plt.subplot(2, 2, 1)
    plt2 = plt.subplot(2, 2, 2)
    plt3 = plt.subplot(2, 2, 3)
    plt4 = plt.subplot(2, 2, 4)

    dat = tbmd(int(sys.argv[1]))
    dat.readAll()
    dat.plotInput(plt1, plt2, time=float(sys.argv[2]))
    tt1, tt2, ttt = dat.clc1time(float(sys.argv[2]), gyr=gyrlist)
    dat.plotOutput(plt1, plt3, plt4, tt1, tt2, ttt)

    plt.show()

if __name__ == "__main__":
    __main__()
