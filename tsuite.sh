#!/bin/bash

function contains() {
    local n=$#
    local value=${!n}
    for ((i=1;i < $#;i++)) {
        if [ "${!i}" == "${value}" ]; then
            echo $i
            return 0
        fi
    }
    echo "-1"
    return 1
}

# include tests to be run
export RUN_THESE=""
export script=`which $0`
export dn=`dirname $script `
source ${dn}/test2run.inc

echo "# remove tests, you don't want to run not here, but in test2run.inc" > pytest.ini
echo \[pytest\] >> pytest.ini
echo python_functions=${RUN_THESE} >> pytest.ini

# some more defaults
export PYTHON=python
export HYPOTHESIS_PROFILE=full
export PYTHONPATH=${PYTHONPATH}

#exit 0

### IPP specific changes to the environment...
#
if [[ $DOMAIN == *"mpg.de" ]] ; then
  if [[ `uname -s` == "SunOS" ]] ; then
    if [[ `uname -r` == "5.11" ]] ; then
      if [[ `uname -i` == "i86pc" ]] ; then
        # Solaris 11, x86
        echo Solaris 11 x86
        export PATH=/afs/ipp/aug/ads-diags/sunx86_511/python2.7/bin:${PATH}
        export PYTHON_HOME=/afs/ipp/aug/ads-diags/sunx86_511/python2.7/
        export PYTHONPATH=/afs/ipp/aug/ads-diags/sunx86_510/python2.7/64/lib/python2.7/site-packages:${PYTHONPATH}
        export PYTHON=/afs/ipp/aug/ads-diags/sunx86_511/python2.7/bin/python2.7
      else
        # Solaris 11, SPARC
        echo Solaris 11, whooohoo!!!
        export PATH=/afs/ipp/aug/ads-diags/sun4x_511/python2.7/bin:${PATH}
        export PYTHON_HOME=/afs/ipp/aug/ads-diags/sun4x_511/python2.7/
        export PYTHONPATH=/afs/ipp/aug/ads-diags/sun4x_510/python2.7/64/lib/python2.7/site-packages:${PYTHONPATH}
        export PYTHON=/afs/ipp/aug/ads-diags/sun4x_511/python2.7/bin/python2.7
        echo No chance to test using python on this platform
        exit 0
      fi
    else
      if [[ `uname -r` == "5.10" ]] ; then
        if [[ `uname -i` == "i86pc" ]] ; then
          # Solaris 10, x86
          echo Solaris 10, x86
          export PATH=/afs/ipp/aug/ads-diags/sunx86_510/python2.7/bin:${PATH}
          export PYTHON_HOME=/afs/ipp/aug/ads-diags/sunx86_510/python2.7/
          export PYTHONPATH=/afs/ipp/aug/ads-diags/sunx86_510/python2.7/64/lib/python2.7/site-packages:${PYTHONPATH}
          export PYTHON=/afs/ipp/aug/ads-diags/sunx86_510/python2.7/bin/python2.7
       else
          # Solaris 10, SPARC
          echo Solaris 10, SPARC
          export PATH=/afs/ipp/aug/ads-diags/sun4x_510/python2.7/bin:${PATH}
          export PYTHON_HOME=/afs/ipp/aug/ads-diags/sun4x_510/python2.7/
          export PYTHONPATH=/afs/ipp/aug/ads-diags/sun4x_510/python2.7/lib/python2.7:${PYTHONPATH}
          export PYTHON=/afs/ipp/aug/ads-diags/sun4x_510/python2.7/bin/python2.7
          echo No chance to test using python on this platform
          exit 0
        fi
      else
        echo "unknown OS:"
        uname -a
        exit 1
      fi
    fi
  else
    if [ `uname -s` == "Linux" ] ; then
      # Linux OS, assume x86
      #echo Linux-x86
      PYTHON_VER=`python -c 'import sys; print(sys.version_info.major)'`
      if [[ $PYTHON_VER != "2" ]] ; then
        if [[ $PYTHON_VER != "3" ]] ; then
          PYTHON3=`which python3 2> /dev/null`
          if [[ $PYTHON3 == *"/python3" ]]; then
            export PYTHON=`which python3`
          else
            echo Need working python 2.x or 3.x
            exit 1
          fi
        fi
      fi
    else
      echo "unknown OS:"
      uname -a
      exit 1
    fi
  fi
fi

#$PYTHON --version
## end IPP specific changes dependent on hostOS

for arg in "$@"; do
  if [ $arg == help ] || [ $arg == --help ] ; then
    echo "Syntax: ./tsuite.sh [nocompile] [libdir=<...>] [full|debug|dev] [--verbose]"
    echo ""
    echo "Order of arguments is not relevant."
    echo "  --verbose      :   additional output during test(s)"
    echo "  nocompile      :   skip the library build process, just run tests"
    echo "  libdir=<dir>   :   look for libtorbeamX.so in directory: <dir>"
    echo "  [profile]      :   full (default), debug runs even more, dev runs less tests"
    echo "  --help/help    :   show this help page and exit"
    exit 0
  fi
  if [[ $arg == libdir=* ]]; then
    bdir=`echo $arg | cut -d= -f2`
  fi
done
if [[ "$bdir" == "" ]]; then
  bdir="../libtorbeam/build/lib/"
fi
echo Testing libs in directory: $bdir ===================
libdir=`echo $bdir | tr / ^ `

# nocompile switch skips compilation step
if [[ $(contains "$@" "nocompile") == "-1" ]] ; then
  mkdir -p $bdir
  cd $bdir/..
  cmake ..
  make
  cd -
fi

if [ $(contains "$@" "debug") != "-1" ]; then
  export HYPOTHESIS_PROFILE=debug
fi
if [ $(contains "$@" "dev") != "-1" ]; then
  export HYPOTHESIS_PROFILE=dev
fi

if [ $(contains "$@" "--verbose") != "-1" ]; then
  export VERBOSE=" --verbose "
else
  if [ $(contains "$@" "-v") != "-1" ]; then
    export VERBOSE=" --verbose "
  else
    export VERBOSE=
  fi
fi

echo "execute: ${PYTHON} -m pytest -s --hypothesis-show-statistics --hypothesis-profile" $HYPOTHESIS_PROFILE $VERBOSE --libdir $libdir
${PYTHON} -m pytest -s --hypothesis-show-statistics --hypothesis-profile $HYPOTHESIS_PROFILE $VERBOSE --libdir $libdir

if [[ $? == 0 ]] ; then
  echo " "
  if [[ "${HYPOTHESIS_PROFILE}" == "full" ]] ; then
    echo Everything passed, you may want to check in your stuff...
  else
    echo Everything passed, but you should use profile: full for maximum confidence
  fi
else
  echo " "
  echo Something failed, better double check the log...
  exit 1
fi
