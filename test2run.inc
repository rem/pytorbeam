# (Un-)Comment lines to allow only specific tests to be run

# all tests
export RUN_THESE="test_\*"

#export RUN_THESE="${RUN_THESE} test_cd_and_absorb_aug"
# just the cases meant for libtorbeamB.so
#export RUN_THESE="${RUN_THESE} test_cases_B"
# just reflectometry stuff
#export RUN_THESE="${RUN_THESE} test_refllib"
#export RUN_THESE="${RUN_THESE} test_rtlib"
#export RUN_THESE="${RUN_THESE} test_random_inputs_farina"
#export RUN_THESE="${RUN_THESE} test_random_inputs_westerhof"
#export RUN_THESE="${RUN_THESE} test_iter_rngfreq"
# NAG-dependent tests
#export RUN_THESE="test_nag_relativistic"
