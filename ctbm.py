#!/usr/bin/env python

import re
import os
import sys
import math
import time
from ctypes import cdll, c_int32, c_double, byref
from subprocess import check_output
import namelist as nl
#from IPython import embed
try:
    #    from scipy.interpolate import interp1d
    from scipy import pi
except Exception:
    #    def interp1d():
    #       print("here")
    pi = 3.14159

# prepare to load different flavors of torbeam.so
theOS = check_output(["uname", "-s"]).strip()
if theOS == 'SunOS':
    theVERS = check_output(["uname", "-r"]).strip()
    theOS = theOS + '_' + theVERS
theARCH = check_output(["uname", "-p"]).strip()

libtbma = None
libtbmb = None
libtbmc = None
libtbmd = None
libtbmr = None
try:
    import matplotlib.pyplot as plt
except Exception:
    print ("no matplotlib, will skip figure")
    plt = None
try:
    import numpy as np
except Exception as e:
    print ("Need numpy/scipy for proper execution, but ...")
    print (e)
    sys.exit(1)


def file2array(fname, tplidx=0):
    with open(fname) as f:
        strarr = np.array(f.readlines())
    strarr = strarr[tplidx:]
    nnum = len(re.split(r'\s+', strarr[0].strip()))
    mnum = strarr.size
    res = np.zeros([nnum, mnum])
    res[:, :] = np.NAN
    ic = 0
    for s in strarr:
        jc = 0
        for i in re.split(r'\s+', s.strip()):
            try:
                res[jc][ic] = float(i)
                jc += 1
            except Exception:
                jc += 1
        ic += 1
    return res


def readTopfile(fname):
    topf = {'status': -1, 'm': 0, 'n': 0, 'psep': 0.0, 'R': list(),
            'z': list(), 'Br': list(), 'Bt': list(), 'Bz': list(), 'psi': list()}
    try:
        ff = open(fname)
    except Exception as e:
        print ("error opening file: " + fname)
        print (e)
        return topf
    strarr = np.array(ff.readlines())
    ff.close()
    nextmode = ['m', 'psep', 'R', 'z', 'Br', 'Bt', 'Bz', 'psi', '']
    mode = ''
    numneeded = 0
    itermode = 0
    for i, s in enumerate(strarr):
        if 'Dummy' in s:
            mode = 'm'
            continue
        if 'X-coordinates' in s:
            mode = 'R'
            numneeded = topf['m']
            continue
        if 'Z-coordinates' in s:
            mode = 'z'
            numneeded = topf['n']
            continue
        if 'Number' in s:
            mode = 'm'
            continue
        if 'psi_sep' in s:
            mode = 'psep'
            continue
        if 'Radial' in s:
            mode = 'R'
            numneeded = topf['m']
            continue
        if 'Vertical' in s:
            mode = 'z'
            numneeded = topf['n']
            continue
        if 'B_R' in s.upper():
            mode = 'Br'
            numneeded = topf['m']*topf['n']
            continue
        if 'B_T' in s.upper():
            mode = 'Bt'
            numneeded = topf['m']*topf['n']
            continue
        if 'B_Z' in s.upper():
            mode = 'Bz'
            numneeded = topf['m']*topf['n']
            continue
        if 'psi' in s:
            mode = 'psi'
            numneeded = topf['m']*topf['n']
            continue
        if '+' in mode:
            mode = nextmode[nextmode.index(mode.strip('+'))+1]
            if nextmode.index(mode) == 2:
                numneeded = topf['m']
            if nextmode.index(mode) == 3:
                numneeded = topf['n']
            if nextmode.index(mode) > 3:
                numneeded = topf['m']*topf['n']
            continue
        if mode == '':
            if i == 0:
                mode = 'm'
                print ("start with mode m")
            continue
        if itermode > 1:
            topf['psep'] = 1.0
            continue
        # no keyword, must have numbers...
        number_strings = re.split(r'\s+', s.strip())
        if mode == 'm':  # 'm' special case
            topf['m'] = int(number_strings[0])
            topf['n'] = int(number_strings[1])
            mode = mode + '+'
            continue
        if mode == 'psep':  # 'psep' special case
            topf['psep'] = float(number_strings[2])
            mode = mode + '+'
            continue
        number_strings = re.split(' +|\n', s.strip())
        for st in number_strings:
            topf[mode].append(float(st))
        if len(topf[mode]) == numneeded:
            mode = mode+'+'
    topf['status'] = 0
    return topf


def plotEq(fname='topfile', eqdata=None, eqm=0, eqn=0, plotbt=0, plteq=None):
    if eqm == 0 or eqn == 0:
        topf = readTopfile(fname)
    else:
        try:
            topf = {'status': 0, 'm': eqm, 'n': eqn, 'psep': eqdata[0], 'R': list(
            ), 'z': list(), 'Br': list(), 'Bt': list(), 'Bz': list(), 'psi': list()}
            topf['R'] = eqdata[1:eqm+1].tolist()
            topf['z'] = eqdata[eqm+1:eqm+eqn+1].tolist()
            off = eqm+eqn+1
            topf['Br'] = eqdata[off:off+eqm*eqn].tolist()
            topf['Bt'] = eqdata[off+eqm*eqn:off+2*eqm*eqn].tolist()
            topf['Bz'] = eqdata[off+2*eqm*eqn:off+3*eqm*eqn].tolist()
            topf['psi'] = eqdata[off+3*eqm*eqn:off+4*eqm*eqn].tolist()
        except Exception:
            topf = {'status': -1}
    if topf['status']:
        print ("couldn't read data")
        sys.exit(topf['status'])

    rvec = np.array(topf['R'], dtype=float)
    zvec = np.array(topf['z'], dtype=float)
    br = np.reshape(np.array(topf['Br'], dtype=np.float64), [
                    topf['n'], topf['m']])
    bt = np.reshape(np.array(topf['Bt'], dtype=np.float64), [
                    topf['n'], topf['m']])
    bz = np.reshape(np.array(topf['Bz'], dtype=np.float64), [
                    topf['n'], topf['m']])
    psi = np.reshape(np.array(topf['psi'], dtype=np.float64), [
                     topf['n'], topf['m']])

    if plteq is None:
        fig = plt.figure()
        fig.clear()
        fig.suptitle('Input data')
        if plotbt > 0:
            plteq = plt.subplot(2, 2, 1)
        else:
            plteq = plt.subplot(1, 1, 1)

    if plotbt > 0:
        plt2 = plt.subplot(2, 2, 2)
        plt2.set_title('Br')
        plt2.contour(rvec, zvec, br, 20)
        plt3 = plt.subplot(2, 2, 3)
        plt3.set_title('Bz')
        plt3.contour(rvec, zvec, bz, 20)
        plt4 = plt.subplot(2, 2, 4)
        plt4.set_title('Bt')
        plt4.contour(rvec, zvec, bt, 20)
        print ('min', bt.min(), 'max', bt.max())

    psep = topf['psep']
    plteq.set_title('Equilibrium')
    plteq.contour(rvec, zvec, psi, 25)
    plteq.contour(rvec, zvec, psi, levels=[
                  psep], colors='k', linestyles='solid')
    plteq.set_title('psi over R,z grid')


def plotPr(prdata=None, prk=0, prl=0, pltpr=None):
    if prk == 0 or prl == 0 or prdata is None:
        return
    if pltpr is None:
        fig = plt.figure()
        fig.clear()
        fig.suptitle('Input data')
        pltpr = plt.subplot(1, 1, 1)

    pltpr.set_title('kinetic profiles vs rho')
    # densline =
    pltpr.plot(prdata[0:prk], prdata[prk:2*prk], label='ne [e19m**-3]')
    # templine =
    pltpr.plot(prdata[2*prk:2*prk+prl],
               prdata[2*prk+prl:2*(prk+prl)], label='Te [keV]')
    pltpr.legend(loc=0, fontsize='small')


def readInbeam(fname):
    """ Read all contained TORBEAM parameters from given file

    inputs: filename pointing to inbeam.dat
    sets default values first and overwrites all with values from
    given namelistfile.
    """
    intinbeam, dblinbeam = default_inbeams()
    try:
        if os.path.exists(fname):
            inb_f = nl.read_namelist_file(fname)
            inb_l = inb_f.groups['edata']
            # now assign namelist entries to correct inbeam cell(s)
            intinbeam[0] = inb_l['ndns'] if 'ndns' in inb_l else intinbeam[0]
            intinbeam[1] = inb_l['nte'] if 'nte' in inb_l else intinbeam[1]
            intinbeam[2] = inb_l['nmod'] if 'nmod' in inb_l else intinbeam[2]
            intinbeam[3] = inb_l['npow'] if 'npow' in inb_l else intinbeam[3]
            intinbeam[4] = inb_l['ncd'] if 'ncd' in inb_l else intinbeam[4]
            intinbeam[5] = inb_l['ianexp'] if 'ianexp' in inb_l else intinbeam[5]
            intinbeam[6] = inb_l['ncdroutine'] if 'ncdroutine' in inb_l else intinbeam[6]
            intinbeam[7] = inb_l['nprofv'] if 'nprofv' in inb_l else intinbeam[7]
            intinbeam[8] = inb_l['noout'] if 'noout' in inb_l else intinbeam[8]
            intinbeam[9] = inb_l['nrela'] if 'nrela' in inb_l else intinbeam[9]
            intinbeam[10] = inb_l['nmaxh'] if 'nmaxh' in inb_l else intinbeam[10]
            intinbeam[11] = inb_l['nabsroutine'] if 'nabsroutine' in inb_l else intinbeam[11]
            intinbeam[12] = inb_l['nastra'] if 'nastra' in inb_l else intinbeam[12]
            intinbeam[13] = inb_l['nprofcalc'] if 'nprofcalc' in inb_l else 0.  # ... 14 - 48 unused .intinbeam[]
            intinbeam[14] = inb_l['ncdharm'] if 'ncdharm' in inb_l else 0.
            intinbeam[15] = inb_l['npnts_extrap'] if 'npnts_extrap' in inb_l else 1.
            intinbeam[16] = inb_l['nfreq_extrap'] if 'nfreq_extrap' in inb_l else 1.
            intinbeam[17] = inb_l['if_rhot'] if 'if_rhot' in inb_l else 0.
            intinbeam[49] = inb_l['nrel'] if 'nrel' in inb_l else intinbeam[49]
            dblinbeam[0] = inb_l['xf'] if 'xf' in inb_l else dblinbeam[0]
            dblinbeam[1] = inb_l['xtordeg'] if 'xtordeg' in inb_l else dblinbeam[1]
            dblinbeam[2] = inb_l['xpoldeg'] if 'xpoldeg' in inb_l else dblinbeam[2]
            dblinbeam[3] = inb_l['xxb'] if 'xxb' in inb_l else dblinbeam[3]
            dblinbeam[4] = inb_l['xyb'] if 'xyb' in inb_l else dblinbeam[4]
            dblinbeam[5] = inb_l['xzb'] if 'xzb' in inb_l else dblinbeam[5]
            dblinbeam[6] = inb_l['xdns'] if 'xf' in inb_l else dblinbeam[6]
            dblinbeam[7] = inb_l['edgdns'] if 'edgdns' in inb_l else dblinbeam[7]
            dblinbeam[8] = inb_l['xe1'] if 'xe1' in inb_l else dblinbeam[8]
            dblinbeam[9] = inb_l['xe2'] if 'xe2' in inb_l else dblinbeam[9]
            dblinbeam[10] = inb_l['xte0'] if 'xte0' in inb_l else dblinbeam[10]
            dblinbeam[11] = inb_l['xteedg'] if 'xteedg' in inb_l else dblinbeam[11]
            dblinbeam[12] = inb_l['xe1t'] if 'xe1t' in inb_l else dblinbeam[12]
            dblinbeam[13] = inb_l['xe2t'] if 'xe2t' in inb_l else dblinbeam[13]
            dblinbeam[14] = inb_l['xrtol'] if 'xrtol' in inb_l else dblinbeam[14]
            dblinbeam[15] = inb_l['xatol'] if 'xatol' in inb_l else dblinbeam[15]
            dblinbeam[16] = inb_l['xstep'] if 'xstep' in inb_l else dblinbeam[16]
            dblinbeam[17] = inb_l['xtbeg'] if 'xtbeg' in inb_l else dblinbeam[17]
            dblinbeam[18] = inb_l['xtend'] if 'xtend' in inb_l else dblinbeam[18]
            dblinbeam[19] = inb_l['xryyb'] if 'xryyb' in inb_l else dblinbeam[19]
            dblinbeam[20] = inb_l['xrzzb'] if 'xrzzb' in inb_l else dblinbeam[20]
            dblinbeam[21] = inb_l['xwyyb'] if 'xwyyb' in inb_l else dblinbeam[21]
            dblinbeam[22] = inb_l['xwzzb'] if 'xwzzb' in inb_l else dblinbeam[22]
            dblinbeam[23] = inb_l['xpw0'] if 'xpw0' in inb_l else dblinbeam[23]
            dblinbeam[24] = inb_l['xrmaj'] if 'xrmaj' in inb_l else dblinbeam[24]
            dblinbeam[25] = inb_l['xrmin'] if 'xrmin' in inb_l else dblinbeam[25]
            dblinbeam[26] = inb_l['xb0'] if 'xb0' in inb_l else dblinbeam[26]
            dblinbeam[27] = inb_l['xdel0'] if 'xdel0' in inb_l else dblinbeam[27]
            dblinbeam[28] = inb_l['xdeled'] if 'xdeled' in inb_l else dblinbeam[28]
            dblinbeam[29] = inb_l['xelo0'] if 'xelo0' in inb_l else dblinbeam[29]
            dblinbeam[30] = inb_l['xeloed'] if 'xeloed' in inb_l else dblinbeam[30]
            dblinbeam[31] = inb_l['xq0'] if 'xq0' in inb_l else dblinbeam[31]
            dblinbeam[32] = inb_l['xqedg'] if 'xqedg' in inb_l else dblinbeam[32]
            dblinbeam[33] = inb_l['sgnm'] if 'sgnm' in inb_l else dblinbeam[33]
            dblinbeam[34] = inb_l['xzeff'] if 'xzeff' in inb_l else dblinbeam[34]
            dblinbeam[35] = inb_l['rhostop'] if 'rhostop' in inb_l else dblinbeam[35]
            dblinbeam[36] = inb_l['xzsrch'] / \
                100 if 'xzsrch' in inb_l else dblinbeam[36]
            # ... 37 - 49 unused ...
    except Exception:
        print ("something went terribly wrong, using mostly defaults for inbeam.dat")

    return intinbeam, dblinbeam


# TorBeaM modes of operation (AUG = normal = BEAM OUT VOL)
TBM_MODE_NORMAL = 0b000000111

TBM_MODE_BEAM = 0b000000001
TBM_MODE_OUT = 0b000000010
TBM_MODE_VOL = 0b000000100

TBM_MODE_RT = 0b000001000
TBM_MODE_REFL = 0b000010000
TBM_MODE_ITER = 0b000100000
TBM_MODE_RTPROF = 0b001000000

TBM_MODE_DEBUG = 0b100000000


def load_shared_object(mode, libdir=None):
    global libtbma
    global libtbmb
    global libtbmc
    global libtbmd
    global libtbmr
# load the shared object associated with chosen TBM mode, if not yet done so
    libbase = '../libtorbeam/lib/' if libdir is None else libdir+'/'
    try:
        if mode & TBM_MODE_RT:
            if libtbma is None:
                libtbma = cdll.LoadLibrary(libbase + 'libtorbeamA.so')
            tbm = libtbma
        if mode & TBM_MODE_RTPROF:
            if libtbmr is None:
                libtbmr = cdll.LoadLibrary(libbase + 'libtorbeamA1.so')
            tbm = libtbmr
        if mode & TBM_MODE_ITER:
            if libtbmc is None:
                libtbmc = cdll.LoadLibrary(libbase + 'libtorbeamC1.so')
            tbm = libtbmc
        if mode & TBM_MODE_REFL:
            if libtbmd is None:
                libtbmd = cdll.LoadLibrary(libbase + 'libtorbeamD.so')
            tbm = libtbmd
        if mode == TBM_MODE_NORMAL:
            if libtbmb is None:
                libtbmb = cdll.LoadLibrary(libbase + 'libtorbeamB.so')
            tbm = libtbmb
    except Exception as e:
        print ("something went wrong with loading the shared library\nOS thinks:")
        print (e)
        sys.exit(1)
        # end script, if anything goes wrong with loading the lib...so !
    return tbm


def call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata, mode=None, libdir=None):
    """Calls beam_() through a C-wrapper routine and executes TORBEAM with the given input data

    inputs: intinbeam, dblinbeam, m, n, eqdata, k, l, prdata
    are described in detail in input-output.pdf that comes with TORBEAM
    all data is converted to the corresponding type that the library expects
    (all floating point variables are treated as 64-bit (IEEE double))
    (all integer type variables are treated as 32-bit signed integer)

    outputs: t1dat, t1tdat, t2dat, t2new, icnt, ibgout, nprofw
    are also described in the input-output.pdf but some size parameters
    that are explicitely passed back by beam() are now contained implicitely
    by checking sizes of returned numpy arrays (e.g. t2new.shape[0]).
    """
    if mode is None:
        mode = TBM_MODE_NORMAL
    if mode & TBM_MODE_DEBUG:
        print ("mode:", bin(mode))
        print ("beam:", (mode & TBM_MODE_BEAM > 0))
        print ("out:", (mode & TBM_MODE_OUT > 0))
        print ("vol:", (mode & TBM_MODE_VOL > 0))
        print ("refl:", (mode & TBM_MODE_REFL > 0))
        print ("rt:", (mode & TBM_MODE_RT > 0))
        print ("iter:", (mode & TBM_MODE_ITER > 0))
        mode = mode & ~TBM_MODE_DEBUG
    tbm = load_shared_object(mode, libdir=libdir)
    mmax = 150
    if mode & TBM_MODE_RTPROF:
        mmax = 250
    if mode & TBM_MODE_ITER:
        mmax = 450
        eqdata[0] = 1
        al = pi*dblinbeam[2]/180.
        bt = pi*dblinbeam[1]/180.
        dblinbeam[1] = -math.atan(math.tan(bt)/math.cos(al))*180./pi
        dblinbeam[2] = math.asin(math.cos(bt)*math.sin(al))*180./pi
        dblinbeam[33] = 1
    if mode & TBM_MODE_REFL:
        mmax = 512
    eqmax = 1+mmax+mmax+4*mmax*mmax
    prmax = 2*mmax+2*mmax
    c_m = c_int32(m)
    c_n = c_int32(n)
    c_k = c_int32(k)
    c_l = c_int32(l)
    c_intinbeam = (c_int32 * 50)()
    c_fltinbeam = (c_double * 50)()
    c_eqdata = (c_double * eqmax)()
    c_prdata = (c_double * prmax)()
    c_rhoresult = (c_double * 20)()
    c_iend = c_int32(0)
    c_t1data = (c_double * 600000)()
    c_t1tdata = (c_double * 600000)()
    c_t2data = (c_double * 500000)()
    c_t2ndata = (c_double * 15000)()
    c_kend = c_int32(0)
    c_icnt = c_int32(0)
    c_ibgout = c_int32(0)
    c_nprofw = c_int32(intinbeam[7])
    nprofw_mem = 200
    c_volprofw = (c_double * nprofw_mem)()
    for i in range(nprofw_mem):
        c_volprofw[i] = c_double(0.0)
    c_reflout = (c_double * 800000)()
    for i in range(800000):
        c_reflout[i] = c_double(0.0)

    j = 0
    while j < 50:
        c_intinbeam[j] = c_int32(intinbeam[j])
        c_fltinbeam[j] = c_double(dblinbeam[j])
        c_prdata[j] = c_double(prdata[j])
        c_eqdata[j] = c_double(eqdata[j])
        j += 1
    while j < 2*(k+l):
        c_prdata[j] = c_double(prdata[j])
        c_eqdata[j] = c_double(eqdata[j])
        j += 1
    while j < 1+m+n+4*m*n:
        c_eqdata[j] = c_double(eqdata[j])
        if j < prmax:
            c_prdata[j] = c_double(0.0)
        j += 1
    while j < eqmax:
        c_eqdata[j] = c_double(0.0)
        j += 1
    if mode & TBM_MODE_REFL > 0:
        tbm.beam_(byref(c_intinbeam), byref(c_fltinbeam), byref(c_m), byref(c_n), byref(c_eqdata),
                  byref(c_k), byref(c_l), byref(c_prdata),
                  byref(c_rhoresult), byref(c_iend), byref(
                      c_t1data), byref(c_t1tdata),
                  byref(c_kend), byref(c_t2data), byref(
                      c_t2ndata), byref(c_icnt),
                  byref(c_ibgout), byref(c_nprofw), byref(c_volprofw), byref(c_reflout))
    else:
        if mode & TBM_MODE_NORMAL or mode & TBM_MODE_ITER:
            tbm.beam_(byref(c_intinbeam), byref(c_fltinbeam), byref(c_m), byref(c_n),
                      byref(c_eqdata), byref(c_k), byref(c_l), byref(c_prdata),
                      byref(c_rhoresult), byref(c_iend), byref(
                          c_t1data), byref(c_t1tdata),
                      byref(c_kend), byref(c_t2data), byref(
                          c_t2ndata), byref(c_icnt),
                      byref(c_ibgout), byref(c_nprofw), byref(c_volprofw))
        else:
            if mode & TBM_MODE_RT:
                tbm.beam_(byref(c_intinbeam), byref(c_fltinbeam), byref(c_m), byref(c_n),
                          byref(c_eqdata), byref(c_k), byref(
                              c_l), byref(c_prdata),
                          byref(c_rhoresult))
    rhoresult = np.zeros(20)
    for i in np.arange(0, 20):
        rhoresult[i] = np.float64(c_rhoresult[i])
# returning results from rt version
    if mode == TBM_MODE_RT:
        return rhoresult
# now transferring remaining data into python primitives
    iend = np.int32(c_iend.value)
    t1data = np.zeros([6, iend], dtype=np.float64)
    t1tdata = np.zeros([6, iend], dtype=np.float64)
    for i in np.arange(0, 6):
        for j in np.arange(0, iend):
            t1data[i][j] = np.float64(c_t1data[i*iend+j])
            t1tdata[i][j] = np.float64(c_t1tdata[i*iend+j])
    kend = np.int32(c_kend.value)
    t2data = np.zeros([5, kend], dtype=np.float64)
    for i in np.arange(0, 5):
        for j in np.arange(0, kend):
            t2data[i][j] = np.float64(c_t2data[i*kend+j])
    t2ndata = np.zeros([3, 5000], dtype=np.float64)
    for i in np.arange(0, 3):
        for j in np.arange(0, 5000):
            t2ndata[i][j] = np.float64(c_t2ndata[i*5000+j])
    icnt = np.int32(c_icnt.value)
    ibgout = np.int32(c_ibgout.value)
    nprofw = np.int32(c_nprofw.value)
    volprofw = np.zeros([2, nprofw], dtype=np.float64)
    for i in np.arange(2):
        for j in np.arange(0, nprofw):
            volprofw[i][j] = np.float64(c_volprofw[i*nprofw+j])
    # returning extra stuff for the reflectometry version
    if mode & TBM_MODE_REFL:
        reflout = np.zeros([8, iend], dtype=np.float64)
        for i in np.arange(0, 8):
            for j in np.arange(0, iend):
                reflout[i][j] = np.float64(c_reflout[i*iend+j])
        return rhoresult, t1data, t1tdata, t2data, t2ndata, icnt, ibgout, nprofw, volprofw, reflout
    # returning default results
    return rhoresult, t1data, t1tdata, t2data, t2ndata, icnt, ibgout, nprofw, volprofw


def default_inbeams():
    """Returns sensible initialization for most values of intinbeam & dblinbeam

    Routine does not prescribe starting position and/or angles as this
    normally depends on user's choice.
    Values are set such that typical (AUGD) runs should be possible with
    few additional parameters to be filled in. Current drive is switched on.
    """
    intinbeam = np.zeros(50, dtype=int)
    dblinbeam = np.zeros(50)
    intinbeam[0] = 2
    intinbeam[1] = 2
    intinbeam[2] = -1
    intinbeam[3] = 1  # intinbeam[4] = 0
    intinbeam[5] = 2
    intinbeam[6] = 2
    # intinbeam[8] = 0  # intinbeam[9] = 0  # intinbeam[49] = 0
    intinbeam[7] = 25
    # intinbeam[11] = 0 # intinbeam[12] = 0 # intinbeam[13] = 0
    intinbeam[10] = 2
    dblinbeam[0] = 140.0e9  # [1-5] ... stay 0
    dblinbeam[6] = 10.2e13
    dblinbeam[7] = 1.0e13
    dblinbeam[8] = 2.0
    dblinbeam[9] = 0.5
    dblinbeam[10] = 25.0
    dblinbeam[11] = 1.0
    dblinbeam[12] = 2.0
    dblinbeam[13] = 2.0
    dblinbeam[14] = 3.e-7
    dblinbeam[15] = 3.e-7
    dblinbeam[16] = 1.0
    dblinbeam[17] = 1.5
    dblinbeam[18] = 1.5
    dblinbeam[19] = 85.4
    dblinbeam[20] = 85.4
    dblinbeam[21] = 2.55
    dblinbeam[22] = 2.55
    dblinbeam[23] = 1.
    dblinbeam[24] = 165.
    dblinbeam[25] = 60.
    dblinbeam[26] = 2.5  # dblinbeam[27] = 0. # dblinbeam[28] = 0.
    dblinbeam[29] = 1.
    dblinbeam[30] = 1.5
    dblinbeam[31] = 1.
    dblinbeam[32] = 4.
    dblinbeam[33] = -1.
    dblinbeam[34] = 1.  # dblinbeam[35] = 0
    return intinbeam, dblinbeam


def readTopfile3(fname):
    """Reads TORBEAM topfile and returns m, n, eqdata for use in libtorbeam

    Takes the argument of a string, which should point to a filename resembling
    a valid topfile for use in TORBEAM
    File is being read and contained data is properly inserted into variables
    that will be returned to the caller. Output consists of m, n, eqdata[...]
    """
    topf = readTopfile(fname)
    if topf['status'] != 0:
        return -1, -1, np.array([0., 0, 0])
    m = topf['m']
    n = topf['n']
    longlist = [topf['psep']]+topf['R']+topf['z'] + \
        topf['Br']+topf['Bt']+topf['Bz']+topf['psi']
    eqdata = np.array(longlist)
    return m, n, eqdata


def readProfile3(fname):
    """Read a plasma profile in the TORBEAM kinetic data format from file fname"

    Takes a filename and reads first line as number of data tupels, which are
    then read and inserted into array to be returned. Length of array contains
    number of points implicitely.
    """
    f = open(fname, "r")
    try:
        f.fileno()
    except ValueError:
        return -1
    line = f.readline().strip()
    arr = line.split()
    l = int(arr[0])
    data = np.zeros(2*int(arr[0]))
    i = 0
    while i < l:
        line = f.readline().strip()
        arr = line.split()
        data[i] = np.float64(arr[0].replace('D','E'))
        data[i+l] = np.float64(arr[1].replace('D','E'))
        i += 1
    f.close()
    return data


def readPrdata(fname_ne, fname_te):
    """Read two plasma profiles, stacking them together as expected by libtorbeam in prdata

    Makes use of read profile to combine contents of two files, usually ne.dat and Te.dat
    to form the prdata array necessary to run TORBEAM.
    """
    nele = readProfile3(fname_ne)
    tele = readProfile3(fname_te)
    largelist = list(nele) + list(tele)
    prdata = np.array(largelist, dtype=np.float64)
    return int(len(nele)/2), int(len(tele)/2), prdata


def writeDump(fname, intinbeam, dblinbeam, k, l, prdata, m, n, eqdata):
    with open(fname, 'w+') as f:
        for i in intinbeam:
            f.write(str(i)+' ')
        f.write('\n')
        for i in dblinbeam:
            f.write(str(i)+' ')
        f.write('\n')
        f.write(str(k)+' '+str(l)+' ')
        for i in prdata:
            f.write(str(i)+' ')
        f.write('\n')
        f.write(str(m)+' '+str(n)+' ')
        for i in eqdata:
            f.write(str(i)+' ')
        f.write('\n')


def readDump(fname):
    """Read a complete set of data into all input variables that TORBEAM takes

    Used during debugging and left for potential use in the future.
    """
    f = open(fname, "r")
    try:
        f.fileno()
    except ValueError:
        return -1, -1, -1, -1, -1, -1, -1, -1
    f.readline()
    data = f.readline().strip().split()
    intinbeam = np.zeros(50, dtype=np.int32)
    i = 0
    while i < 50:
        intinbeam[i] = np.int32(data[i])
        i += 1
    f.readline()
    data = f.readline().strip().split()
    dblinbeam = np.zeros(50)
    i = 0
    while i < 50:
        dblinbeam[i] = np.float64(data[i].replace('D','E'))
        i += 1
    f.readline()
    data = f.readline().strip().split()
    k = np.int32(data[0])
    l = np.int32(data[1])
    f.readline()
    prdata = np.zeros(600)
    i = 0
    while i < 600:
        data = f.readline().strip().split()
        j = 0
        while j < 10:
            prdata[i] = np.float64(data[j].replace('D','E'))
            i += 1
            j += 1
    f.readline()
    data = f.readline().strip().split()
    m = np.int32(data[0])
    n = np.int32(data[1])
    f.readline()
    eqdata = np.zeros(90301)
    i = 0
    while i < 90301:
        data = f.readline().strip().split()
        j = 0
        while j < 10 and i < 90301:
            eqdata[i] = np.float64(data[j].replace('D','E'))
            i += 1
            j += 1

    return intinbeam, dblinbeam, k, l, prdata, m, n, eqdata


def writeTopfile(m, n, eqdata, fname='topfile'):
    def _fmt(val):
        """ data formatter for gfiles, which doesnt follow normal conventions..."""
        try:
            temp = '0{: 0.8E}'.format(float(val)*10)
            out = ''.join([temp[1], temp[0], temp[3], temp[2], temp[4:]])
        except TypeError:
            out = ''
            idx = 0
            for i in val:
                out += _fmt(i)
                idx += 1
                if idx == 5:
                    out += '\n'
                    idx = 0
            if idx != 0:
                out += '\n'
        return out

    def _fmt2(val):
        """ data formatter for topfiles..."""
        out = ''
        idx = 0
        if isinstance(val, np.ndarray):
            for i in np.arange(0, val.size):
                out += _fmt(val.flat[i])+' '
                idx += 1
                if idx == 8:
                    out += '\n'
                    idx = 0
        else:
            for i in val:
                out += _fmt(i)+' '
                idx += 1
                if idx == 8:
                    out += '\n'
                    idx = 0
        if idx != 0:
            out += '\n'
        return out

    with open(fname, 'w') as f:
        f.write('Number of radial and vertical grid points\n')
        f.write('%d %d\n' % (m, n))
        f.write('Inside and Outside radius and psi_sep\n')
        f.write('%g %g %g\n' % (eqdata[1], eqdata[m], eqdata[0]))
        f.write('Radial coordinates\n')
        f.write(_fmt2(eqdata[1:1+m]))
        f.write('Vertical coordinates\n')
        f.write(_fmt2(eqdata[1+m:1+m+n]))
        f.write('B_r values\n')
        f.write(_fmt2(eqdata[1+m+n:1+m+n+m*n]))
        f.write('B_t values\n')
        f.write(_fmt2(eqdata[1+m+n+m*n:1+m+n+m*n*2]))
        f.write('B_z values\n')
        f.write(_fmt2(eqdata[1+m+n+m*n*2:1+m+n+m*n*3]))
        f.write('psi values\n')
        f.write(_fmt2(eqdata[1+m+n+m*n*3:1+m+n+m*n*4]))


def writeKinetic(k, l, prdata, fnamene='ne.dat', fnameTe='Te.dat'):
    with open(fnamene, 'w') as f:
        f.write("  %d\n" % (k))
        for i in np.arange(0, k):
            f.write("%g %g\n" % (prdata[i], prdata[i+k]))
    with open(fnameTe, 'w') as f:
        f.write("  %d\n" % (l))
        for i in np.arange(0, l):
            f.write("%g %g\n" % (prdata[2*k+i], prdata[2*k+l+i]))


def writeInbeam(intinbeam, dblinbeam, fname='inbeam.dat'):
    with open(fname, 'w') as f:
        f.write("&edata\n")
        f.write(
            "! ODE solver params: ---------------------------------------------------\n")
        f.write(" xrtol = %g,           ! required rel. error\n" %
                dblinbeam[14])
        f.write(" xatol = %g,           ! required abs. error\n" %
                dblinbeam[15])
        f.write(" xstep = %f,           ! integration step\n" % dblinbeam[16])
        f.write(" npow = %d,      ! power absorption on(1)/off(0)\n" %
                intinbeam[3])
        f.write(" ncd = %d,       ! current drive calc. on(1)/off(0)\n" %
                intinbeam[4])
        f.write(
            " ianexp = %d,      ! analytic (1) or experimental (2) equilibrium\n" % intinbeam[5])
        f.write(
            " ndns = %d,   ! analytic (1) or interpolated (2) density profiles\n" % intinbeam[0])
        f.write(
            " nte = %d,      ! analyt. (1) or interp. (2) electron temp. prof.\n" % intinbeam[1])
        f.write(
            " ncdroutine = %d,     ! 0->Curba, 1->Lin-Liu, 2->Lin-Liu+momentum conservation\n" %
            intinbeam[6])
        f.write(" nshot = 0,            ! shot number\n")
        f.write(" xtbeg = %f,           ! tbegin\n" % dblinbeam[17])
        f.write(" xtend = %f,           ! tende\n" % dblinbeam[18])
        f.write(" nmaxh = %d,           ! maximum number of harmonics\n" %
                intinbeam[10])
        f.write(
            " nabsroutine = %d,     ! absorbtion calculated by (0) Westerhof (1) Farina\n" %
            intinbeam[11])
        f.write(" noout = %d,           ! suppress console output\n" %
                intinbeam[8])
        f.write(
            " nprofcalc = %d,       ! profiles ala default (0), ala Maj (1)\n" % intinbeam[13])
        f.write(
            " nrela = %d,           ! weakly(0) or fully(1) relativistic absorption\n" %
            intinbeam[9])
        f.write(
            " nrel = %d,            ! 1 -> rel. mass correction for refl\n" % intinbeam[49])
        f.write(
            " nprofv = %d,          ! number of radial points for volume prof.\n" % intinbeam[7])
        f.write(
            " xzsrch = %d,          ! z-position for starting search for mag. axis\n" %
            dblinbeam[36])
        f.write(
            "! Beam params: ---------------------------------------------------------\n")
        f.write(
            " nmod = %d,          ! mode selection: O-mode (1), X-mode (-1)\n" % intinbeam[2])
        f.write(" xf = %g,            ! wave frequency om=2*pi*xf\n" %
                dblinbeam[0])
        f.write(
            " xtordeg = %f,           ! geom. optics injection angle\n" % dblinbeam[1])
        f.write(
            " xpoldeg = %f,           ! geom. optics injection angle\n" % dblinbeam[2])
        f.write(
            " xxb = %f,       ! beam launching parameter for tracing calc.\n" % dblinbeam[3])
        f.write(
            " xyb = %f,       ! beam launching parameter for tracing calc.\n" % dblinbeam[4])
        f.write(
            " xzb = %f,       ! beam launching parameter for tracing calc.\n" % dblinbeam[5])
        f.write(" xryyb = %f,             ! initial principal curvature\n" %
                dblinbeam[19])
        f.write(" xrzzb = %f,             ! initial principal curvature\n" %
                dblinbeam[20])
        f.write(" xwyyb = %f,             ! initial principal width\n" %
                dblinbeam[21])
        f.write(" xwzzb = %f,             ! initial principal width\n" %
                dblinbeam[22])
        f.write(" xpw0 = %f,      ! initial beam power [MW]\n" % dblinbeam[23])
        f.write(
            "! Plasma params: -------------------------------------------------------\n")
        f.write(" xrmaj = %f,       ! major radius\n" % dblinbeam[24])
        f.write(" xrmin = %f,             ! minor radius\n" % dblinbeam[25])
        f.write(" xzeff = %g,       ! Zeff value\n" % dblinbeam[34])
        f.write(
            " xb0 = %f,       ! central toroidal magnetic field [T]\n" % dblinbeam[26])
        f.write(
            " xdns = %g,      ! electron density [cm**(-3)]\n" % dblinbeam[6])
        f.write(
            " edgdns = %g,     ! electron density [cm**(-3)]\n" % dblinbeam[7])
        f.write(" xe1 = %f,       ! exponent in the density profile\n" %
                dblinbeam[8])
        f.write(" xe2 = %f,       ! exponent in the density profile\n" %
                dblinbeam[9])
        f.write(
            " xte0 = %f,      ! electron temperature [keV]\n" % dblinbeam[10])
        f.write(
            " xteedg = %f,    ! electron temperature [keV]\n" % dblinbeam[11])
        f.write(" xe1t = %f,      ! exponent in the temperature profile\n" %
                dblinbeam[12])
        f.write(" xe2t = %f,      ! exponent in the temperature profile\n" %
                dblinbeam[13])
        f.write(" xdel0 = %f,             ! Shafranov shift on the axis\n" %
                dblinbeam[27])
        f.write(" xdeled = %f,            ! Shafranov shift on the edge\n" %
                dblinbeam[28])
        f.write(" xelo0 = %f,             ! elongation on the axis\n" %
                dblinbeam[29])
        f.write(" xeloed = %f,            ! elongation on the edge\n" %
                dblinbeam[30])
        f.write(" xq0 = %f,       ! safety factor on the axis\n" %
                dblinbeam[31])
        f.write(" xqedg = %f      ! safety factor on the edge\n" %
                dblinbeam[32])
        f.write(" sgnm = %f      ! flux orientation\n" % dblinbeam[33])
        f.write(" xzeff = %f      ! global Zeff\n" % dblinbeam[34])
        f.write("/\n")


def makeBigPlot(m, n, eqdata, k, l, prdata, rhoresult, t1data, t2n_data, intinbeam, dblinbeam):
    plt.figure(figsize=(10, 8))
    plt1 = plt.subplot(2, 2, 1)
    plotEq(eqm=m, eqn=n, eqdata=eqdata, plotbt=0, plteq=plt1)
    plt1.plot(t1data[0, :]/100., t1data[1, :]/100., 'b')
    plt1.plot(t1data[2, :]/100., t1data[3, :]/100., 'b')
    plt1.plot(t1data[4, :]/100., t1data[5, :]/100., 'b')
    plt1.plot([rhoresult[1]/100.], [rhoresult[2]/100.], 'k+', mew=5, ms=14)

    if intinbeam[6] > 9:
        print ("Recalculating deposition in rho")
        rhoresult[0] = t2n_data[0, abs(t2n_data[2, :]).argmax()]
    textstr = r"$\rho=%.2f$\n$\mathrm{R}=%.2f m$\n$\mathrm{z}=%.2f m$" %  \
        (round(rhoresult[0], 2), round(
            rhoresult[1]/100, 2), round(rhoresult[2]/100, 2))
    props = dict(boxstyle='round', facecolor='white')
    plt1.text(0.95, 0.05, textstr, transform=plt1.transAxes,
              fontsize=14, horizontalalignment='right', bbox=props)
    plt2 = plt.subplot(2, 2, 3)
    plt2.plot(t2n_data[0, :], t2n_data[1, :], 'b')
    plt2.set_title('P_EC vs rho')
    plt3 = plt.subplot(2, 2, 4)
    plt3.plot(t2n_data[0, :], t2n_data[2, :], 'b')
    plt3.set_title('I_ECCD vs rho')
    plt4 = plt.subplot(2, 2, 2)
    plt4.set_title('kinetic profiles vs rho')
    plotPr(prdata=prdata, prk=k, prl=l, pltpr=plt4)
    if intinbeam[11] == 0:
        abstr = 'Westerhof'
    if intinbeam[11] == 1:
        abstr = 'Farina'
    if intinbeam[13] == 0:
        prstr = 'standard'
    if intinbeam[13] == 1:
        prstr = 'a la Maj'
    textstr = 'tor=%.2f pol=%.2f\nabsorb:%s\nprofiles:%s' % (
        round(dblinbeam[1], 2), round(dblinbeam[2], 2), abstr, prstr)
    plt2.text(0.55, 0.77, textstr, transform=plt1.transAxes,
              fontsize=14, horizontalalignment='right', bbox=props)
    plt.tight_layout()
    plt.show()


def theMain(mode=None, libdir=None):
    fname = '/topfile'
    nename = '/ne.dat'
    tename = '/Te.dat'
    ibname = '/inbeam.dat'
    verbose = 0
    noplot = 0
    if '--rt' in sys.argv:
        mode = TBM_MODE_RT
        sys.argv.remove('--rt')
    if '-verbose' in sys.argv:
        verbose = 1
        sys.argv.remove('-verbose')
    if '-v' in sys.argv:
        verbose = 1
        sys.argv.remove('-v')
    if '-noplot' in sys.argv:
        noplot = 1
        sys.argv.remove('-noplot')
    if 'noplot' in sys.argv:
        noplot = 1
        sys.argv.remove('noplot')
    dodebug = 0
    if '-debug' in sys.argv:
        dodebug = 1
        sys.argv.remove('-debug')
    if 'debug' in sys.argv:
        dodebug = 1
        sys.argv.remove('debug')
    if len(sys.argv) > 1:
        if os.path.isdir(sys.argv[1]):
            path_to_input = sys.argv[1]
        else:
            path_to_input = ''
            fname = sys.argv[1]
        if len(sys.argv) > 2:
            nename = sys.argv[2]
        if len(sys.argv) > 3:
            tename = sys.argv[3]
        if len(sys.argv) > 4:
            ibname = sys.argv[4]
    else:
        print (sys.argv[0], " [ [path] | [topfile] [ne.dat] [Te.dat] [inbeam.dat] ]")
        print (" ")
        path_to_input = './'
    if 'tests' in sys.argv:
        sys.argv.remove('tests')
        print ("Testing now in separate python file !!")
        sys.exit(1)
    fname = path_to_input+fname
    nename = path_to_input+nename
    tename = path_to_input+tename
    ibname = path_to_input+ibname
    if os.path.exists(fname) is False:
        print ("can't find topfile at: "+fname)
        sys.exit(1)
    if os.path.exists(nename) is False:
        print ("can't find ne.dat at: "+nename)
        sys.exit(1)
    if os.path.exists(tename) is False:
        print ("can't find Te.dat at: "+tename)
        sys.exit(1)
    if os.path.exists(ibname) is False:
        print ("can't find inbeam.dat at: "+ibname)
        sys.exit(1)
    m, n, eqdata = readTopfile3(fname)
    print ("loaded:", fname)
    k, l, prdata = readPrdata(nename, tename)
    print ("loaded:", nename, tename)
    intinbeam, dblinbeam = readInbeam(ibname)
    print ("loaded:", ibname, "\nCalling TORBEAM library...")
    if mode is None:
        mode = TBM_MODE_NORMAL
    if 'iter' in path_to_input:
        mode = mode | TBM_MODE_ITER
    if dodebug:
        mode = mode | TBM_MODE_DEBUG
    if 'refl' in path_to_input:
        mode = mode | TBM_MODE_REFL
        writeInbeam(intinbeam, dblinbeam, fname='echo.dat')
    if verbose:
        writeDump('DUMP_'+re.findall(r'\w+:\w+:\w+', time.ctime())[0].replace(':', ''),
                  intinbeam, dblinbeam, k, l, prdata, m, n, eqdata)
    results = call_torbeam(intinbeam, dblinbeam, m, n,
                           eqdata, k, l, prdata, mode=mode, libdir=libdir)
    if mode & TBM_MODE_RT == 0:
        print("rho[0]: %f exit code: %d" % (results[0][0], int(results[0][19])))
    else:
        print("rho[0]: %f exit code: %d" % (results[0], int(results[19])))
        noplot = 1
    if noplot == 0 and plt is not None:
        makeBigPlot(m, n, eqdata, k, l, prdata,
                    results[0], results[1], results[4], intinbeam, dblinbeam)


if __name__ == "__main__":
    for arg in sys.argv:
        if arg.startswith('libdir='):
            libdir = arg[7:]
            sys.argv.remove(arg)
            theMain(libdir=libdir)
            sys.exit(0)
    theMain()
