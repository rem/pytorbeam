#!/usr/bin/env python

import sys
sys.path.append('/u/rem/soft/pyddww')
from ddww import dd
from math import exp
from numpy import zeros

def neprof(coeff, lam):
    coeff[1] = coeff.sum()
    rho = [i/30. for i in range(40)]
    nep = zeros(40)
    nep = [ coeff[1] * exp(-lam * (rho - 1)) if rho > 1 else rho ]

def getTeprof(shot, diag='TBM', experiment='AUGD'):
    sf = dd.shotfile()
    sf.open(diag, shot, experiment=experiment)


if __name__ == "__main__":
    main()