#!/usr/bin/env python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

import os
import sys
import numpy as np
import scipy as sp
from itertools import chain
try:
  import eqtools as eqt
except Exception as e:
  print ("Problem loading equilibrium tools")
  print ("OS thinks:")
  print (e)
  sys.exit(1)

from ctbm import *
from tbmdata_tcv import *

if len(sys.argv) < 2:
    print('Need a TORAY dataset, identify with shot+time')
    print(' e.g. '+sys.argv[0]+' 53051t1.2 ')
    print('  add launcher number, if not default (1), like:')
    print('      '+sys.argv[0]+' 53051t1.2  4 ')
    sys.exit(1)

writefiles = 1  # if writefiles is true (1), standard TORBEAM input files will be written to cwd

flt = sys.argv[1]
uname = os.environ['USER']

dlist = os.listdir('/tmp/'+uname)
base = ''
for f in dlist:
    try:
        pos = f.index(flt)
        base = f[pos:]
    except Exception:
        continue

if len(base) == 0:
    print('no file matching (*'+sys.argv[1]+'*) found under /tmp/'+uname)
    sys.exit(1)

gfilename = '/tmp/'+uname+'/eqdsk.'+base
netefilename = '/tmp/'+uname+'/profiles_'+base

sshot = gfilename.split('/')[3][6:11]
stime = gfilename.split('/')[3][12:]

shot = int(sshot)
time = float(stime)

ff = open(netefilename)
strarr = np.array(ff.readlines())

for i in strarr:
    if 'enec' in i:
        continue
    if 'njene' in i:
        i = i.strip()
        i = i.replace(',',';')
        exec (i)
        renein = np.zeros(njene, dtype=float)
        ene = np.zeros(njene, dtype=float)
        continue
    if 'gamte' in i:
        continue
    if 'njte' in i:
        i = i.strip()
        i = i.replace(',',';')
        exec (i)
        rtein = np.zeros(njte, dtype=float)
        te = np.zeros(njte, dtype=float)
        continue
    i = i.replace("(", "[") 
    i = i.replace(")", "]")
    i = i.replace(",1]", "]")
    i = i.replace("]", "-1]")
    i = i.replace(",", ";")
    i = i.strip()
    exec (i)

# WRITE ne.dat and Te.dat (for reference) 
if writefiles:
    nefiler = open('ne.dat', 'wb') 
    header = '  '+str(njene)+'\n'
    nefiler.write(header)
    for n,i in enumerate(renein):
        nefiler.write(str(i)+' '+str(ene[n]/1e13)+'\n')    
    nefiler.close()

    tefiler = open('Te.dat', 'wb') 
    header = '  '+str(njte)+'\n'
    tefiler.write(header)
    for n,i in enumerate(rtein):
        tefiler.write(str(i)+' '+str(te[n])+'\n')    
    tefiler.close()

# make some MDSplus calls for equilibrium data via matlab
eq = eqt.eqdskreader.EqdskReader(gfile=gfilename)
tt = time
eq._btaxv = np.array([btvac_wrap_tcv(shot, tt)])
eq._time = np.array([time])
m = eq.getRGrid().size
n = eq.getZGrid().size
R = eq.getRGrid()
z = eq.getZGrid()
br = eq.rz2BR(R, z, tt, make_grid=True)
bt = eq.rz2BT(R, z, tt, make_grid=True)
if eq._btaxv < 0:
    bt = -abs(bt)
else:
    bt = abs(bt)
bz = eq.rz2BZ(R, z, tt, make_grid=True)
psi = eq.rz2psi(R, z, tt, make_grid=True)

if m > 40 or n > 70:
    target_m = 39
    target_n = 69
else:
    target_m = 0
    target_n = 0

# change to different grid resolution, if needed
if target_m > 0 and target_n > 0 and ( target_m != m or target_n != n):
    print ('interpolating equilibrium grid') # from (%(m)dx%(n)d) to (%(tm)dx%(tn)d)\n' % { 'm': m, 'n': n, 'tm': target_m, 'tn': target_n })
    Rn = np.linspace(start=R[0], stop=R[-1], num=target_m)
    zn = np.linspace(start=z[0], stop=z[-1], num=target_n)
    f = sp.interpolate.interp2d(R, z, br, kind='cubic', copy=True)
    br = f(Rn, zn)
    f = sp.interpolate.interp2d(R, z, bt, kind='cubic', copy=True)
    bt = f(Rn, zn)
    f = sp.interpolate.interp2d(R, z, bz, kind='cubic', copy=True)
    bz = f(Rn, zn)
    f = sp.interpolate.interp2d(R, z, psi, kind='cubic', copy=True)
    psi = f(Rn, zn)
    R = Rn
    z = zn
    m = target_m
    n = target_n

# chose launcher number
if len(sys.argv) > 2:
    lnum = int(sys.argv[2])
else:
    lnum = 1

# write out topfile in correct format
if writefiles:
    topfiler = open('topfile', 'wb')
    header = 'Number of radial and vertical grid points in ' + str(shot) + ' ' + str(eq._time[0])+'\n'
    topfiler.write(header)
    line = str(m) + ' ' + str(n)+'\n'
    topfiler.write(line)
    topfiler.write('Inside and Outside radius and psi_sep\n')
    topfiler.write(str(eq.getRGrid()[0])+' '+str(eq.getRGrid()[eq.getRGrid().size-1]))
    topfiler.write(' '+str(eq.getFluxLCFS()[0])+'\n')
    topfiler.write('Radial coordinates\n')
    topfiler.write(eqt.filewriter._fmt2(R))
    topfiler.write('Vertical coordinates\n')
    topfiler.write(eqt.filewriter._fmt2(z))
    topfiler.write('B_r values\n')
    topfiler.write(eqt.filewriter._fmt2(br))
    topfiler.write('B_t values\n')
    topfiler.write(eqt.filewriter._fmt2(bt))
    topfiler.write('B_z values\n')
    topfiler.write(eqt.filewriter._fmt2(bz))
    topfiler.write('psi values\n')
    topfiler.write(eqt.filewriter._fmt2(psi))
    topfiler.close()

    write_inbeam_tcv(shot, time, lnum) 

ene = ene/1.e13

eqdata = np.array([0.0]+R.tolist()+z.tolist()+list(chain.from_iterable(br))+ \
  list(chain.from_iterable(bt))+list(chain.from_iterable(bz))+list(chain.from_iterable(psi)))
prdata = np.array(renein.tolist()+ene.tolist()+rtein.tolist()+te.tolist())
k = renein.size
l = rtein.size
intinbeam, dblinbeam = default_inbeams()

# get beam params from matlab script
res = beam_param_tcv(shot, eq._time[0], lnum)
s = res.replace('!','#').replace(',','').split('\n')
for i in s:
    exec (i.strip())
dblinbeam[0:6] = [ xf, xtordeg, xpoldeg, xxb, xyb, xzb-xzaxis ]
dblinbeam[19:26] = [ xryyb, xrzzb, xwyyb, xwzzb, xpw0, xrmaj, xrmin ]

# chose profile calc & absorption model
intinbeam[11] = 1
intinbeam[13] = 1

# Call the library
rhoresult, t1data, t1tdata, t2data, t2n_data, icnt, ibgout, nv, volprof  = \
      call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata)

# Plot 
plt1 = plt.subplot(2,2,1)
plotEq(eqm=m, eqn=n, eqdata=eqdata, plotbt=0, plteq=plt1)

plt1.plot(t1data[0,:]/100., t1data[1,:]/100., 'b')
plt1.plot(t1data[2,:]/100., t1data[3,:]/100., 'b')
plt1.plot(t1data[4,:]/100., t1data[5,:]/100., 'b')

plt2 = plt.subplot(2,2,3)
plt2.plot(t2n_data[0,:], t2n_data[1,:], 'b')
plt2.set_title('P_EC vs rho')
plt3 = plt.subplot(2,2,4)
plt3.plot(t2n_data[0,:], t2n_data[2,:], 'b')
plt3.set_title('I_ECCD vs rho')

plt4 = plt.subplot(2,2,2)
plt4.set_title('kinetic profiles vs rho')

plt4.plot(prdata[0:k], prdata[k:2*k])
plt4.plot(prdata[2*k:2*k+l], prdata[2*k+l:2*k+2*l])

plt.show()

