function [r_phi0_out,z_phi0_out,r1_out,z1_out,rc_out,zc_out,thetphi_L_out,thetphi_tor_out,raysbylauncher_out,powers_out,fname_out,z_axis_out,varargout] = ...
  toray_raygeom_me(shot,times_in,freqarr,wrfoarr,raysbylauncher_in,liuqe_vers,save_dir,isave, ...
  ipowers_tree,isignIB,varargin)
%
%        [r_phi0_out,z_phi0_out,r1_out,z1_out,rc_out,zc_out,thetphi_L_out,thetphi_tor_out,raysbylauncher_out,powers_out,fname_out,z_axis_out] = ...
%           toray_raygeom_TCV(shot,times_in,freqarr,wrfoarr,raysbylauncher_in,liuqe_vers,save_dir,isave, ...
%           ipowers_tree,isignIB,varargin);
%
%       examples: toray_raygeom_2(14525,1.0,[1 1 1],18,1,0);
%                 toray_raygeom_2(14525,1.0,[1 0 1], 18,1,1, [10 20 ; 0 0; 30 50])
%
%       Calculates a set of rays to be used by Toray.
%       First, the coordinates of the X2 beam waist are computed, taking account
%       of the launcher orientation. Then, the distance to the plasma surface,
%       for the specified shot and times_in, is derived as well as the curvature
%       of the wavefront. From this information, the point from which the rays
%       seem to be issued is deduced.
%       A correction is then applied to the toroidal angle to use as input
%       for Toray
%
%       shot:      self explanatory (must be larger than ~ 13000)
%       times_in:      [s] self explanatory
%
%       NEW: If only shot and times_in are provided, returns only the launcher characteristics: rc,zc,thetphi_L,thetphi_tor
%
%       freqarr:   frequency for each launcher (X2*6 and X3*3 by default)
%       wrfoarr:   O-mode fraction, 0 per launcher as default
%       raysbylauncher_in: number of rays for each launcher (1 by default), changes with effective power used if ipowers_tree=1
%       liuqe_vers:liuqe_version 1 or 2 or 3 or -1 (1 by default)
%       save_dir : directory where the file raygeom... should be saved (/tmp/$USER by default)
%       isave    : if 1 save raygeom data, otherwise only computes out args (0 by default)
%       ipowers_tree: 1 => read powers from tree (1 by default)
%       isignIB: sign of Ip*B0, if negative (-1), phi angle will be changed (+1 by default)
%       varargin{i} : gives key action i.
%       varargin{i+1} : arguments which meaning depends on key action varargin{i}
%
%       if varargin{i}='angles' then:
%            angles_sweep = varargin{i+1}{1}, determines if in sweep mode
%            iangles_tree= varargin{i+1}{2}:abs()= 1 or 2 => use angles from tree or model, 0 => angles from varargin{2}{i}{3}
%            angles = varargin{i+1}{3} if do not use those from tree. Added shift R option if iangles_tree<0
%
%       if varargin{i}='deltaz' then:
%            deltaz = varargin{i+1}; (default = 0.) (assumes in [m])
%
%       if varargin{i}='plasmabndzmag' then:
%            2 possibilities for defining plasma boundary and z_axis:
%            A) Values already selected at specific time => give double 1D array
%               r_contour = varargin{i+1}{1};
%               z_contour = varargin{i+1}{2};
%               z_axis    = varargin{i+1}{3};
%            B) Values vs time, will select time from input "times_in": => give structures:
%               r_contour.data=varargin{i+1}{1}.data and times from varargin{i+1}{1}.dim{2}
%               same for z_contour
%               time for z_axis = varargin{i+1}{1}.dim{1} (as 1D array)
%            if one of the three is empty, fetch all from tree
%
%       if varargin{i}='suffix' then:
%            varargin{i+1}: suffix to add to end of file name for consistency with GUIprofs new option
%
%       example of varargin for 2 key_action: 'angles' and 'file'
%        toray_raygeom(...,'angles',[{1} {0} {MDSprof.torinputs.thetphi_L}],'file',[{0} {2}])
%
%    Outputs:
%       r_phi0, z_phi0{:} : nominal path assuming phi=0
%       r1, z1        {:} : effective nominal path
%       rc, zc, thetphi_L,thetphi_tor of the different launchers
%

%       if isave==1: save in file
%
%       NOTE:   All angles in degrees
%
%               J.-Ph.Hogge   Oct 2nd 98
%
%               Revision 2.0  Oct 29 98

%       History:
%        1.0 Oct 2  98 JPH
%            Initial release
%        2.0 Oct 29.98 JPH
%            A bug in the rays orientation was corrected
%        2.1 Nov  3 98 JPH
%            Possibility to give angles manually
%        2.2 JULY 2001 GA
%            Possibility to use X3 launchers (top of TCV)
%            (see also trans_launch_to_toray)
%

%
% Defaults
%
% mxnb_launcher should be >= 3
%
mxnb_launcher=9;
powers=NaN * ones(1,mxnb_launcher);
if ~exist('raysbylauncher_in') || isempty(raysbylauncher_in)
  raysbylauncher_in = ones(1,mxnb_launcher);
end
ii= raysbylauncher_in>0;
powers(ii)=1;
default_minpower = 0.5; % power below which assumes no EC
angles_sweep=0;
iangles_tree=1;
deltaz=0.0;
iplasmabndzmag=0; % plasmabndzmag given in input
namesuffix='';
nverbose = 1;
%
% Other default outputs:
%
r_phi0=struct([]);
z_phi0=struct([]);
r1=struct([]);
z1=struct([]);
rc=[];
zc=[];
dim2=max(max(raysbylauncher_in).*angles_sweep,1);
thetphi_tor(1:mxnb_launcher,1:2,1:dim2) = NaN * ones(mxnb_launcher,2,dim2);
thetphi_L=thetphi_tor;
fname = [];
z_axis = [];

% at this stage just dummy outputs as if only one time value as input
r_phi0_out = r_phi0;
z_phi0_out = z_phi0;
r1_out = r1;
z1_out = z1;
rc_out = NaN(mxnb_launcher,1);
zc_out = NaN(mxnb_launcher,1);
thetphi_L_out = thetphi_L;
thetphi_tor_out = thetphi_tor;
powers_out = reshape(powers,length(powers),1);
fname_out = fname;
z_axis_out = z_axis;
raysbylauncher_out = reshape(raysbylauncher_in,length(raysbylauncher_in),1);
if length(raysbylauncher_in) <mxnb_launcher
  raysbylauncher_out(end+1:mxnb_launcher) = 0;
end
launchers_param_all = {};

%
% get inputs
%
% Main ones not yet checked
if ~exist('shot')
  disp('shot required as input')
  return;
end
if ~exist('times_in')
  disp('times_in(s) required as input')
  return;
end

if ~exist('freqarr') || isempty(freqarr)
  freqarr = [82.7e9 82.7e9 82.7e9 82.7e9 82.7e9 82.7e9 117.8e9 117.8e9 117.8e9];
end
if ~exist('wrfoarr') || isempty(wrfoarr)
  wrfoarr = [0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0];
end
if ~exist('liuqe_vers') || isempty(liuqe_vers)
  liuqe_vers = 1;
end
if ~exist('save_dir') || isempty(save_dir)
  save_dir = ['/tmp/' getenv('USER')];
end
if ~exist('isave') || isempty(isave)
 isave = 0;
end
if ~exist('ipowers_tree') || isempty(ipowers_tree)
 ipowers_tree = 1;
end
if ~exist('isignIB') || isempty(isignIB)
  isignIB = 1;
end

if ~isempty(varargin)
  for i=1:2:length(varargin)
    switch varargin{i}
      % case distribution standard or sweep
      case 'angles'
        % assumes arguments as follows: angles_sweep angle_from_tree angles
        angles_sweep=varargin{i+1}{1};
        if angles_sweep
          iangles_tree=0;
          angles=varargin{i+1}{3};
          angles_input=varargin{i+1}{3};
        else
          iangles_tree=varargin{i+1}{2};
          if iangles_tree <= 0
            angles=varargin{i+1}{3};
            angles_input=varargin{i+1}{3};
            % $$$ % Thus even if angles has sweep type dimensions, take only 1st ray as correct angle
            % $$$ angles=angles(:,:,1);
          end
        end
        
      case 'deltaz'
        % define deltaz
        deltaz = varargin{i+1};
        
      case 'plasmabndzmag'
        % define R,Z of plasma boundary
        rcont = varargin{i+1}{1};
        zcont = varargin{i+1}{2};
        % need zmag, as shifts plasma to z=0 for Toray
        zmag = varargin{i+1}{3};
        if ~isstruct(rcont)
          if ~isempty(rcont) && ~isempty(zcont) && ~isempty(zmag) ;iplasmabndzmag=1; end
        else
          if ~isempty(rcont.data) && ~isempty(zcont.data) && ~isempty(zmag.data) ;iplasmabndzmag=1; end
        end
        
      case 'launcher_val'
        % defines launcher design, not needed here
        
      case 'suffix'
        namesuffix=varargin{i+1};
        
      otherwise
        % not yet defined
        disp(['case: ' varargin{i} ' not yet defined in toray_raygeom_TCV'])
        return
    end
  end
end
if liuqe_vers>=2 || liuqe_vers==-1;
  vext=['_' num2str(liuqe_vers)];
else
  vext='';
end


% Get launcher positions (if required)
% changes only thephi angles of active rays

isignshot=sign(shot);
if shot<-100;
  disp('uses abs(shot) in toray_raygeom_TCV');
  shot=abs(shot);
end

% Toray_raygeom_TCV was written for getting launcher information for a single time input
% Now we include the option for times_in to be an array and we scan over time the whole subroutine essentially for historical reason
% To simplify the implementation, the full trace of whatever signal is assumed to be loaded for the first time index
%
do_load_tdi = 1; % should be put to 0 at last tdi call or end of loop, so the "continue" jumps over it if there is no power at the beginning
nb_times_in = length(times_in);
for it=1:nb_times_in
  time = times_in(it);
  raysbylauncher = raysbylauncher_in; % reset to input for each time
  %----------------------------------------------------------------------------
  if abs(iangles_tree) >= 1
    if shot==-1 || shot>=100000 || liuqe_vers==-1 || ipowers_tree == 0
      powers(1:9)=NaN;
    else
      ishot=mdsopen(shot);
      % power is now saved to node toray with write_pgyro
      lastwarn('');
      if do_load_tdi
        pow_trace=tdi('\results::toray.input:p_gyro');
        if isempty(pow_trace.data) && isempty(findstr(lastwarn,'tag not found')) && size(pow_trace.data,2)==0 ...
              && strcmp(getenv('USER'),'sauter')
          if nverbose>=3; disp(' node results::toray.input:p_gyro not yet filled in, do it now'); end
          [pgstatus,pgtime,pgyro,pgtoTCV] = write_pgyro(shot,1,[],[],[],[],[],1);
          ishot=mdsopen(shot);
          if nverbose>=3; disp(' DONE with toray.input:p_gyro'); end
          pow_trace=tdi('\results::toray.input:p_gyro');
        end
      end
      if ~isempty(pow_trace.data)
        it_val=iround_os(pow_trace.dim{1},time); % this way is equivalent to tdios
        powers(1:mxnb_launcher) = pow_trace.data(it_val,1:mxnb_launcher);
      else
        powers(1:mxnb_launcher)=NaN;
      end
      % mdsclose;
    end
  end

  if abs(iangles_tree)==1
    % Case we want the actual launcher parameters (angles not given) from measured values

    if liuqe_vers==-1
      disp('should not be here, show example to O. Sauter')
      iangles_tree
      return
    end
  
    mdsopen(shot);
    % has meaningful measured value only if used, thus use power here in any case
    % get which launchers were used (0 for no, 1 for yes)
    ij=zeros(size(powers));
    ijk= ~isnan(powers) & powers>default_minpower;
    ij(ijk)=1;
    if sum(ij)==0 && ipowers_tree == 1
% $$$       dim2=max(max(raysbylauncher).*angles_sweep,1);
% $$$       thetphi_tor_out(1:length(raysbylauncher),1:2,1:dim2) = NaN * ones(length(raysbylauncher),2,dim2);
% $$$       thetphi_L_out=thetphi_tor_out;
% $$$       r_phi0_out=[];
% $$$       z_phi0_out=[];
% $$$       r1_out=[];
% $$$       z1_out=[];
% $$$       rc_out=[];
% $$$       zc_out=[];
      raysbylauncher_out(1:length(raysbylauncher),it) = NaN.*ones(size(raysbylauncher));
      powers_out(:,it)=NaN.*ones(length(raysbylauncher),1);
      continue;
    end
  
    if shot >= 15500         % we directly read the angles from the ECRH tree
    
      if nverbose >= 3
        disp(' ');
        disp(' Launcher angles are directly read from the ECRH tree');
        disp(' ');
      end
      clear th phi
      tension_angles = -100.;
      for i=1:3
        if do_load_tdi
          x2_theta_l{i} = tdi(['\ecrh::launchers:theta_l:x2_' num2str(i)]);
          if ~isempty(x2_theta_l{i}.data) && ~ischar(x2_theta_l{i}.data)
            x2_theta_i{i} = interpos(x2_theta_l{i}.dim{1},x2_theta_l{i}.data,times_in,tension_angles);
          else
            x2_theta_i{i} = [];
          end
        end
        if isempty(x2_theta_i{i})
          th(i) = NaN;
        else
          th(i) = x2_theta_i{i}(it);
        end
        if do_load_tdi
          x2_phi_l{i} = tdi(['\ecrh::launchers:phi_l:x2_' num2str(i)]);
          if ~isempty(x2_phi_l{i}.data) && ~ischar(x2_phi_l{i}.data)
            x2_phi_i{i} = interpos(x2_phi_l{i}.dim{1},x2_phi_l{i}.data,times_in,tension_angles);
          else
            x2_phi_i{i} = [];
          end
        end
        if isempty(x2_phi_i{i})
          phi(i) = NaN;
        else
          phi(i) = x2_phi_i{i}(it);
        end
      end
    
      %%%%%%%%%%%%%
      
      if shot>=17246
            
        for i=4:6
          if do_load_tdi
            x2_theta_l{i} = tdi(['\ecrh::launchers:theta_l:x2_' num2str(i)]);
            if ~isempty(x2_theta_l{i}.data) && ~ischar(x2_theta_l{i}.data)
              x2_theta_i{i} = interpos(x2_theta_l{i}.dim{1},x2_theta_l{i}.data,times_in,tension_angles);
            else
              x2_theta_i{i} = [];
            end
          end
          if isempty(x2_theta_i{i})
            th(i) = NaN;
          else
            th(i) = x2_theta_i{i}(it);
          end
          if do_load_tdi
            x2_phi_l{i} = tdi(['\ecrh::launchers:phi_l:x2_' num2str(i)]);
            if ~isempty(x2_phi_l{i}.data) && ~ischar(x2_phi_l{i}.data)
              x2_phi_i{i} = interpos(x2_phi_l{i}.dim{1},x2_phi_l{i}.data,times_in,tension_angles);
            else
              x2_phi_i{i} = [];
            end
          end
          if isempty(x2_phi_i{i})
            phi(i) = NaN;
          else
            phi(i) = x2_phi_i{i}(it);
          end
        end
        % Read X3 mirror parameters from ECRH tree (SC, 08/03/12)
        if shot>=23079
          if do_load_tdi
            x3_theta_l = tdi(['\ecrh::launchers:theta_mir:x3']);
            if ~isempty(x3_theta_l.data) && ~ischar(x3_theta_l.data)
              x3_theta_i = interpos(x3_theta_l.dim{1},x3_theta_l.data,times_in,tension_angles);
            else
              x3_theta_i = [];
            end
          end
          if isempty(x3_theta_i)
            th(7:9) = NaN(1,3);
          else
            th(7:9) = x3_theta_i(it)*ones(1,3);
          end
          if do_load_tdi
            x3_phi_l = tdi(['\ecrh::launchers:r:x3']);
            if ~isempty(x3_phi_l.data) && ~ischar(x3_phi_l.data)
              x3_phi_i = interpos(x3_phi_l.dim{1},x3_phi_l.data,times_in,tension_angles);
            else
              x3_phi_i = [];
            end
          end
          if isempty(x3_phi_i)
            phi(7:9) = NaN(1,3);
          else
            phi(7:9) = x3_phi_i(it)*ones(1,3)*100.;
          end
        else
          th(7:9)=NaN;
          if do_load_tdi
            th79_trace = tdi('\ecrh::trcf_ecrh_launchers_c:channel_010');
            if ~isempty(th79_trace.data) && ~ischar(th79_trace.data)
              th79_values = interpos(th79_trace.dim{1},th79_trace.data,times_in,tension_angles);
            else
              th79_values = [];
            end
          end
          if isempty(th79_values)
            th(7:9) = NaN;
          else
            % th(7:9)  = th79_values(it);
            th(7:9)=45-0.7226 * th79_values(it);
          end
          %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
          %
          % phi become radial mirror position (in [cm]) for launcher 7-9
          %
          %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
          if do_load_tdi
            phi79_trace = tdi('\ecrh::trcf_ecrh_launchers_c:channel_005');
            if ~isempty(phi79_trace.data) && ~ischar(phi79_trace.data)
              phi79_values = interpos(phi79_trace.dim{1},phi79_trace.data,times_in,tension_angles);
            else
              phi79_values = [];
            end
          end
          if isempty(phi79_values)
            phi(7:9) = NaN;
          else
            % phi(7:9)  = phi79_values(it);
            phi(7:9)=(1056.8-(8.61*phi79_values(it)+167))/10;
          end
        end
      else
        if mxnb_launcher>3
          phi(4:mxnb_launcher)=NaN;
          th(4:mxnb_launcher)=NaN;
        end
      end
    
    else  % we cannot read values from the ECRH tree
      % case shot < 15500
      if nverbose >= 3
        disp(' ');
        disp(' Launcher angles estimated by computation');
        disp(' ');
      end
    
      if do_load_tdi
        phi1_trace = tdi('\ecrh::trcf_ecrh_launchers:channel_004');
        if ~isempty(phi1_trace.data) && ~ischar(phi1_trace.data)
          phi1_values = interpos(phi1_trace.dim{1},phi1_trace.data,times_in,tension_angles);
        else
          phi1_values = NaN(1,nb_times_in);
        end
        phi2_trace = tdi('\ecrh::trcf_ecrh_launchers:channel_005');
        if ~isempty(phi2_trace.data) && ~ischar(phi2_trace.data)
          phi2_values = interpos(phi2_trace.dim{1},phi2_trace.data,times_in,tension_angles);
        else
          phi2_values = NaN(1,nb_times_in);
        end
        phi3_trace = tdi('\ecrh::trcf_ecrh_launchers:channel_006');
        if ~isempty(phi3_trace.data) && ~ischar(phi3_trace.data)
          phi3_values = interpos(phi3_trace.dim{1},phi3_trace.data,times_in,tension_angles);
        else
          phi3_values = NaN(1,nb_times_in);
        end
      end
      phi1   = phi1_values(it);
      phi2   = phi2_values(it);
      phi3   = phi3_values(it);
      if do_load_tdi
        theta1_trace = tdi('\ecrh::trcf_ecrh_launchers:channel_001');
        if ~isempty(theta1_trace.data) && ~ischar(theta1_trace.data)
          theta1_values = interpos(theta1_trace.dim{1},theta1_trace.data,times_in,tension_angles);
        else
          theta1_values = NaN(1,nb_times_in);
        end
        theta2_trace = tdi('\ecrh::trcf_ecrh_launchers:channel_002');
        if ~isempty(theta2_trace.data) && ~ischar(theta2_trace.data)
          theta2_values = interpos(theta2_trace.dim{1},theta2_trace.data,times_in,tension_angles);
        else
          theta2_values = NaN(1,nb_times_in);
        end
        theta3_trace = tdi('\ecrh::trcf_ecrh_launchers:channel_003');
        if ~isempty(theta3_trace.data) && ~ischar(theta3_trace.data)
          theta3_values = interpos(theta3_trace.dim{1},theta3_trace.data,times_in,tension_angles);
        else
          theta3_values = NaN(1,nb_times_in);
        end
      end
      th1   = theta1_values(it);
      th2   = theta2_values(it);
      th3   = theta3_values(it);
    
      phi(1) = (phi1 + 0.009766) * 350/20/0.5;
      th(1)  = 52 - 4.8087 * (th1 + 4.29430);
      phi(2) = (phi2 + 0.004800) * 350/20/0.5;
      th(2)  = 52 - 4.9581 * (th2 + 4.26655);
      phi(3) = (phi3 + 0.000000) * 350/20/0.5;
      th(3)  = 52 - 4.8026 * (th3 + 4.18149);
    
      if shot >= 15260 % need to correct launcher 2 toroidal angle
        % by 1.2  degree
        if nverbose >= 1; disp('Adapting launcher 2 toroidal angle by 1.2 degree'); end
        th(2) = th(2) + 1.2;
      end
      if mxnb_launcher>3
        phi(4:mxnb_launcher)=NaN;
        th(4:mxnb_launcher)=NaN;
      end
    end
  
    angles=[th' phi'];
  
    % mdsclose;
    
  elseif abs(iangles_tree)==2
    % from model angles
    %    if shot<20000 || shot>=100000
    %      % Note: to be changed if model for 100000 can exist (or -1 shot)
    %      disp(' ')
    %      disp(' model for angles not saved for shots before 20000 or -1 or >=100000')
    %      disp(' ')
    %      return
    %    end
    if nverbose >= 3; disp(' angles from model'); end
    mdsopen(shot)
    clear th phi
    for igyro=1:6
      if do_load_tdi
        toTCV{igyro}= tdi(['\pcs::switch_line_' num2str(igyro,'%.2d') ':state']);
      end
      if strcmpi(toTCV{igyro}.data,'TCV')
        if do_load_tdi
          theta{igyro} = tdi(['\pcs::design_mirror_' num2str(igyro,'%.2d')]);
          if ~isempty(theta{igyro}.data) && ~ischar(theta{igyro}.data)
            th_values{igyro}=interp1(theta{igyro}.dim{1},theta{igyro}.data,times_in);
          else
            th_values{igyro} = NaN(1,nb_times_in);
          end
          ph{igyro} = tdi(['\pcs::design_launcher_' num2str(igyro,'%.2d')]);
        end
        th(igyro) = th_values{igyro}(it);
        phi(igyro) = ph{igyro}.data;
        powers(igyro)=1.;
      else
        th(igyro)=NaN;
        phi(igyro)=NaN;
        powers(igyro)=0.;
      end
    end
    if do_load_tdi
      theta7_trace=tdi('\pcs::design_mirror_07');
      if ~isempty(theta7_trace.data) && ~ischar(theta7_trace.data)
        th7_values = interp1(theta7_trace.dim{1},theta7_trace.data,times_in);
      else
        th7_values = NaN(1,nb_times_in);
      end
    end
    th(7:9) = 90. - 0.5*th7_values(it);
    if do_load_tdi
      R7_trace = tdi('\pcs::mir_R_07');
      if ~isempty(R7_trace.data) && ~ischar(R7_trace.data)
        if length(R7_trace.data)>1
          disp('unexpected, OS thought R7-9 are fixed during shot, check with O. Sauter')
          return
        else
          R7_values = R7_trace.data*ones(1,nb_times_in);
        end
      else
        R7_values = NaN(1,nb_times_in);
      end
    end
    phi(7:9)=R7_values(it) * 100;
  
    angles=[th' phi'];
  
  else
    % angles from varargin{2}{i}{3}, with i such that varargin{1}{i}='angles'
    thetphi_L=angles;
    clear th phi
    th=angles(:,1,:);
    phi=angles(:,2,:);
  
  end

  if iangles_tree < 0
    % add shift to tree/model values
    % So far just a scalar assumed as shift R for X3 launchers
    if isscalar(angles_input)
      if nverbose >= 1; disp(['add shift of ' angles_input ' to R position of X3 launcher']); end
      phi(7:9) = phi(7:9) + angles_input;
      angles=[th' phi'];
    else
      whos angles
      disp('error, option not yet defined, ask O. Sauter')
      return
    end
  end

  if ipowers_tree
    if nverbose >= 3 && do_load_tdi
      disp(' Powers taken from tree')
      fprintf('\n             ');
      for i=1:min(length(powers),length(raysbylauncher))
        fprintf('%8s',['L',int2str(i)]);
      end
      fprintf('\n             ');
    end
    for i=1:min(length(powers),length(raysbylauncher))
      if isnan(powers(i)) || powers(i)<=default_minpower % 0.5kW minimum?
        raysbylauncher(i)=0;
        if nverbose >= 3 && do_load_tdi; fprintf('%8s','-'); end
      else
        if nverbose >= 3 && do_load_tdi; fprintf('%8s','TCV'); end
      end
    end
  end
  if nverbose >= 3 && do_load_tdi; fprintf('\n'); end

  % defined only up to mxnb_launcher launcher => set others inactive
  if (length(raysbylauncher)>mxnb_launcher)
    raysbylauncher(mxnb_launcher+1:end) = 0;
  end

  if nverbose >= 3 && do_load_tdi
    fprintf('powers (kW) :');fprintf('%8.1f',powers); fprintf('\n')
    fprintf('rays        :'); fprintf('%8.0f',raysbylauncher); fprintf('\n')
    fprintf('theta_L     :'); fprintf('%8.1f',angles(:,1)); fprintf('\n')
    fprintf('phi_L       :'); fprintf('%8.1f',angles(:,2)); fprintf('\n')
  end

  dim2=max(max(raysbylauncher).*angles_sweep,1);
  thetphi_tor(1:length(raysbylauncher),1:2,1:dim2) = NaN * ones(length(raysbylauncher),2,dim2);
  thetphi_L=thetphi_tor;
  clear th phi
  th=angles(:,1,:);
  phi=angles(:,2,:);

  % -------------------------------------------------------------------
  % Open tree to get information on plasma boundary location...
  % -------------------------------------------------------------------
  %
  % construct plasma boundary and zmag
  if ~iplasmabndzmag
    if  shot<=0
      disp('shot number does not allow to fetch plasma boundary from nodes')
      disp('load an eqdsk before')
      return
    end
    if liuqe_vers==-1
      disp('should not be here, show example to O. Sauter')
      return
    end
    % fetch from tree
    mdsopen(shot);
    
    %get number of points describing plasma
    if do_load_tdi
      nptsall_trace = tdi(['\results::npts_contour',vext]);
      times = nptsall_trace.dim{1};
      for i=1:length(times_in),
        [~, j]=min(abs(times-times_in(i)));
        ind_times(i) = j;
      end
      nptsall_values = nptsall_trace.data;
      r_ctr1_trace  = tdi(['\results::r_contour',vext]);
      z_ctr1_trace  = tdi(['\results::z_contour',vext]);
      z_axis_trace  = tdi(['\results::z_axis',vext]);
    end
    nptsall(it) = nptsall_values(ind_times(it));
    % r and z locations, multiply by 100 to get [cm]
    r_ctr1  =  r_ctr1_trace.data(1:nptsall(it),ind_times(it))*100;
    % z will be shifted by -zaxis to have zaxis=0, but this concerns values saved to files for toray
    % here launchers at given position and equilibrium shifted by deltaz
    z_ctr1  =  (z_ctr1_trace.data(1:nptsall(it),ind_times(it)) + deltaz)*100;
    z_axis = (z_axis_trace.data(ind_times(it)) + deltaz)*100.;
    
  else
    % from input parameters
    if ~isstruct(rcont)
      r_ctr1 = rcont*100;
      z_ctr1 = (zcont + deltaz)*100;
      z_axis = (zmag+ deltaz)*100;
    else
      times1=rcont.dim{2}; % assume same time base for rcont and zcont, but not for zmag
      times2=zmag.dim{1}; % assume same time base for rcont and zcont, but not for zmag
      [~, j]=min(abs(times1-time));
      ii=find(rcont.data(:,j)>0);
      r_ctr1 = rcont.data(ii,j)*100;
      z_ctr1 = (zcont.data(ii,j) + deltaz)*100;
      [~, j]=min(abs(times2-time));
      z_axis = (zmag.data(j)+ deltaz)*100;
    end
    nptsall(it)=size(r_ctr1,1);
  end

  %p_box is a rectangle with plasma limits
  % take 5cm security since equilibrium limits might change with smoothing or slight z~=0 shift
  p_box   = [min(r_ctr1)-5,max(r_ctr1)+5,min(z_ctr1)-10,max(z_ctr1)+10];
  
  %
  % Merge angles and launcher position with rays for Toray with plama boundary
  %
  
  % pas logique toutes les grandeurs devraient avoir les memes dimension
  % dimlaunchers en colonne et dim nray en ligne
  r_phi0 = num2cell(NaN * ones(dim2,length(raysbylauncher)));
  z_phi0 = num2cell(NaN * ones(dim2,length(raysbylauncher)));
  r1     = num2cell(NaN * ones(dim2,length(raysbylauncher)));
  z1     = num2cell(NaN * ones(dim2,length(raysbylauncher)));
  rc     = NaN * ones(length(raysbylauncher),dim2);
  zc     = NaN * ones(length(raysbylauncher),dim2);

  % adapt raysbylauncher such as launcher turned-off if angles are NaNs

  % thetphi_L=angles;
  keep_rays_if_outside=1;
  if isave~=1   % just need to compute r_phi0{i},z_phi0{i},r1{i},z1{i}, then return
    for i=1:length(raysbylauncher)
      if raysbylauncher(i)>=1
        freq=freqarr(i);
        wrfo=wrfoarr(i);
        Th=th(i,:);
        Phi=phi(i,:);
        iii=find(~isnan(Th));
        for i3=1:length(iii)
          ii=iii(i3);
          [x,y,zec,thetec,phaiec,phi_p,r_phi0{ii,i},z_phi0{ii,i},r1{ii,i},z1{ii,i},w_p,w02,beamcoord,launchers_param] =...
              launch_point(i,freq,Th(ii),Phi(ii),r_ctr1,z_ctr1,z_axis,nptsall(it),0,shot);
          launchers_param.theta_launcher = Th(ii);
          launchers_param.phi_launcher = Phi(ii);
          launchers_param.freq = freq;
          launchers_param.theta_toray = thetec;
          launchers_param.phi_toray = phaiec;
          launchers_param.zec = zec;
          launchers_param.r1 = r1{ii,i};
          launchers_param.z1 = z1{ii,i};
          launchers_param.z_axis = z_axis;
          launchers_param.time = time;
          launchers_param_all{i}{it} = launchers_param;
          if isempty(x) | isempty(y)
            if nverbose >= 3;disp('un rayon hors plasma'); end
            if keep_rays_if_outside
              thetphi_tor(i,1:2,ii)=[thetec phaiec];
              thetphi_L(i,1:2,ii)=[Th(ii) Phi(ii)];
            else
              if length(Th)==1
                raysbylauncher(i)=0;
              else
                raysbylauncher(i)=raysbylauncher(i) - 1;
              end
              r_phi0{ii,i}=NaN;
              z_phi0{ii,i}=NaN;
              r1{ii,i}=NaN;
              z1{ii,i}=NaN;
              thetphi_tor(i,1:2,ii)=[NaN NaN];
              thetphi_L(i,1:2,ii)=[NaN NaN];
            end
          else
            rc(i,ii)=0.01*sqrt((x(1))^2 + (y(1))^2);
            zc(i,ii)=0.01*(zec(1)+z_axis);
            thetphi_tor(i,1:2,ii)=[thetec phaiec];
            thetphi_L(i,1:2,ii)=[Th(ii) Phi(ii)];
          end
        end
      else
        if nverbose >= 3; disp(sprintf('Launcher #%i not selected',i)); end
      end
    end
  end
  
  % open file in which ray geometry will be stored
  
  if isave
    s = sprintf('%.4f',time); %  fname = sprintf('$HOME/onetwo/bin/INPUTS/raygeom_%st%s',num2str(shot),s);
    fname = sprintf('%s/raygeom_%st%s%s',save_dir,num2str(isignshot*abs(shot)),s,namesuffix);
    
    if nverbose >= 3
      disp(' ');
      disp(sprintf('Ray geometry will be stored in file: %s ',fname));
    end
    fid = fopen(fname,'wt');
    
    if angles_sweep==0
      indnray=raysbylauncher;
    elseif angles_sweep==1
      indnray=ones(1,length(raysbylauncher));
    else
      error([' Warning, angles_sweep should be 0 or 1, not: ' num2str(angles_sweep)]);
      % return
    end
    
    nraystot=zeros(1,length(raysbylauncher));
    raystot=[];
    w_p=[];
    w02=[];
    for i=1:length(raysbylauncher)
      if raysbylauncher(i)>=1
        freq=freqarr(i);
        wrfo=wrfoarr(i);
        Th=th(i,:);
        Phi=phi(i,:);
        iii=find(~isnan(Th));
        for i3=1:length(iii)
          ii=iii(i3);
          [x,y,zec,thetec,phaiec,phi_p,r_phi0{ii,i},z_phi0{ii,i},r1{ii,i},z1{ii,i},w_p,w02,beamcoord,launchers_param] =...
              launch_point(i,freq,Th(ii),Phi(ii),r_ctr1,z_ctr1,z_axis,nptsall(it),0,shot);
          launchers_param.theta_launcher = Th(ii);
          launchers_param.phi_launcher = Phi(ii);
          launchers_param.freq = freq;
          launchers_param.theta_toray = thetec;
          launchers_param.phi_toray = phaiec;
          launchers_param.zec = zec;
          launchers_param.r1 = r1{ii,i};
          launchers_param.z1 = z1{ii,i};
          launchers_param.z_axis = z_axis;
          launchers_param.time = time;
          launchers_param_all{i}{it} = launchers_param;
          
          if ~isempty(x) & ~isempty(y)
            rc(i,ii)=0.01*sqrt((x(1))^2 + (y(1))^2);
            zc(i,ii)=0.01*(zec(1)+z_axis);
            thetphi_L(i,1:2,ii)=[Th(ii) Phi(ii)];
            thetphi_tor(i,1:2,ii)=[thetec phaiec];
            
            % z will be shifted by z_axis to have z_axis=0 for equilibrium
            % thus give zec (=z_launch-zaxis) to construct rays
            % also shift p_box
            p_boxeff=p_box - [0 0 z_axis z_axis]; % affecting launchers 7-9 %p_boxeff(3:4) = p_box(3:4) - z_axis;
            [rays,raysbylauncher(i)] = gaussian_rays(i,indnray(i),x,y,zec,thetec,phaiec,phi_p,p_boxeff,w_p,w02,beamcoord);
            % same freq and O-mode fraction for each ray per launcher
            raystot=[raystot; [rays' freq/1e9*ones(size(rays,2),1) wrfo*ones(size(rays,2),1)]];
            nraystot(i)=nraystot(i)+size(rays,2);
          else
            thetphi_L(i,1:2,ii)=[NaN NaN];
            thetphi_tor(i,1:2,ii)=[NaN NaN];
          end
        end
      else
        %disp(sprintf('Launcher #%i not selected',i));
      end
      % to pass nray effective to raygeom prepare data of GUIprofs
    end
    
    ii=find(raysbylauncher>0);
    nray_tot=sum(nraystot);
    raysbylauncher(ii)=nraystot(ii);
    fprintf(fid,'%i\n',nray_tot);
    if isignIB>0
      fprintf(fid,'%8.3f %8.3f %8.3f %8.3f %8.3f  %8.3f %8.3f\n',raystot');
    else
      beep
      disp(' ')
      disp('WARNING: PHI ANGLES CHANGED SIGN IN ORDER TO HAVE CORRECT TORAY VALUES, SINCE SIGN(IP*B0)<0')
      disp(' ')
      beep
      raystot(:,end+1)=raystot(:,5);
      raystot(:,5)=-raystot(:,5);
      fprintf(fid,'%8.3f %8.3f %8.3f %8.3f %8.3f  %8.3f %8.3f   %8.3f\n',raystot');
      fprintf(fid,' Sign of phi was changed because sign(Ip)*sign(B0)<0, last column is original phi values\n');
    end
    fclose(fid);
  end %fin isave==1

  % copy main outputs to outputs_vs_time
  if nb_times_in > 1
    r_phi0_out{it} = r_phi0;
    z_phi0_out{it} = z_phi0;
    r1_out{it} = r1;
    z1_out{it} = z1;
    rc_out(1:length(rc),it) = rc;
    zc_out(1:length(zc),it) = zc;
    thetphi_L_out(:,:,:,it) = thetphi_L(:,:,:);
    thetphi_tor_out(:,:,:,it) = thetphi_tor(:,:,:);
    raysbylauncher_out(1:length(raysbylauncher),it) = reshape(raysbylauncher,length(raysbylauncher),1);
    powers_out(:,it) = reshape(powers,length(powers),1);
    fname_out{it} = fname;
    z_axis_out(it) = z_axis;
  else
    r_phi0_out = r_phi0;
    z_phi0_out = z_phi0;
    r1_out = r1;
    z1_out = z1;
    rc_out = rc;
    zc_out = zc;
    thetphi_L_out = thetphi_L(:,:,:);
    thetphi_tor_out = thetphi_tor(:,:,:);
    raysbylauncher_out = reshape(raysbylauncher,length(raysbylauncher),1);
    powers_out = powers;
    fname_out = fname;
    z_axis_out = z_axis;
  end
  do_load_tdi = 0;
end

varargout{1} = launchers_param_all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% end function toray_raygeom %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [x,y,z]=line_plane_intersect(A,B,C,D,x1,y1,z1,l,m,n);
%
% determines the intersection of *one* plane given by :
%
%          Ax + By + Cz + D = 0
%
% and a series of 3D line of equation :
%
%
%          (x-x1)/l = (y-y1)/m = (z-z1)/n
%
% where (x1,y1,z1) is a point of the line (and if (x2,y2,z2) is another
% one, then )
%
%          l = (x2-x1)
%          m = (y2-y1)
%          n = (z2-z1)
%
%


% Initialize values
x = zeros(size(x1));
y = x;
z = x;


z_phi0    = A * l + B * m + C * n;
[i]      = find(z_phi0 == 0);
z_phi0(i) = eps;

rho      = -(A*x1 + B*y1 + C*z1 + D)./z_phi0;

x        = x1 + l .* rho;
y        = y1 + m .* rho;
z        = z1 + n .* rho;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% end function  line_plane_intersect %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [x,y]=line_intersect_2d(a1,b1,a2,b2);
%intersection de deux droite d'equation y=ax+b
[i] = find(a2-a1 == 0);
a1(i) = a1(i) +eps;
x = - (b2-b1)./(a2-a1);
y = a1 .* x + b1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% end function  line_intersect_2d %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function R = R(w0,f,z);
%freq in [GHz] & distance in [mm]
lambda = 300/f;
Zr = pi * w0.^2/lambda;
R = z .* (1 + Zr.^2./z.^2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% end function R %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [wz,Zr] = wz(w0,f,z);
%freq in [GHz] & distance in [mm]
lambda = 300/f;
Zr = pi * w0.^2/lambda;
wz = w0 .* sqrt(1+z.^2./Zr.^2);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% end function wz %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [rays,nray] = gaussian_rays(launcher,nray,x,y,z,thetec,phaiec,phi_p,p_box,w_p,w02,beamcoord);

if launcher<=6
  [rays,nray] = gaussian_rays_x2(launcher,nray,x,y,z,thetec,phaiec,phi_p,p_box,w_p);
else
  [rays,nray] = gaussian_rays_x3(launcher,nray,x,y,z,thetec,phaiec,phi_p,p_box,w_p,w02,beamcoord);
  
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% end function gaussian_ray %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [rays,nray] = gaussian_rays_x3(launcher,nray,x,y,z,thetec,phaiec,phi_p,p_box,w_p,w02,beamcoord);

% SC, 26/02/12: we use w_p (waist at plasma intersection) instead of w02 (min.
%   waist) and we no longer accept the cylindrical beam approximation
% We use the waist rather than w_p/sqrt(2), both for consistency with X2 and
%   because r_angle_routine_x3 always assumed that the input was the
%   electric-field waist (so it was incorrect before)
% Support for astigmatic (elliptical) waist
% SC, 07/03/12: since w_p is just a proportionality factor, remove from
%   argument and assume unit width

if nray==1
  R_rays=[0;0];
  A_rays=0;
elseif nray>1 & nray<7
  R_rays=[zeros(length(w_p),1) w_p*ones(1,nray-1)];
  ang=linspace(0,2*pi,nray);
  A_rays=[0 ang(1:end-1)];
elseif nray>=7
  [R_rays,A_rays,nray]=r_angle_routine_x3(nray);
  R_rays=w_p*R_rays;
end

R_c=w_p/sqrt(2)./tan(phi_p/180*pi);

% Thinking in terms of beam coordinates (beamcoord: eta,zeta,xi), setting  the
%   origin in the center of the beam at the second ray origin, we have two ray
%   origins in general for an astigmatic beam, at xi=R_c(2)-R_c(1) and xi=0. For
%   each ray passing through [eta,zeta,R_c(2)] we choose the reference point at
%   xi=0. The reference point coordinates are therefore
%   [eta*(1-R_c(2)/R_c(1)),0,0].
bcray = [R_rays(1,:).*cos(A_rays)*(1-R_c(2)/R_c(1)); ...
  zeros(1,nray); ...
  zeros(1,nray)];

x_ray = beamcoord(1,:)*bcray+x(2);
y_ray = beamcoord(2,:)*bcray+y(2);
z_ray = beamcoord(3,:)*bcray+z(2);

bcraydir = [R_rays(1,:).*cos(A_rays)*R_c(2)/R_c(1); ...
  R_rays(2,:).*sin(A_rays); ...
  abs(R_c(2))*ones(1,nray)];
dx=beamcoord(1,:)*bcraydir;
dy=beamcoord(2,:)*bcraydir;
dz=beamcoord(3,:)*bcraydir;
dr = sqrt(dx.^2+dy.^2+dz.^2);

thet_ray=acos(dz./dr)/pi*180;
phi_ray=atan2(dy,dx)/pi*180;

A=0;
B=0;
C=1;
D=-p_box(4);

l=dx./dr;
m=dy./dr;
n=dz./dr;


[x_ray,y_ray,z_ray]=line_plane_intersect(A,B,C,D,x_ray,y_ray,z_ray,l,m,n);


rays = [x_ray;y_ray;z_ray; thet_ray;phi_ray];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% end function gaussian_ray_x3 %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [rays,nray] = gaussian_rays_x2(launcher,nray,x,y,z,thetec,phaiec,phi_p,p_box,w_p);

if nray==1
  R_rays=0;
  A_rays=0;
elseif nray>1 & nray<7
  R_rays=[0 w_p*ones(1,nray-1)];
  ang=linspace(0,2*pi,nray);
  A_rays=[0 ang(1:end-1)];
elseif nray>=7
  [R_rays,A_rays]=r_angle_routine_x2(nray);
  R_rays=w_p*R_rays;
end

R_c=w_p/sqrt(2)./tan(phi_p/180*pi);

% Valid for nray = 1,6,18,30 so far
% The rays are distributed in such a way that they map a gaussian beam if they
% carry the same amount of power.
%
% E ~ E0 exp{- r^2/w^2}
% P ~ E^2 ~ E0^2 exp{-2r^2/w^2}
%
% R = [R1 R2 R3 R4]  (normalized to w, the spot-size)
%
% disp(sprintf('Number of rays: %i',nray))


% $$$ disp(sprintf('FOR TORAY (version *WITH* ray flexibility):'));
% $$$ disp(sprintf('-------------------------------------------'));

x_ray = ones(1,nray) * x;       % Line vector
y_ray = ones(1,nray) * y;       % Line vector
z_ray = ones(1,nray) * z;       % Line vector

% R_rays to w_power because phi_p angles for power
% SC, 27/02/12: this seems buggy - it essentially replaces w_p by w_p^2 which is
%   not even dimensionally correct
%w_power=1.0
%thet_ray = thetec  + phi_p * R_rays/w_power .* cos(A_rays) ;
%phi_ray  = phaiec  + phi_p * R_rays/w_power .* sin(A_rays) ;

% this seems better although the angle around the cone bundle does not map
%   directly to theta and phi
%thet_ray = thetec  + R_rays/R_c .* cos(A_rays) ;
%phi_ray  = phaiec  + R_rays/R_c .* sin(A_rays) ;
%gaussian_rays = 1;

% This is the correct way
% Orientation of beam
dx = sin(thetec/180*pi)*cos(phaiec/180*pi);
dy = sin(thetec/180*pi)*sin(phaiec/180*pi);
dz = cos(thetec/180*pi);
% We now construct a cartesian system with the beam and the unit vectors
%   [sin(phaiec);-cos(phaiec);0] and
%   [cos(thetec)*cos(phaiec);cos(thetec)*sin(phaiec);-sin(thetec)]
%   and convert the rays back to the lab system
dx = abs(R_c)*dx+R_rays.*(cos(A_rays)*sin(phaiec/180*pi)+ ...
  sin(A_rays)*cos(thetec/180*pi)*cos(phaiec/180*pi));
dy = abs(R_c)*dy+R_rays.*(-cos(A_rays)*cos(phaiec/180*pi)+ ...
  sin(A_rays)*cos(thetec/180*pi)*sin(phaiec/180*pi));
dz = abs(R_c)*dz-R_rays.*sin(A_rays)*sin(thetec/180*pi);
dr = sqrt(dx.^2+dy.^2+dz.^2);

thet_ray=acos(dz./dr)/pi*180;
phi_ray=atan2(dy,dx)/pi*180;

%
% Cut the rays so that they are close to the plasma
%

% 'Orientation of rays'

% Version 1 ---> wrong
% l =  cos(phi_ray *pi/180) .* sin(thet_ray*pi/180);
% m = -sin(phi_ray *pi/180) .* sin(thet_ray*pi/180);
% n =  cos(thet_ray*pi/180);
% (OS 04.2008) note that above should have allowed to see that definiton of thet_ray,phi_ray was not correct, but above
% lines have sign of l and n wrong, so was anyway wrong as well

%
% Version 2 (Oct. 29 98): The launcher angles thet_l and phi_l are used again
%                         to estimate the rays orientation
%

% Version 3 (April 2008)(OS): keep thet_l and phi_l used for intersection but use standard function to obtain back transformation from trans_toray_to_launch.
% Note that now we do have:
% l = -cos(phi_ray *pi/180) .* sin(thet_ray*pi/180) = cos(thet_l*pi/180);
% m = -sin(phi_ray *pi/180) .* sin(thet_ray*pi/180) = sin(thet_l*pi/180) .* sin(phi_l*pi/180);
% n = -cos(thet_ray*pi/180) = sin(thet_l*pi/180) .* cos(phi_l*pi/180);
% these correspond to -kx, -ky and -kz (but sign not important in line_plane_intersect)
%

A = 1;
B = 0;
C = 0;
D = - p_box(2);

[thet_l,phi_l] = trans_toray_to_launch(thet_ray,phi_ray,'TCV');

l = cos(thet_l*pi/180);
m = sin(thet_l*pi/180) .* sin(phi_l*pi/180);
n = sin(thet_l*pi/180) .* cos(phi_l*pi/180);

% We want to find the intersection with the largest r of plasma
% (not yet definitive)) (but given as starting rays to Toray)
% compute x_ray,y_ray,z_ray at x=Rmax=-D

[x_ray,y_ray,z_ray]=line_plane_intersect(A,B,C,D,x_ray,y_ray,z_ray,l,m,n);

rays = [x_ray;y_ray;z_ray; thet_ray;phi_ray];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% end function gaussian_rays_x2 %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%----------------------------------------------------------------------
function [r,angle,ntot]=r_angle_routine_x3(ntot)
%Update (december 2002) G.A.
%This routine has been modified in order to improve
%the gaussian beam model. it implies that the total number
%of rays must satisfy ntot=6*N+1 where N is an integer.
%
% Massively streamlined 2/12 (SC)
% Added support for elliptical cross section
% Since w_p is just a proportionality factor, remove from arguments and assume
%   unit width

nbre=6;
nverbose = 1;

if mod(ntot,nbre)~=1
  if nverbose >= 3
    disp(' ')
    disp('******************************************')
    disp('!  The total number of rays for the X3   !')
    disp('!launcher must be 6*N+1 (N is an integer)!')
    disp('******************************************')
    disp(' ')
  end
  %Take the nearest upper value = 6*N+1
  ntot=ntot-mod(ntot,nbre)+7;
  if nverbose >= 3
    disp(' ')
    disp(['raysbylauncher will be ' sprintf('%d',ntot)])
    disp(' ')
  end
end
if nverbose >= 3; disp(sprintf('number of rays per r = %d',nbre)); end

layers=floor(ntot/nbre)+1;
n=[1,nbre*ones(1,layers-1)];
N(1)=n(1);
N(2:length(n))=cumsum(n(1:end-1))+n(2:end)/2;
angoff=(mod([1:layers-1],2)-1)*pi/nbre;
r(1)=0;
angle(1)=0;
for i=2:length(n),
  r=[r,repmat((-log(1-N(i)/ntot)).^0.5,1,n(i))];
  angle=[angle,2*pi*[0:n(i)-1]/n(i)+angoff(i-1)];
end
r=r/sqrt(2);

if nverbose >= 3
  disp(' ')
  disp('*-------------------*')
  disp(sprintf('| last r = %1.2f mm |',r(end)))
  disp('*-------------------*')
  disp(' ')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% end function r_angle_routine_x3 %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -----------------------------------------------------------------------------
function [r,angle]=r_angle_routine_x2(ntot)

%fonction de base n>=7

% Massively streamlined 2/12 (SC)
% Since w_p is just a proportionality factor, remove from arguments and assume
%   unit width

nbre=5;
layers=floor(ntot/nbre)+1;
resid=ntot-1-nbre*(layers-1);
reseven=floor(resid/(layers-1));
resres=mod(resid,layers-1);
n=[1,(nbre+reseven)*ones(1,layers-1)];
n(2:resres+1)=n(2:resres+1)+1;
N(1)=n(1);
N(2:length(n))=cumsum(n(1:end-1))+n(2:end)/2;
angoff=rand(1,layers-1)*2*pi;
r(1)=0;
angle(1)=0;
for i=2:length(n),
  r=[r,repmat((-log(1-N(i)/ntot)).^0.5,1,n(i))];
  angle=[angle,2*pi*[0:n(i)-1]/n(i)+angoff(i-1)];
end
r=r/sqrt(2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% end function r_angle_routine %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%calculate launch point for X2 launcher

function [x,y,zec,thetec,phaiec,phi_p,r_phi0,z_phi0,r1,z1,w_p,w02,beamcoord,varargout] = ...
  launch_point(launcher,freq,theta_l,phi_l,r_ctr1,z_ctr1,z_axis,npts,idisplay,shot)
% Calls the specific launch_point function for given launchers (side or top launch)

if launcher<7
  % launchers 1-6
  w02=[];
  [x,y,zec,thetec,phaiec,phi_p,r_phi0,z_phi0,r1,z1,w_p,varargout_1] =...
    launch_point_16(launcher,freq,theta_l,phi_l,r_ctr1,z_ctr1,z_axis,npts,idisplay);
  beamcoord=[];
else
  % launchers 7-9
  if shot>-1 && shot<=34579,
    ct=1/197.8;
    cr=1/98.9;
  elseif shot>-1 && shot<45399,
    ct=1/56.6;
    cr=1/113.14;
  else
    ct=1e-5;
    cr=1/134.35;
    ct=5e-3;
    cr=1/134.35;
  end
  alpha=0;
  [x,y,zec,thetec,phaiec,phi_p,r_phi0,z_phi0,r1,z1,w_p,w02,beamcoord,varargout_1] =...
    launch_point_79(launcher,freq,theta_l,phi_l,r_ctr1,z_ctr1,z_axis,npts,idisplay, ...
    ct,cr,alpha);

end

varargout{1} = varargout_1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% end function launch_point %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [x,y,zec,thetec,phaiec,phi_p,r_phi0,z_phi0,r1,z1,w_p,varargout] = ...
  launch_point_16(launcher,freq,theta_l,phi_l,r_ctr1,z_ctr1,z_axis,npts,idisplay)

% disp(...) only if idisplay

% -------------------------------------------------------------------
% Launcher geometrical parameters [cm], launcher is in (x,y) plane
% -------------------------------------------------------------------
% From TOP: x <=> W-E
%           y <=> S-N
%           z <=> Vertical axis
%
%  corrected 11/05/07 (SC)
%  based on information from TPG based on original launcher drawings and on
%    tcvports.m for port coordinates (assumed to be for machine at room
%    temperature)
%
p3     = [130.723  0 39.885 ];       % Coordinates of beam center on Mir #3 [cm]
p4     = [121.8869  0 51.2 ];        % Coordinates of beam center on Mir #4 [cm]
pp     = [119.056 0 52.2 ];          % Coordinates Mir #4 rotation axis [cm]

r_p    = 1;                          % Dist between Mir#4 surface and axis of rotation [cm]

w0     = 18.8;                       % Beam waist dimension (measured) [mm]
d2     = 27.55;                      % Distance from Mir #3 to beam waist
l_axis = 45.5;                      % Launcher axis z coordinate *launcher # 2,3)

if (launcher==1 | launcher ==4)
  p3(3) = p3(3) - 45.75;
  p4(3) = p4(3) - 45.75;
  pp(3) = pp(3) - 45.75;
  l_axis = l_axis - 45.75;
end

%p3     = [130.791  0 40.135 ];       % Coordinates of beam center on Mir #3 [cm]
%p4     = [121.803  0 51.522 ];       % Coordinates of beam center on Mir #4 [cm]
%pp     = [119.0567 0 52.135 ];       % Coordinates Mir #4 rotation axis [cm]
%r_p    = 1;                          % Dist between Mir#4 surface and axis of rotation [cm]

%w0     = 18.8;                       % Beam waist dimension (measured) [mm]
%d2     = 27.55;                      % Distance from Mir #3 to beam waist
%l_axis = 45.75;                      % Launcher axis z coordinate *launcher # 2,3)

%if (launcher==1 | launcher ==4)
%  p3(3) = p3(3) - l_axis;
%  p4(3) = p4(3) - l_axis;
%  pp(3) = pp(3) - l_axis;
%  l_axis = 0;
%end

% defaults
thetec=-1;
phaiec=-1;
r_phi0=[1 1];
z_phi0=[1 1];
r1=r_phi0;
z1=z_phi0;


% -------------------------------------------------------------------
% Initial computation of Rayleigh length, angle etc...
% -------------------------------------------------------------------
%
% theta_0 is the angle between the beam from Mir#3 to Mir#4 and the horizontal
theta_0 = atan2(p4(3)-p3(3),p4(1)-p3(1)) * 180/pi;
if idisplay; disp(sprintf('Angle theta_0 between Mir#3-Mir#4 and the horizontal %f',...
    theta_0)); end;

% The relation between Mir#4 tilt measured with respect to the horizontal
% and theta_l is
% delta_mir = (pi/2 - theta_0 - 2*theta_l)/2
%
% old version: delta_mir = ( theta_0 - theta_l)/2
% delta_mir = (180 + theta_0 + theta_l)/2;
delta_mir = -(180 - theta_0 -theta_l)/2;
if idisplay;
  disp(sprintf('Relation between Mir#4 tilt and theta_l %f',delta_mir));
end;
%
% Let's find a point p_i on the mirror surface (the closest to the rotation axis)
%
%
%  This is a bug! delta_mir is the angle of the mirror to the horizontal, so
%    the cos and sin are reversed (SC, 11/05/07)
%p_i = [pp(1) + r_p * cos(pi/180*delta_mir)  0  pp(3) + r_p * sin(pi/180*delta_mir) ];
p_i = [pp(1) + r_p * sin(pi/180*delta_mir)  0  pp(3) - r_p * cos(pi/180*delta_mir) ];

%
% Now we know: - The coordinates of the mirror (1 point plus direction)
%              - The beam trajectory
% Let'find the intersection p4 of the central ray coming from mir#3 and mir#4

a1 = tan(delta_mir * pi/180);
a2 = tan(theta_0   * pi/180);
b1 = p_i(3) - p_i(1)*a1;
b2 = p3(3)  - p3(1)*a2;
[p4(1),p4(3)]=line_intersect_2d(a1,b1,a2,b2);

%
% The distance d2_1 from mir #3 (p3) and the intersection point on mir #4 (p4)is thus
%
d2_1 = sqrt((p3(1)-p4(1))^2 + (p3(3)-p4(3))^2);

% and the distance d2_2 from mir#4 to the beam waist is
d2_2 = d2 - d2_1;

%
% The reflected ray goes through p4 and travels with an angle -theta_l
%
a3 = tan(theta_l * pi/180);
b3 = (p4(3) - p4(1)*a3);
r_phi0 = linspace(130,50,3);
z_phi0 = a3*r_phi0 + b3;

% -------------------------------------------------------------------
% -------------------------------------------------------------------
%       Estimate distance from beam waist to plasma surface...
% -------------------------------------------------------------------
% -------------------------------------------------------------------
%
n_int = 0;           % n_int = number of found intersection (<= 2)
D_int = [];          % D_int = distance from mir #4 to plasma



% -------------------------------------------------------------------
%       ECRH +  ECCD cases
% -------------------------------------------------------------------
% The problem is 3D.
% - Rotate the reflected ray around launcher axis
% - Determine the direction (dx,dy,dz) of the ray
% - Find its intersection with the plasma boundary
% - Top view: X_axis -> W-E
%             Y_axis -> S-N
%             Z_axis -> Vertical

%%% if phi_l~=0 & phi_l~=180

%
% In cartesian coordinate we know that the beam passes through the point
% p4 = (x0,y0,z0) on mirror #4
% and its direction is given by the unit vector(dx,dy,dz)

x0 = p4(1);
y0 = p4(2)  + (p4(3) - l_axis) * sin(phi_l*pi/180);
z0 = l_axis + (p4(3) - l_axis) * cos(phi_l*pi/180);
%
dx =  - cos(theta_l*pi/180);
dy =  - sin(theta_l*pi/180) * sin(phi_l*pi/180);
dz =  - sin(theta_l*pi/180) * cos(phi_l*pi/180);

check = dx^2 +dy^2 +dz^2;



% the ray equation is thus (x,y,z) = (x0,y0,z0) + a (dx,dy,dz)
%
%
% Let's now find the intersection of the ray with the surface
% To do that, we construct 4 (size(A)-1)*size(npts) matrices
% containing the slopes a and the origin abscissae b of all the segments
% of the ray and the plasma profile.

A = linspace(0,80,31);
npts=length(r_ctr1);
z1 = z0 + A*dz;
r1 = sqrt((x0 + A*dx).^2 + (y0+A*dy).^2);
z2 = [z1(2:length(A)) z1(length(A))+eps];
r2 = [r1(2:length(A)) r1(length(A))+eps];

% r,z,r1,z1 are row vectors of dimension A
% the a1, b1 and a2, b2  matrices represent the slopes and
% abscissae at origin of every segment of the beam path (a1,b1)
% and the plasma boundary contour (a2,b2)

[tmp1 R1]=polyfit(r1,z1,1);
a1=tmp1(1)*ones(npts,length(A));
b1=tmp1(2)*ones(npts,length(A));
r_ctr2 = [r_ctr1(2:npts)' r_ctr1(1)]';
z_ctr2 = [z_ctr1(2:npts)' z_ctr1(1)]';
a2 = (z_ctr2 -z_ctr1)./((r_ctr2-r_ctr1)+eps);
b2 =  z_ctr1 - r_ctr1.*a2;
a2 = repmat(a2,1,length(A));
b2 = repmat(b2,1,length(A));

% Calculate all the intersections
[x_int,z_int] = line_intersect_2d(a1,b1,a2,b2);

% The matrices testseg1 and testseg2 contain informations on whether the intersections
% of a segment of the beam path and a segment of the plasma profile lies in the segment itself.

testseg1 = (repmat(r2,npts,1)-x_int)./((repmat(r2,npts,1)-repmat(r1,npts,1))+eps);
testseg2 = (repmat(r_ctr2,1,length(A))-x_int)./((repmat(r_ctr2,1,length(A))-repmat(r_ctr1,1,length(A)))+eps);

% Find the intersections: [k] contains the indices of intersections
[k] = find(0<=testseg1 & testseg1 <=1 & 0<=testseg2 & testseg2 <=1);
n_int=length(k);

if ~n_int
  disp(' ***********************************************************************')
  disp(' bad angles given, cannot find intersection with plasma')
  disp(' ***********************************************************************')
  x=[];
  y=[];
  zec=[];
  phi_p=[];
  R_c=[];
  w_p=[];
  w02=[];
  varargout{1} = {};
  return
end

if abs(dz) > 1.e-8;
  x_int = x0 + (z_int-z0) * dx/dz;
  y_int = y0 + (z_int-z0) * dy/dz;
else
  x_int =     - x_int * cos((180-theta_l)*pi/180);
  if abs(dx) > 1.e-8;
    y_int =  y0 + (x_int - x0) * dy/dx ;
  else
    y_int = y0;
  end
end

% D_int is the distance from p4 (intersection on mirror #4) and the plasma
D_int = sqrt((x0 -x_int(k)).^2 + (y0-y_int(k)).^2 + (z0-z_int(k)).^2);

% D_bwp is the distance from beam waist to plasma ( <0 if bw is inside plasma)
D_bwp = D_int - d2_2;



if idisplay
  disp(' ');
  disp(sprintf('Number of intersections found: %0.2i ',n_int));
  disp(' ');
  for i_int=1:n_int
    disp(sprintf('Intersection %0.2i: %f %f %f ',i_int,x_int(k(i_int)),y_int(k(i_int)),z_int(k(i_int))));
  end
end



% Take the first intersection only and determine whether it is in or outside the plasma
[jmin] = find(min(D_int));
D=min(D_int);
Dbwp = D_bwp(jmin);
if Dbwp <=0
  if idisplay
    disp(' WARNING: BEAM WAIST IS INSIDE PLASMA');
  end
  Dbwp = -1;
else
  Dbwp =  1;
end

% Estimate the radius of curvature R_c at the plasma entrance point
% (negative if beam waist is inside plasma)
R_c = R(w0,freq*1e-9,(D-d2_2)*10)/10;

%R_c = abs(R_c);


% Estimate the spot-size w_p at plasma
[w_p,Zr_p] = wz(w0,freq*1e-9,(D-d2_2)*10);
w_p=w_p/10;
Zr_p=Zr_p/10;

% Estimate the aperture angle phi_p (in POWER) at the plasma edge
phi_p = atan2(w_p,sqrt(2)*R_c)*180/pi;


% Estimate the point from which the rays are issued
x = x_int(k(jmin)) - R_c*dx;
y = y_int(k(jmin)) - R_c*dy;
z = z_int(k(jmin)) - R_c*dz;

% Estimate Smin and Smax, distances from ray origin to plasma interface
S = sqrt((x_int(k)-x).^2 + (z_int(k)-z).^2);


% Correction to toroidal angle
phi_cor = atan(y/x) *180/pi;
phi_cor = 0.;


% Initial estimation for theta_t (i.e. theta_toray )and phi_t
[theta_t,phi_t]=trans_launch_to_toray(theta_l,phi_l,launcher);

if idisplay
  disp(' ');
  
  disp(sprintf('Launcher poloidal angle (theta_l)             : %2.7g  [deg]',theta_l));
  disp(sprintf('Launcher toroidal angle (thetec)              : %2.7g  [deg]',phi_l));
  disp(sprintf('Toray poloidal angle (thetec)                 : %2.7g  [deg]',theta_t));
  disp(sprintf('Toray toroidal angle (phaiec)                 : %2.7g  [deg]',phi_t));
  disp(sprintf('Plasma z_axis                                 : %2.7g  [deg]',z_axis));
  disp(sprintf('Correction applied to phaiec                  : %2.7g  [deg]',phi_cor));
  disp(sprintf('Distance between mirror 4 and plasma          : %0.5g  [cm]',D));
  disp(sprintf('Distance between the beam waist and the plasma: %0.5g  [cm]',D-d2_2));
  disp(sprintf('Distance between mirror 4 and the beam waist  : %0.5g  [cm]',d2_2));
  disp(sprintf('Radius of curvature at the plasma             : %0.5g  [cm]',R_c));
  disp(sprintf('Spot-size at the plasma                       : %0.5g  [cm]',w_p));
  disp(sprintf('Beam aperture angle (in POWER) at the plasma  : %0.5g  [deg]',phi_p));
  disp(sprintf('Coordinate of central ray on mir #4 : %f %f %f',x0,y0,z0))
  disp(sprintf('Origin of rays                      : %f %f %f',x,y,z));
  disp(' ')
end

varargout{1}.x0 = x0;
varargout{1}.y0 = y0;
varargout{1}.z0 = z0;
varargout{1}.x = x;
varargout{1}.y = y;
varargout{1}.z = z;
varargout{1}.R_m=[R_c R_c];
varargout{1}.w_m=[w_p w_p];
varargout{1}.Z_m=[Zr_p Zr_p];

% -------------------------------------------------------------------
% Generate input for TORAY ...
% -------------------------------------------------------------------
xec = sqrt(x^2 + y^2) * Dbwp;  % Dbwp = -1 if beam waist inside plasma
zec = z-z_axis;
thetec = theta_t;
phaiec = phi_t -phi_cor;

%disp('FOR TORAY (version without ray flexibility):');
%disp('--------------------------------------------')
%disp('')
%disp(sprintf('XEC   : \t \t %0.5g  [cm]',xec));
%disp(sprintf('ZEC   : \t \t %0.5g  [cm]',zec));
%disp(sprintf('THETEC: \t \t %0.5g  [deg]',thetec));
%disp(sprintf('PHAIEC: \t \t %0.5g  [deg]',phaiec));
%disp(sprintf('HLWEC : \t \t %0.5g  [deg]',phi_p/1.3));
%disp(' ')
%disp('Note  : HLWEC = Aperture angle (arbitrarily) divided by 1.3 (JPH 8.6.98) ');
%disp(' ')


%
% -------------------------------------------------------------------
% Make a plot...
% -------------------------------------------------------------------
%
%%   x0 = p4(1);
%%   y0 = p4(2)  + (p4(3) - l_axis) * sin(phi_l*pi/180);
%%   z0 = l_axis + (p4(3) - l_axis) * cos(phi_l*pi/180);


%figure(11)
%hold off

% Draw plasma boundary
%plot(r_ctr1(1:npts),z_ctr1(1:npts))

%Title = sprintf('Shot #%i at time %f with theta_l = %f and phi_l =%f',shot,time,theta_l,phi_l);
%title(Title)

% Draw ray path (valid for phi_l = 0)
% -----------------------------------
% hold on
%plot(r_phi0,z_phi0,'r-.')
%plot(r1,z1,'b--')

% Draw p_i (closest mir point to the rotation point)
% --------------------------------------------------
% initial position
%plot(p_i(1),p_i(3),'r*')
% 'Rotated' position
p_i(3) = l_axis + (p_i(3) - l_axis) * cos(phi_l*pi/180);
%plot(p_i(1),p_i(3),'b*')

% Draw Beam waist
p_s(1) = p4(1) + d2_2*cos(theta_l*pi/180 + pi);
p_s(3) = p4(3) + d2_2*sin(theta_l*pi/180 + pi);
%plot(p_s(1),p_s(3),'r+')
p_s(3) = l_axis + (p_s(3) - l_axis) * cos(phi_l*pi/180);
%plot(p_s(1),p_s(3),'b+')
%text(p_s(1)+2,p_s(3),'Beam Waist')

% Draw Incidence point on mirror #4
%plot(p4(1),p4(3),'r+')
p4(3) = l_axis + (p4(3) - l_axis) * cos(phi_l*pi/180);
%plot(p4(1),p4(3),'b+')
%text(p4(1)+2,p4(3),'Incidence point on mirror 4')

% Draw rays origin
%plot(xec,z,'b+')
%text(xec+2,z,'Rays origin')

% Draw intersection with plasma boundary
%plot(x_int(k),z_int(k),'ksquare')
%axis('equal')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% end function launch_point_16 %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function [x,y,zec,thetec,phaiec,phi_p,r_phi0,z_phi0,r1,z1,w_p,w02,beamcoord,varargout] = ...
  launch_point_79(launcher,freq,theta_l,phi_l,r_ctr1,z_ctr1,z_axis,npts,idisplay,ct,cr,alpha);

%---------------------------------------------------------------------
%calculate launch point for X3 launcher
%
%NOTE : phi_l is the mirror radial position, not an angle !!!
%       all distances in cm!
%
% SC, 24/02/12: made w02 into a 2-element vector (for elliptical beam) and
%   added argument beamcoord (3x3 matrix of cartesian unit direction vectors for
%   astigmatic beam plane (first two columns) and beam propagation (third
%   column); x,y,zec,w_p,phi_p are also 2-element vectors
%---------------------------------------------------------------------

nverbose = 1;

if nargin<12|isempty(alpha),
  alpha=0;%mirror's rotation around R, towards sector 2
end
% if nargin<11|isempty(cr),
%   cr=1/113.14;%mirror's curvature in the radial direction (shots 34580-45398)
  %cr=1/98.9;% (until shot 34579)
  cr=1/134.35;% (from 45399)
% end
% if nargin<10|isempty(ct),
%   ct=1/56.6;%mirror's curvature in the toroidal direction (shots 34580-45398)
  %ct=1/197.8;% (until shot 34579)
  ct=0.;% (from 45399)
% end
r=phi_l;
phi_f=0;
p4=[r 0 98.85];
l0=12.45;
if launcher==7
  phi_f=4;
end
if launcher==9
  phi_f=-4;
end
p4(2)=(r+l0)*tan(phi_f*pi/180);

theta_f=theta_l;
tf=theta_f*pi/180;
pf=-phi_f*pi/180;%minus sign to be consistent with x3_beam_angles_b.tex
af=alpha*pi/180;

% these formulas are as in x3_beam_angles_b.tex
Q=cos(af)*cos(tf)-sin(af)*tan(pf);
% Eq. 20
nx=-cos(af)*cos(tf)+(r+l0)*tan(pf)*sin(af)/Q*((cos(tf))^2*ct+(sin(tf))^2*cr);
ny=-sin(af)-(r+12.45)*tan(pf)*cos(af)*cos(tf)*ct/Q;
nz=cos(af)*sin(tf)+(r+l0)*tan(pf)*sin(af)*cos(tf)*sin(tf)/Q*(cr-ct);
N=sqrt(nx^2+ny^2+nz^2);
nx=nx/N;ny=ny/N;nz=nz/N;
idotn=-cos(pf)*nx+sin(pf)*ny;
% Eq. 18
dx=-cos(pf)-2*idotn*nx;
dy=sin(pf)-2*idotn*ny;
dz=-2*idotn*nz;
norme=sqrt(dx^2 +dy^2+dz^2);
dx=dx/norme;
dy=dy/norme;
dz=dz/norme;
check=sqrt(dx^2 +dy^2+dz^2);
dr=sqrt(dx.^2+dy.^2);

etaprime=[-sin(af)*cos(tf);cos(af);sin(af)*sin(tf)];%M (Eq. 4) times eta
beamcoord(:,1)=etaprime-dot(etaprime,[dx;dy;dz])*[dx;dy;dz];
beamcoord(:,1)=beamcoord(:,1)/norm(beamcoord(:,1));
beamcoord(:,3)=[dx;dy;dz];
beamcoord(:,2)=cross(beamcoord(:,3),beamcoord(:,1));

% higher-order corrections to p4 (Eq. 10)
p4(1)=p4(1)+(r+12.45)*sin(af)*tan(pf)/Q;
p4(2)=-(r+12.45)*cos(af)*cos(tf)*tan(pf)/Q;

if abs(dx)>2*eps
  phaiec=atan2(dy,dx)*180/pi;
else
  if abs(dy)>2*eps
    phaiec=sign(dy)*90;
  else
    phaiec=0
  end
end

if dr>2*eps
  if dx<0
    thetec=90+abs(atan2(dz,dr)*180/pi);
  else
    thetec=(90+abs(atan2(dz,dr)*180/pi));
  end
else
  thetec=180;
end

x0=p4(1);
y0=p4(2);
z0=p4(3);
A=linspace(0,200,31);
r1=sqrt((x0+A*dx).^2+(y0+A*dy).^2);
x1=x0+A*dx;
y1=y0+A*dy;
z1=z0+A*dz;
r_phi0=r1;
z_phi0=z1;
z2 = [z1(2:length(A)) z1(length(A))+eps];
r2 = [r1(2:length(A)) r1(length(A))+eps];

%find intersection between plasma boundary and ray
%
%!!!all functions are r(z) and not z(r)
%

[tmp1 R1] = polyfit(z1,r1,1);
a1 = tmp1(1)*ones(length(r_ctr1),length(A));
b1 = tmp1(2)*ones(length(r_ctr1),length(A));
r_ctr2 = [r_ctr1(2:npts)' r_ctr1(1)]';
z_ctr2 = [z_ctr1(2:npts)' z_ctr1(1)]';
a2 = (r_ctr2 -r_ctr1)./((z_ctr2-z_ctr1)+eps);
b2 =  r_ctr1 - z_ctr1.*a2;
a2 = repmat(a2,1,length(A));
b2 = repmat(b2,1,length(A));

% Calculate all the intersections
[z_int,x_int] = line_intersect_2d(a1,b1,a2,b2);

% Find the intersections: [k] contains the indices of intersections
testseg1 = (repmat(z2,npts,1)-z_int)./((repmat(z2,npts,1)-repmat(z1,npts,1))+eps);
testseg2 = (repmat(z_ctr2,1,length(A))-z_int)./((repmat(z_ctr2,1,length(A))-repmat(z_ctr1,1,length(A)))+eps);
[k] = find(0<=testseg1 & testseg1 <=1 & 0<=testseg2 & testseg2 <=1);
n_int=length(k);

if ~n_int
  if (nverbose>=1)
    disp(' ***********************************************************************')
    disp(' bad angles given, cannot find intersection with plasma')
    disp(' ***********************************************************************')
  end
  x=[];
  y=[];
  zec=[];
  R_c=[];
  phi_p=[];
  w_p=[];
  w02=[];
  return
end

% $$$ if abs(dx) > eps;
x_int = x0 + (z_int-z0) * dx/dz;
y_int = y0 + (z_int-z0) * dy/dz;
% $$$ else
% OS: This else part should be just x_int=x0*ones(size(x_int); and  y_int = y0*ones(size(x_int)); if dx small
% $$$   x_int =     - x_int * cos((180-theta_l)*pi/180);
% $$$   if abs(dx) > 1.e-8;
% $$$     y_int =  y0 + (x_int - x0) * dy/dx ;
% $$$   else
% $$$     y_int = y0*ones(size(x_int));
% $$$   end
% $$$ end

% D_int is the distance from p4 (intersection on mirror #4) and the plasma
D_int = sqrt((x0 -x_int(k)).^2 + (y0-y_int(k)).^2 + (z0-z_int(k)).^2);
% Take the first intersection only
[jmin] = find(min(D_int));
D=min(D_int);

%beam construction (waist, distance,...) -> go from cm to mm

% toroidal and radial focal lengths (Eqs. 36 and 30) -> mm
f=[0.5/(cos(tf)*cos(af)*ct-sin(af)*tan(af)*cos(tf)*ct- ...
        sin(af)*tan(af)*sin(tf)*tan(tf)*cr); ...
   1/(4*cr*cos(tf)*cos(af))]*10;




d1=abs(727.5+880-p4(1)*10);         %distance between wave guide and mirror
% Up to 2011 the waist was assumed to be identical to X2, which is incorrect.
% The X3 waveguide has a downtaper with mouth diameter 59.2 mm (instead of
%   63.5 mm); we multiply this by 0.5 (half-width) and by 0.592 (to get to the
%   waist). This would give 17.52 mm.
% JPhH has done a more accurate determination and finds 16.9 mm
%w01=18.8;
%w01=17.52;
w01=16.9;                           %beam waist @ wave guide [mm]
lambda=(3e8/freq)*1e3;              %wave length [mm]
for i=1:2,
  if isinf(f(i)),
    w02(i)=w01;
    d2(i)=-d1;
  else
    w02(i)=sqrt((w01^2*f(i)^2)/((d1-f(i))^2+pi^2*w01^4/lambda^2)); %Minimum waist in the plasma [mm]
    % these formulas are incorrect in general - they always give a positive d2,
    %   whereas the correct formula can give a negative value; the two do coincide
    %   when the result is positive, as was the case for the pre-2012 mirror
    %   (SC, 09/03/12)
    %  f1(i)=(pi/lambda)*w01*w02(i);
    %  d2(i)=f(i)+(w02(i)/w01)*sqrt(f(i)^2-f1(i)^2);      %distance from mirror to beam waist w0 [mm]
    d2(i)=f(i)+f(i)^2*(d1-f(i))/(pi^2*w01^4/lambda^2+(d1-f(i))^2);
  end
end
w02=w02(:);
%f1=f1(:);
d2=d2(:);

% all distances from here again in cm
d2=d2/10;
w02=w02/10;
% Estimate the radius of curvature R_c at the plasma entrance point
% (negative if beam waist is inside plasma)
R_c = R(w02*10,freq*1e-9,(D-d2)*10)/10;

[w_p,Zr_p]=wz(w02*10,freq*1e-9,(D-d2)*10);
w_p=w_p/10;                         %Beam waist at plasma entrance

% Estimate the aperture angle phi_p (in POWER) at the plasma edge
phi_p = atan2(w_p,sqrt(2)*R_c)*180/pi;

% Estimate the points from which the meridian and sagittal ray projections are
%   issued
x = x_int(k(jmin)) - R_c*dx;
y = y_int(k(jmin)) - R_c*dy;
z = z_int(k(jmin)) - R_c*dz;
zec = z-z_axis;


% added for Oulfa and TORBEAM 25/04/2017: at mirror position (D=0): (Radius of curvature, waist (1/e) E field, radius, and Z)

R_m = R(w02*10,freq*1e-9,(0-d2)*10)/10;

[w_m,Zrr]=wz(w02*10,freq*1e-9,(0-d2)*10);
w_m=w_m/10;
Z_m=Zr_p;


if (nverbose >= 3)
  disp(        '*-------------------------------------------------------------*')
  disp(        '*-------------- beam parameters ------------------------------*')
  disp(        '*-------------------------------------------------------------*')
  disp(        '|                                                             |')
  disp(sprintf('|   Radial spot-size w02 (in power) = %1.2f mm                |',w02(2)*10/sqrt(2)))
  disp(sprintf('|   Toroidal spot-size w02 (in power) = %1.2f mm              |',w02(1)*10/sqrt(2)))
  disp(sprintf('|   Radial waist at boundary w_p (in power) = %1.2f mm        |',w_p(2)*10/sqrt(2)))
  disp(sprintf('|   Toroidal waist at boundary w_p (in power) = %1.2f mm      |',w_p(1)*10/sqrt(2)))
  disp(sprintf('|   Wavefront (radial) radius at boundary R_c = %1.2f mm   |',R_c(2)*10))
  disp(sprintf('|   Wavefront (toroidal) radius at boundary R_c = %1.2f mm  |',R_c(1)*10))
  disp(        '|                                                             |')
  disp(sprintf('|		 and beta=%1.1f deg                            |',thetec-90))
  disp(        '|                                                             |')
  disp(        '*-------------------------------------------------------------*')
  disp(' ')
end

varargout{1}.x0 = x0;
varargout{1}.y0 = y0;
varargout{1}.z0 = z0;
varargout{1}.x = x;
varargout{1}.y = y;
varargout{1}.z = z;
varargout{1}.R_m=R_m;
varargout{1}.w_m=w_m;
varargout{1}.Z_m=Z_m;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%% end function launch_point_79 %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
