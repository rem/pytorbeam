#!/usr/bin/env python
'''Suite of tests to be run under python2/3+hypothesis
'''

import os
import sys
import math
import numpy as np
#from scipy.interpolate import interp1d
import ctbm
#from IPython import embed

try:
    import pytest
    from hypothesis import given, strategies as st
    from hypothesis import settings
    #embed()
except ImportError as err:
    print("Need module hypothesis for running unittests")
    print(err)
    print("Exiting now.")
    sys.exit(1)

BASEPATH = '../libtorbeam/tests/'

def decode_libdir(ldir):
    ''' Backconvert encoded pathspec into proper string
    '''
    res = '../'+ldir.replace('^', '/')[3:] if ldir.startswith('^^^') else ldir.replace('^', '/')
    return res

@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()

    # we only look at actual failing test calls, not setup/teardown
    if rep.when == "call" and rep.failed:
        mode = "a" if os.path.exists("failures") else "w"
        with open("failures", mode) as ffile:
            # let's also access a fixture for the fun of it
            if "tmpdir" in item.fixturenames:
                extra = " (%s)" % item.funcargs["tmpdir"]
            else:
                extra = ""
            ffile.write(rep.nodeid + extra + "\n")

@given(c=st.integers(min_value=0, max_value=1),
       a=st.integers(min_value=0, max_value=1))
def test_cd_and_absorb_aug(c, a, libdir):
    path = BASEPATH + 'test_rem'
    m, n, eqdata = ctbm.readTopfile3(path+'/topfile')
    k, l, prdata = ctbm.readPrdata(path+'/ne.dat', path+'/Te.dat')
    intinbeam, dblinbeam = ctbm.readInbeam(path+'/inbeam.dat')
    # make sure, routine returns rho[0] as rho
    intinbeam[6] = intinbeam[6] % 10
    # avoid extra output
    intinbeam[8] = 1
    # randomize absorption method and profile calculation
    intinbeam[11] = c
    intinbeam[13] = a

    result = ctbm.call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata,
                               mode=ctbm.TBM_MODE_NORMAL, libdir=decode_libdir(libdir))
    rhoresult = result[0]
    assert rhoresult[19] == 0
    sys.stdout.write('.')
    sys.stdout.flush()
    assert np.abs(rhoresult[0]-0.6524) < 1e-3

@given(path=st.sampled_from(['test_X3']))
def test_nag_relativistic(path, libdir):
    path = BASEPATH + path
    m, n, eqdata = ctbm.readTopfile3(path+'/topfile')
    k, l, prdata = ctbm.readPrdata(path+'/ne.dat', path+'/Te.dat')
    intinbeam, dblinbeam = ctbm.readInbeam(path+'/inbeam.dat')
    intinbeam[6] = intinbeam[6] % 10
    intinbeam[8] = 1    # no extra output
    result = ctbm.call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata,
                               mode=ctbm.TBM_MODE_NORMAL, libdir=decode_libdir(libdir))
    rhoresult = result[0]
    assert rhoresult[19] == 0
    assert np.abs(rhoresult[0] - 0.161) < 1e-3

@given(path=st.sampled_from([BASEPATH + 'test_rem', 'tcvdata/', 'augdata/', \
                             BASEPATH + 'test_rtprofiles']))
def test_cases_B(path, libdir):
    m, n, eqdata = ctbm.readTopfile3(path+'/topfile')
    k, l, prdata = ctbm.readPrdata(path+'/ne.dat', path+'/Te.dat')
    intinbeam, dblinbeam = ctbm.readInbeam(path+'/inbeam.dat')
    intinbeam[6] = intinbeam[6] % 10
    intinbeam[8] = 1
#      if verbose:
#        ctbm.writeDump(path[-8:]+'_'+re.findall('\w+:\w+:\w+', time.ctime())[0].replace(':',''),
#                intinbeam, dblinbeam, k, l, prdata, m, n, eqdata)
#    rhoresult, t1data, t1tdata, t2data, t2n_data, icnt, ibgout, nv, volprof = \
#        ctbm.call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata,
#                          mode=ctbm.TBM_MODE_NORMAL, libdir=decode_libdir(libdir))
    result = ctbm.call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata,
                               mode=ctbm.TBM_MODE_NORMAL, libdir=decode_libdir(libdir))
    rhoresult = result[0]
    assert rhoresult[19] == 0
    if settings.verbosity.name == "verbose":
        print(path, rhoresult[0], '(0.32 < x < 0.68)\n')
    else:
        sys.stdout.write('.')
        sys.stdout.flush()
    if '_rem' in path:
        assert np.abs(rhoresult[0]-0.653135033843) < 6e-5
    if '_rtprofiles' in path:
        assert np.abs(rhoresult[0]-0.40630368094) < 5e-4
    if 'tcvdata' in path:
        assert np.abs(rhoresult[0]-0.584773270962) < 1e-5
    if 'augdata' in path:
        assert np.abs(rhoresult[0]-0.354880205877) < 1e-5

    assert np.abs(rhoresult[0]-0.5) < 0.16

@given(xf=st.floats(min_value=75e9, max_value=105e9))
def test_refllib(xf, libdir):
    # set up an approximate lookup table for results, using a spline
    # values calculated with Torbeam rev. 455
    xxf = np.array([75., 80., 85., 90., 91., 92., 93., 94., 95., 96., 97., 98., 99.4,\
                    100.8, 102.2, 103.6, 105.])*1e9
    yyf = np.array([1.00032326, 0.98881804, 0.97743859, 0.962228, 0.95798597,\
                    0.9517868, 0.94558764, 0.93658061, 0.92301162, 0.9012845,\
                    0.87839478, 0.85550505, 0.82345944, 0.78731942, 0.74860689,\
                    0.70989436, 0.67004873])
    #myspline = interp1d(xxf, yyf)
    # directory for base-cas
    path = BASEPATH + 'test_aug_refl/'
    m, n, eqdata = ctbm.readTopfile3(path+'topfile')
    k, l, prdata = ctbm.readPrdata(path+'ne.dat', path+'/Te.dat')
    intinbeam, dblinbeam = ctbm.readInbeam(path+'/inbeam.dat')
    intinbeam[8] = 1
    # change base case to the parameter varation for this test run
    dblinbeam[0] = xf
    target_rho = np.interp(xf, xxf, yyf)
    mode = ctbm.TBM_MODE_REFL | ctbm.TBM_MODE_NORMAL
    #rhoresult, t1data, t1tdata, t2data, t2n_data, icnt, ibgout, nv, volprof, reflout = \
    result = ctbm.call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata,
                               mode=mode, libdir=decode_libdir(libdir))
    reflout = result[9]
    assert result[0][19] == 3
    rhoturn = reflout[0, np.argmin(reflout[2, :])]
    if settings.verbosity.name == "verbose":
        print("xf:", xf/1e9, "e9 tgtrho:", target_rho, " got: ", rhoturn, " delta: ", \
            np.abs(rhoturn - target_rho))
    else:
        sys.stdout.write('.')
        sys.stdout.flush()

    assert np.abs(rhoturn - target_rho) < 5e-3

@given(xf=st.floats(min_value=130e9, max_value=145e9))
def test_rtlib(xf, libdir):
    path = BASEPATH + 'test_rem/'
    m, n, eqdata = ctbm.readTopfile3(path+'topfile')
    k, l, prdata = ctbm.readPrdata(path+'ne.dat', path+'/Te.dat')
    intinbeam, dblinbeam = ctbm.readInbeam(path+'/inbeam.dat')
    dblinbeam[0] = xf
    intinbeam[8] = 1
    #rhonormal, t1data, t1tdata, t2data, t2n_data, icnt, ibgout, nv, volprof =\
    result = ctbm.call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata,
                               mode=ctbm.TBM_MODE_NORMAL, libdir=decode_libdir(libdir))
    rhonormal = result[0]
    assert rhonormal[19] == 0

    rhoresult = ctbm.call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata,
                                  mode=ctbm.TBM_MODE_RT, libdir=decode_libdir(libdir))
    assert rhoresult[19] == 0
    if settings.verbosity.name == "verbose":
        print("chosen freq: ", dblinbeam[0], " rho_rt: ", rhoresult[0], " rho_norm: ",\
          rhonormal[0], " delta: ", np.abs(rhoresult[0]-rhonormal[0]))
    else:
        sys.stdout.write('.')
    sys.stdout.flush()

    assert np.abs(rhonormal[0]-rhoresult[0]) < 1e-3

@given(xf=st.floats(min_value=165e9, max_value=180e9))
def test_iter_rngfreq(xf, libdir):
    path = BASEPATH + 'test_iter/'
    m, n, eqdata = ctbm.readTopfile3(path+'topfile')
    k, l, prdata = ctbm.readPrdata(path+'ne.dat', path+'/Te.dat')
    intinbeam, dblinbeam = ctbm.readInbeam(path+'/inbeam.dat')
    dblinbeam[0] = xf
    intinbeam[8] = 1
    #rhonormal, t1data, t1tdata, t2data, t2n_data, icnt, ibgout, nv, volprof = \
    result = ctbm.call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata,
                               mode=ctbm.TBM_MODE_NORMAL | ctbm.TBM_MODE_ITER,
                               libdir=decode_libdir(libdir))
    rhonormal = result[0]
    assert rhonormal[19] == 0
    if settings.verbosity.name == 'verbose':
        print("chosen freq: ", dblinbeam[0], " rho_norm: ", rhonormal[0])
    else:
        sys.stdout.write('.')
        sys.stdout.flush()

    assert np.abs(rhonormal[0]-0.5) < 5e-1

def random_inputs(xf, ints, dbls, cd, harm, int11, tmpdir, libdir):
    path = BASEPATH + 'test_rem/'
    m, n, eqdata = ctbm.readTopfile3(path+'topfile')
    k, l, prdata = ctbm.readPrdata(path+'ne.dat', path+'/Te.dat')
    intinbeam, dblinbeam = ctbm.readInbeam(path+'inbeam.dat')
    intinbeam[2] = -1   # X-mode for now
    intinbeam[3] = 1    # power absorption on (normal case)
    intinbeam[4] = int(cd > 0)             # cd on or off ?
    intinbeam[6] = intinbeam[4] * (cd -1)  # 0 if off, 0-2, if on
    intinbeam[8] = 1    # no extra output
    if len(ints) < 5:
        ints.extend([0, 0, 0, 0, 0])
    intinbeam[9] = ints[0]
    intinbeam[10] = harm
    intinbeam[11] = int11
    intinbeam[12] = 0
    intinbeam[13] = ints[1]
    intinbeam[14] = ints[2]
    intinbeam[49] = ints[3]
    if len(dbls) < 6:
        dbls.extend([0.01, 0.01, 0.01, 0.01, 0.01, 0.01])
    dblinbeam[0] = xf   # random gyrotron freq
    dblinbeam[1] = dblinbeam[1] - 2.5 + dbls[0] / math.pi * 20
    dblinbeam[2] = dblinbeam[2] - 2.5 + dbls[1] / math.pi * 20
    dblinbeam[3] = dblinbeam[3] + dbls[2]
    dblinbeam[5] = dblinbeam[5] + dbls[3]
    dblinbeam[23] = dbls[4]
    dblinbeam[34] = 1 + dbls[5]
    # fix invalid parameter combination with NAG-using relativistic absorption
    intinbeam[10] = intinbeam[10] if intinbeam[9] == 0 else max(3, intinbeam[9])
    result = ctbm.call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata,
                               mode=ctbm.TBM_MODE_NORMAL, libdir=decode_libdir(libdir))
    rhoresult = result[0]
    if settings.verbosity.name == "verbose":
        print('--------- ', dblinbeam[0], intinbeam[4], intinbeam[10], '=>', rhoresult[0])
    else:
        sys.stdout.write('.')
    sys.stdout.flush()

    #ctbm.writeInbeam(intinbeam, dblinbeam, fname=tmpdir+'/inbeam.dat')
    #ctbm.writeTopfile(m, n, eqdata, fname=tmpdir+'/topfile')
    #ctbm.writeKinetic(k, l, prdata, fnamene=tmpdir+'/ne.dat', fnameTe=tmpdir+'/Te.dat')

    return rhoresult[0]

@given(xf=st.floats(min_value=130e9, max_value=145e9),
       ints=st.lists(elements=st.integers(min_value=0, max_value=1), max_size=4),
       cd=st.integers(min_value=0, max_value=3),
       harm=st.integers(min_value=1, max_value=5),
       dbls=st.lists(st.floats(min_value=0.01, max_value=1.0), max_size=6))
def test_random_inputs_westerhof(xf, ints, dbls, cd, harm, libdir):
    result = random_inputs(xf, ints, dbls, cd, harm, 0, 'tmpdir', libdir)

    assert np.abs(result-0.5) <= 0.5
