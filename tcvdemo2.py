#!/usr/bin/env python

import os
import sys
import numpy as np
import scipy as sp
from itertools import chain
from IPython import embed

try:
    import eqtools as eqt
except Exception as e:
    print ("Problem loading equilibrium tools")
    print ("OS thinks:")
    print (e)
    sys.exit(1)

import ctbm
from tbmdata_tcv import beam_param_tcv

ECsystem = 4
time = 1.0
shot = 53049

noplot = 0

class MyError(Exception):
    def __init__(self, value):
        Exception.__init__()
        self.value = value
    def __str__(self):
        return repr(self.value)

if len(sys.argv) > 1:
    if 'noplot' in sys.argv:
        noplot = 1
        sys.argv.remove('noplot')

    try:
        if '-h' in sys.argv or 'help' in sys.argv:
            raise MyError('Please try again.')
        shot = int(sys.argv[1])
        if (shot < 0 or shot > 100000):
            raise MyError('Shotnumber out of range')
    except Exception as e:
        print("\nUsage:")
        print(sys.argv[0], " [shot] [time] [ECsys] [noplot] [generate_infiles]")
        print(" ")
        print("Without arguments, script loads data from dataset tcvdata")
        print("'noplot' anywhere in the argument list skips plotting\n")
        print("Parameter generate_infiles triggers writing of regular")
        print("TORBEAM input files with underscore at beginning:")
        print("_ne.dat, _Te.dat, _inbeam.dat and _topfile\n")
        print(e)
        print('\n')
        sys.exit(0)

    if np.size(sys.argv) > 2:
        time = float(sys.argv[2])
    if np.size(sys.argv) > 3:
        ECsystem = int(sys.argv[3])

    import tbmdata_tcv as tbmdata
    dat = tbmdata.tbmd(shot)
    dat.readAll()
    m, n, eqdata = dat.eqdata(time=time)
    k, l, prdata = dat.prdata(time=time)
    intinbeam, dblinbeam = dat.default_inbeams()
    # chose launcher number
    if ECsystem == 0:
        ECsystem = 1
    dblinbeam = dat.beamdata(time=time, gyr=ECsystem, beamdbl=dblinbeam)

else:
# Build eqdata from specific input files
    m, n, eqdata = ctbm.readTopfile3('tcvdata/topfile')
    k, l, prdata = ctbm.readPrdata('tcvdata/ne.dat', 'tcvdata/Te.dat')
    intinbeam, dblinbeam = ctbm.readInbeam('tcvdata/inbeam.dat')

# read angles from shotfile or skip
    if ECsystem > 0:
        res = beam_param_tcv(shot, time, ECsystem)
        s = res.replace('!', '#').replace(',', '').split('\n')
        for i in s:
            exec(i.strip())
        dblinbeam[0:6] = [xf, xtordeg, xpoldeg, xxb, xyb, xzb]
        dblinbeam[19:26] = [xryyb, xrzzb, xwyyb, xwzzb, xpw0, xrmaj, xrmin]
    else:
        print("using launcher values from inbeam.dat")

# profile calc & absorption model
intinbeam[11] = 1
intinbeam[13] = 1

if 'generate_infiles' in sys.argv:
    ctbm.writeTopfile(m, n, eqdata, fname='_topfile')
    ctbm.writeInbeam(intinbeam, dblinbeam, fname='_inbeam.dat')
    ctbm.writeKinetic(k, l, prdata, fnamene='_ne.dat', fnameTe='_Te.dat')
    print("TORBEAM input files written.")
    sys.exit(0)

# set temporary testing values:
if intinbeam[11] == 0:
    abstr = 'Westerhof'
if intinbeam[11] == 1:
    abstr = 'Farina'
if intinbeam[13] == 0:
    prstr = 'standard'
if intinbeam[13] == 1:
    prstr = 'a la Maj'

# Call the library
rhoresult, t1data, t1tdata, t2data, t2n_data, icnt, ibgout, nv, volprof = ctbm.call_torbeam(intinbeam, dblinbeam, m, n, eqdata, k, l, prdata)

# writing output arrays to files (in similar format as TORBEAM wrapper)

# poloidal plane beam path
with open('t1.dat', 'w') as f:
  for i in t1data.T:
    i.tofile(f, sep=' ', format='%g')
    f.write('\n')

# power and current deposition profile vs. rho
with open('t2n.dat', 'w') as f:
  for i in t2n_data.T:
    i.tofile(f, sep=' ', format='%g')
    f.write('\n')

# 4 numbers as returned from the real-time version go in here
with open('rhoresult.dat', 'w') as f:
  f.write(str(rhoresult))

if noplot == 0:
# Plot result
  plt1 = plt.subplot(2,2,1)
  ctbm.plotEq(eqm=m, eqn=n, eqdata=eqdata, plotbt=0, plteq=plt1)
  plt1.plot(t1data[0,:]/100., t1data[1,:]/100., 'b')
  plt1.plot(t1data[2,:]/100., t1data[3,:]/100., 'b')
  plt1.plot(t1data[4,:]/100., t1data[5,:]/100., 'b')
  plt1.plot([rhoresult[1]/100.],[rhoresult[2]/100.],'k+', mew=5,ms=14)

  textstr = '$\\rho=%.2f$\n$\mathrm{R}=%.2f m$\n$\mathrm{z}=%.2f m$' %  \
     (round(rhoresult[0],2), round(rhoresult[1]/100,2), round(rhoresult[2]/100,2))
  props = dict(boxstyle='round', facecolor='white')
  plt1.text(0.95, 0.05, textstr, transform=plt1.transAxes, fontsize=14, horizontalalignment='right', bbox=props)

  plt2 = plt.subplot(2,2,3)
  plt2.plot(t2n_data[0,:], t2n_data[1,:], 'b')
  plt2.set_title('P_EC vs rho')
  plt3 = plt.subplot(2,2,4)
  plt3.plot(t2n_data[0,:], t2n_data[2,:], 'b')
  plt3.set_title('I_ECCD vs rho')

  plt4 = plt.subplot(2,2,2)
  plt4.set_title('kinetic profiles vs rho')
  plotPr(prdata=prdata, prk=k, prl=l, pltpr=plt4)

  textstr = 'tor=%.2f pol=%.2f\nabsorb:%s\nprofiles:%s' % ( round(dblinbeam[1], 2), round(dblinbeam[2], 2), abstr, prstr )
  plt2.text(0.5, 0.77, textstr ,transform=plt1.transAxes, fontsize=14, horizontalalignment='right', bbox=props)

  plt.tight_layout()
  plt.show()
